/*     */ package IFrames;
/*     */ 
/*     */ import Algoritmos.CreateGraphic;
/*     */ import Algoritmos.SeriesDatos;
/*     */ import java.awt.Color;
/*     */ import java.awt.Font;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.ActionListener;
/*     */ import java.text.DecimalFormat;
/*     */ import javax.swing.BorderFactory;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JColorChooser;
/*     */ import javax.swing.JComponent;
/*     */ import javax.swing.JDesktopPane;
/*     */ import javax.swing.JInternalFrame;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JSpinner;
/*     */ import javax.swing.JTabbedPane;
/*     */ import javax.swing.SpinnerModel;
/*     */ import javax.swing.SpinnerNumberModel;
/*     */ import javax.swing.table.DefaultTableModel;
/*     */ import systemlibs.MDIFrame;
/*     */ 
/*     */ public class SetGraphicIFrame extends JInternalFrame
/*     */ {
/*  35 */   JTabbedPane tabbedPane = new JTabbedPane();
/*  36 */   JComponent panelEjes = new JDesktopPane();
/*  37 */   JComponent panelGraphic = new JDesktopPane();
/*  38 */   JDesktopPane pan1 = new JDesktopPane();
/*  39 */   JDesktopPane pan2 = new JDesktopPane();
/*  40 */   JDesktopPane pan3 = new JDesktopPane();
/*  41 */   JDesktopPane pan4 = new JDesktopPane();
/*  42 */   JDesktopPane pan5 = new JDesktopPane();
/*  43 */   JDesktopPane pan6 = new JDesktopPane();
/*     */   JColorChooser selectColor;
/*  46 */   JLabel lb1 = new JLabel();
/*  47 */   JLabel lb2 = new JLabel();
/*  48 */   JLabel lb3 = new JLabel();
/*  49 */   JLabel lb4 = new JLabel();
/*  50 */   JLabel lb5 = new JLabel();
/*  51 */   JLabel lb6 = new JLabel();
/*  52 */   JLabel lb7 = new JLabel();
/*  53 */   JLabel lb8 = new JLabel();
/*  54 */   JLabel lb9 = new JLabel();
/*  55 */   JLabel lb10 = new JLabel();
/*  56 */   JLabel lb11 = new JLabel();
/*  57 */   JLabel lbColor1 = new JLabel();
/*  58 */   JLabel lbColor2 = new JLabel();
/*  59 */   JLabel lbColor3 = new JLabel();
/*     */   Color colorAux1;
/*     */   Color colorAux2;
/*     */   Color colorAux3;
/*  65 */   private final SpinnerModel spinnerModelMinX = new SpinnerNumberModel(200, 0, 1000, 1);
/*  66 */   private final JSpinner spinnerMinX = new JSpinner(this.spinnerModelMinX);
/*  67 */   private final SpinnerModel spinnerModelMaxX = new SpinnerNumberModel(800, 0, 16000, 1);
/*  68 */   private final JSpinner spinnerMaxX = new JSpinner(this.spinnerModelMaxX);
/*  69 */   private final SpinnerModel spinnerModelIncX = new SpinnerNumberModel(100, 0, 1000, 1);
/*  70 */   private final JSpinner spinnerIncX = new JSpinner(this.spinnerModelIncX);
/*     */ 
/*  72 */   private final SpinnerModel spinnerModelMinY = new SpinnerNumberModel(0.0D, -1.0D, 2.0D, 0.01D);
/*  73 */   private final JSpinner spinnerMinY = new JSpinner(this.spinnerModelMinY);
/*  74 */   private final SpinnerModel spinnerModelMaxY = new SpinnerNumberModel(1.1D, -1.0D, 2.0D, 0.1D);
/*  75 */   private final JSpinner spinnerMaxY = new JSpinner(this.spinnerModelMaxY);
/*  76 */   private final SpinnerModel spinnerModelIncY = new SpinnerNumberModel(0.2D, 0.0D, 2.0D, 0.1D);
/*  77 */   private final JSpinner spinnerIncY = new JSpinner(this.spinnerModelIncY);
/*     */ 
/*  79 */   private final SpinnerModel spinnerModelLine = new SpinnerNumberModel(1.2D, 0.0D, 5.0D, 0.1D);
/*  80 */   private final JSpinner spinnerLine = new JSpinner(this.spinnerModelLine);
/*  81 */   private final SpinnerModel spinnerModelGuide = new SpinnerNumberModel(1.2D, 0.0D, 5.0D, 0.1D);
/*  82 */   private final JSpinner spinnerGuide = new JSpinner(this.spinnerModelGuide);
/*     */ 
/*  84 */   JButton buttAppli1 = new JButton();
/*  85 */   JButton buttAppli2 = new JButton();
/*  86 */   JButton buttColorLine = new JButton();
/*  87 */   JButton buttColorBackground = new JButton();
/*  88 */   JButton buttColorGuide = new JButton();
/*  89 */   JDesktopPane deskGraph = new JDesktopPane();
/*     */   CreateGraphic graf;
/*     */   String ion;
/*     */   float lmbInf;
/*     */   float lmbSup;
/*     */   double[][] spectraAux;
/*     */   double[][] spectraPeak;
/*  97 */   DecimalFormat df = new DecimalFormat("#.##");
/*     */   float Inf;
/*     */   float Sup;
/*     */   String str;
/*     */   DefaultTableModel tbMod;
/*     */ 
/*     */   public SetGraphicIFrame()
/*     */   {
/* 104 */     initComponents();
/* 105 */     orderComponents();
/*     */   }
/*     */ 
/*     */   private void initComponents() {
/* 109 */     setClosable(true);
/* 110 */     setTitle("Ajustes de la Gráfica");
/*     */ 
/* 112 */     this.pan1.setFont(new Font("Courier New", 0, 14));
/* 113 */     this.pan1.setBorder(BorderFactory.createTitledBorder("Longitud de Onda"));
/*     */ 
/* 115 */     this.pan2.setFont(new Font("Courier New", 0, 14));
/* 116 */     this.pan2.setBorder(BorderFactory.createTitledBorder("Intensidad Relativa"));
/*     */ 
/* 118 */     this.pan3.setFont(new Font("Courier New", 0, 14));
/* 119 */     this.pan3.setBorder(BorderFactory.createTitledBorder("Espectro"));
/*     */ 
/* 121 */     this.pan4.setFont(new Font("Courier New", 0, 14));
/* 122 */     this.pan4.setBorder(BorderFactory.createTitledBorder("Fondo"));
/*     */ 
/* 124 */     this.pan5.setFont(new Font("Courier New", 0, 14));
/* 125 */     this.pan5.setBorder(BorderFactory.createTitledBorder("Líneas Guia"));
/*     */ 
/* 127 */     this.lb1.setFont(new Font("Courier New", 0, 14));
/* 128 */     this.lb1.setText("Desde:");
/*     */ 
/* 130 */     this.lb2.setFont(new Font("Courier New", 0, 14));
/* 131 */     this.lb2.setText("Hasta:");
/*     */ 
/* 133 */     this.lb3.setFont(new Font("Courier New", 0, 14));
/* 134 */     this.lb3.setText("Incremento:");
/*     */ 
/* 136 */     this.lb4.setFont(new Font("Courier New", 0, 14));
/* 137 */     this.lb4.setText("Desde:");
/*     */ 
/* 139 */     this.lb5.setFont(new Font("Courier New", 0, 14));
/* 140 */     this.lb5.setText("Hasta:");
/*     */ 
/* 142 */     this.lb6.setFont(new Font("Courier New", 0, 14));
/* 143 */     this.lb6.setText("Incremento:");
/*     */ 
/* 145 */     this.lb7.setFont(new Font("Courier New", 0, 14));
/* 146 */     this.lb7.setText("Color de Línea:");
/*     */ 
/* 148 */     this.lb8.setFont(new Font("Courier New", 0, 14));
/* 149 */     this.lb8.setText("Grosor:");
/*     */ 
/* 151 */     this.lb9.setFont(new Font("Courier New", 0, 14));
/* 152 */     this.lb9.setText("Color de Fondo:");
/*     */ 
/* 154 */     this.lb10.setFont(new Font("Courier New", 0, 14));
/* 155 */     this.lb10.setText("Color de Línea:");
/*     */ 
/* 157 */     this.lb11.setFont(new Font("Courier New", 0, 14));
/* 158 */     this.lb11.setText("Grosor:");
/*     */ 
/* 160 */     this.buttAppli1.setFont(new Font("Courier New", 0, 15));
/* 161 */     this.buttAppli1.setText("Aplicar");
/*     */ 
/* 163 */     this.buttAppli2.setFont(new Font("Courier New", 0, 15));
/* 164 */     this.buttAppli2.setText("Aplicar");
/*     */ 
/* 166 */     this.buttColorLine.setFont(new Font("Courier New", 0, 15));
/* 167 */     this.buttColorLine.setText("x");
/*     */ 
/* 169 */     this.buttColorBackground.setFont(new Font("Courier New", 0, 15));
/* 170 */     this.buttColorBackground.setText("x");
/*     */ 
/* 172 */     this.buttColorGuide.setFont(new Font("Courier New", 0, 15));
/* 173 */     this.buttColorGuide.setText("x");
/*     */   }
/*     */ 
/*     */   private void orderComponents()
/*     */   {
/* 178 */     setSize(350, 335);
/* 179 */     this.pan1.setBounds(10, 10, 320, 90);
/* 180 */     this.lb1.setBounds(10, 25, 60, 27);
/* 181 */     this.lb2.setBounds(10, 55, 60, 27);
/* 182 */     this.lb3.setBounds(150, 25, 90, 27);
/* 183 */     this.spinnerMinX.setBounds(80, 25, 60, 27);
/* 184 */     this.spinnerMaxX.setBounds(80, 55, 60, 27);
/* 185 */     this.spinnerIncX.setBounds(250, 25, 60, 27);
/* 186 */     this.spinnerMinX.setValue(Integer.valueOf(MDIFrame.chart.getRangeMin()));
/* 187 */     this.spinnerMaxX.setValue(Integer.valueOf(MDIFrame.chart.getRangeMax()));
/* 188 */     this.spinnerIncX.setValue(Integer.valueOf(MDIFrame.chart.getIncrementRange()));
/* 189 */     this.pan1.add(this.lb1);
/* 190 */     this.pan1.add(this.lb2);
/* 191 */     this.pan1.add(this.lb3);
/* 192 */     this.pan1.add(this.spinnerMinX);
/* 193 */     this.pan1.add(this.spinnerMaxX);
/* 194 */     this.pan1.add(this.spinnerIncX);
/*     */ 
/* 196 */     this.pan2.setBounds(10, 110, 320, 90);
/* 197 */     this.lb4.setBounds(10, 25, 60, 27);
/* 198 */     this.lb5.setBounds(10, 55, 60, 27);
/* 199 */     this.lb6.setBounds(150, 25, 90, 27);
/* 200 */     this.spinnerMinY.setBounds(80, 25, 60, 27);
/* 201 */     this.spinnerMaxY.setBounds(80, 55, 60, 27);
/* 202 */     this.spinnerIncY.setBounds(250, 25, 60, 27);
/* 203 */     this.spinnerMinY.setValue(Float.valueOf(MDIFrame.chart.getDomainMin()));
/* 204 */     this.spinnerMaxY.setValue(Float.valueOf(MDIFrame.chart.getDomainMax()));
/* 205 */     this.spinnerIncY.setValue(Float.valueOf(MDIFrame.chart.getIncrementDomain()));
/*     */ 
/* 207 */     this.pan2.add(this.lb4);
/* 208 */     this.pan2.add(this.lb5);
/* 209 */     this.pan2.add(this.lb6);
/* 210 */     this.pan2.add(this.spinnerMinY);
/* 211 */     this.pan2.add(this.spinnerMaxY);
/* 212 */     this.pan2.add(this.spinnerIncY);
/*     */ 
/* 214 */     this.buttAppli1.setBounds(10, 220, 250, 35);
/*     */ 
/* 216 */     this.panelEjes.add(this.pan1);
/* 217 */     this.panelEjes.add(this.pan2);
/* 218 */     this.panelEjes.add(this.buttAppli1);
/*     */ 
/* 220 */     this.pan3.setBounds(10, 10, 320, 65);
/* 221 */     this.lb7.setBounds(10, 25, 115, 27);
/* 222 */     this.lbColor1.setBounds(130, 25, 20, 20);
/* 223 */     this.lbColor1.setOpaque(true);
/* 224 */     this.lbColor1.setBackground(MDIFrame.chart.getG0Color());
/* 225 */     this.colorAux1 = MDIFrame.chart.getG0Color();
/* 226 */     this.lbColor1.setBorder(BorderFactory.createLineBorder(Color.black));
/* 227 */     this.buttColorLine.setBounds(160, 25, 20, 20);
/*     */ 
/* 229 */     this.lb8.setBounds(190, 25, 60, 27);
/* 230 */     this.spinnerLine.setBounds(250, 25, 60, 27);
/*     */ 
/* 232 */     this.pan3.add(this.lb7);
/* 233 */     this.pan3.add(this.lbColor1);
/* 234 */     this.pan3.add(this.buttColorLine);
/* 235 */     this.pan3.add(this.lb8);
/* 236 */     this.pan3.add(this.lb9);
/* 237 */     this.pan3.add(this.spinnerLine);
/*     */ 
/* 239 */     this.pan4.setBounds(10, 80, 320, 55);
/* 240 */     this.lb9.setBounds(10, 25, 120, 27);
/* 241 */     this.lbColor2.setBounds(130, 25, 20, 20);
/* 242 */     this.lbColor2.setOpaque(true);
/* 243 */     this.lbColor2.setBackground(MDIFrame.chart.getBackL());
/* 244 */     this.colorAux2 = MDIFrame.chart.getBackL();
/* 245 */     this.lbColor2.setBorder(BorderFactory.createLineBorder(Color.black));
/* 246 */     this.buttColorBackground.setBounds(160, 25, 20, 20);
/* 247 */     this.spinnerLine.setValue(Float.valueOf(MDIFrame.chart.getStrokLine()));
/*     */ 
/* 249 */     this.pan4.add(this.lb9);
/* 250 */     this.pan4.add(this.lbColor2);
/* 251 */     this.pan4.add(this.buttColorBackground);
/*     */ 
/* 253 */     this.pan5.setBounds(10, 145, 320, 65);
/* 254 */     this.lb10.setBounds(10, 25, 115, 27);
/* 255 */     this.lbColor3.setBounds(130, 25, 20, 20);
/* 256 */     this.lbColor3.setOpaque(true);
/* 257 */     this.lbColor3.setBackground(MDIFrame.chart.getGuideL());
/* 258 */     this.colorAux3 = MDIFrame.chart.getGuideL();
/* 259 */     this.lbColor3.setBorder(BorderFactory.createLineBorder(Color.black));
/* 260 */     this.buttColorGuide.setBounds(160, 25, 20, 20);
/*     */ 
/* 262 */     this.lb11.setBounds(190, 25, 60, 27);
/* 263 */     this.spinnerGuide.setBounds(250, 25, 60, 27);
/*     */ 
/* 265 */     this.pan5.add(this.lb10);
/* 266 */     this.pan5.add(this.lbColor3);
/* 267 */     this.pan5.add(this.buttColorGuide);
/* 268 */     this.pan5.add(this.lb11);
/* 269 */     this.pan5.add(this.spinnerGuide);
/* 270 */     this.buttAppli2.setBounds(10, 220, 250, 35);
/*     */ 
/* 272 */     this.spinnerGuide.setValue(Float.valueOf(MDIFrame.chart.getStrokGuide()));
/*     */ 
/* 274 */     this.panelGraphic.add(this.pan3);
/* 275 */     this.panelGraphic.add(this.pan4);
/* 276 */     this.panelGraphic.add(this.pan5);
/* 277 */     this.panelGraphic.add(this.buttAppli2);
/*     */ 
/* 279 */     this.tabbedPane.addTab("Ejes", this.panelEjes);
/* 280 */     this.tabbedPane.addTab("Gráfica", this.panelGraphic);
/*     */ 
/* 282 */     this.buttAppli1.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 284 */         SetGraphicIFrame.this.setAxisValues();
/*     */       }
/*     */     });
/* 288 */     this.buttAppli2.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 290 */         SetGraphicIFrame.this.setColorsStrokValues();
/*     */       }
/*     */     });
/* 294 */     this.buttColorLine.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 296 */         SetGraphicIFrame.this.setLineColor();
/*     */       }
/*     */     });
/* 300 */     this.buttColorBackground.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 302 */         SetGraphicIFrame.this.setBackgroundColor();
/*     */       }
/*     */     });
/* 306 */     this.buttColorGuide.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 308 */         SetGraphicIFrame.this.setGuideColor();
/*     */       }
/*     */     });
/* 312 */     add(this.tabbedPane);
/*     */   }
/*     */ 
/*     */   private void setAxisValues() {
/* 316 */     MDIFrame.chart.setRangeMin(((Integer)this.spinnerMinX.getValue()).intValue());
/* 317 */     MDIFrame.chart.setRangeMax(((Integer)this.spinnerMaxX.getValue()).intValue());
/* 318 */     MDIFrame.chart.setIncrementRange(((Integer)this.spinnerIncX.getValue()).intValue());
/* 319 */     MDIFrame.chart.setDomainMin(Float.valueOf(String.valueOf(this.spinnerMinY.getValue())).floatValue());
/* 320 */     MDIFrame.chart.setDomainMax(Float.valueOf(String.valueOf(this.spinnerMaxY.getValue())).floatValue());
/* 321 */     MDIFrame.chart.setIncrementDomain(((Float)this.spinnerIncY.getValue()).floatValue());
/* 322 */     MDIFrame.chart.setAxis();
/*     */   }
/*     */ 
/*     */   private void setColorsStrokValues() {
/* 326 */     MDIFrame.chart.setBackL(this.colorAux2);
/* 327 */     MDIFrame.chart.setG0Color(this.colorAux1);
/* 328 */     MDIFrame.chart.setGuideL(this.colorAux3);
/* 329 */     MDIFrame.chart.setStrokLine(Float.valueOf(String.valueOf(this.spinnerLine.getValue())).floatValue());
/* 330 */     MDIFrame.chart.setStrokGuide(Float.valueOf(String.valueOf(this.spinnerGuide.getValue())).floatValue());
/* 331 */     MDIFrame.chart.setColorsStrok();
/*     */   }
/*     */ 
/*     */   private void setLineColor() {
/* 335 */     this.colorAux1 = getColor();
/* 336 */     this.lbColor1.setBackground(this.colorAux1);
/*     */   }
/*     */ 
/*     */   private void setGuideColor() {
/* 340 */     this.colorAux3 = getColor();
/* 341 */     this.lbColor3.setBackground(this.colorAux3);
/*     */   }
/*     */ 
/*     */   private void setBackgroundColor() {
/* 345 */     this.colorAux2 = getColor();
/* 346 */     this.lbColor2.setBackground(this.colorAux2);
/*     */   }
/*     */ 
/*     */   private Color getColor()
/*     */   {
/* 352 */     Color selectedColor = new Color(0, 0, 0);
/* 353 */     this.selectColor = new JColorChooser();
/* 354 */     selectedColor = JColorChooser.showDialog(null, "Seleccione un color", Color.BLACK);
/* 355 */     return selectedColor;
/*     */   }
/*     */ 
/*     */   private void cancel()
/*     */   {
/*     */   }
/*     */ 
/*     */   private void getGraphic() {
/* 363 */     MDIFrame.sd.setLine(0);
/* 364 */     MDIFrame.sd.setSpectraGraph(this.spectraAux);
/* 365 */     MDIFrame.sd.addSpectra();
/*     */   }
/*     */ 
/*     */   public boolean isNumberFloat(String cadena) {
/*     */     try {
/* 370 */       Float.parseFloat(cadena);
/* 371 */       return true; } catch (NumberFormatException nfe) {
/*     */     }
/* 373 */     return false;
/*     */   }
/*     */ }

/* Location:           /home/pardo/Desktop/material_beca_ciop/LIBS Analyzer/LIBS Analyzer Linux/LIBSAnalyzer.jar
 * Qualified Name:     IFrames.SetGraphicIFrame
 * JD-Core Version:    0.6.2
 */