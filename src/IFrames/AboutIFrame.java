/*     */ package IFrames;
/*     */ 
/*     */ import Algoritmos.CreateGraphic;
/*     */ import java.awt.Font;
/*     */ import javax.swing.JDesktopPane;
/*     */ import javax.swing.JInternalFrame;
/*     */ import javax.swing.JLabel;
/*     */ 
/*     */ public class AboutIFrame extends JInternalFrame
/*     */ {
/*  28 */   JLabel lb1 = new JLabel();
/*  29 */   JLabel lb2 = new JLabel();
/*  30 */   JLabel lb3 = new JLabel();
/*  31 */   JLabel lb4 = new JLabel();
/*  32 */   JLabel lb5 = new JLabel();
/*  33 */   JLabel lb6 = new JLabel();
/*  34 */   JLabel lb7 = new JLabel();
/*  35 */   JLabel lb8 = new JLabel();
/*  36 */   JLabel lb9 = new JLabel();
/*     */ 
/*  38 */   JDesktopPane deskGraph = new JDesktopPane();
/*     */   CreateGraphic graf;
/*     */ 
/*     */   public AboutIFrame()
/*     */   {
/*  43 */     initComponents();
/*  44 */     orderComponents();
/*     */   }
/*     */ 
/*     */   private void initComponents() {
/*  48 */     setClosable(true);
/*  49 */     setTitle("Acerca de");
/*     */ 
/*  51 */     this.lb1.setFont(new Font("Courier New", 0, 14));
/*  52 */     this.lb1.setText("Este programa es propiedad del");
/*  53 */     this.lb1.setHorizontalAlignment(0);
/*     */ 
/*  55 */     this.lb2.setFont(new Font("Courier New", 0, 14));
/*  56 */     this.lb2.setText("Instituto Politécnico Nacional");
/*  57 */     this.lb2.setHorizontalAlignment(0);
/*     */ 
/*  59 */     this.lb3.setFont(new Font("Courier New", 0, 14));
/*  60 */     this.lb3.setText("Registro público  número:");
/*  61 */     this.lb3.setHorizontalAlignment(0);
/*     */ 
/*  63 */     this.lb4.setFont(new Font("Courier New", 1, 14));
/*  64 */     this.lb4.setText("03-2014-111810131900-01");
/*  65 */     this.lb4.setHorizontalAlignment(0);
/*     */ 
/*  67 */     this.lb5.setFont(new Font("Courier New", 0, 14));
/*  68 */     this.lb5.setText("MTA. Zeferino Pérez Báez");
/*  69 */     this.lb5.setHorizontalAlignment(0);
/*     */ 
/*  71 */     this.lb6.setFont(new Font("Courier New", 0, 14));
/*  72 */     this.lb6.setText("Dr. Luis Vidal Ponce Cabrera");
/*  73 */     this.lb6.setHorizontalAlignment(0);
/*     */ 
/*  75 */     this.lb7.setFont(new Font("Courier New", 0, 14));
/*  76 */     this.lb7.setText("Dra. Teresa Flores Reyes");
/*  77 */     this.lb7.setHorizontalAlignment(0);
/*     */ 
/*  79 */     this.lb8.setFont(new Font("Courier New", 0, 14));
/*  80 */     this.lb8.setText("Dr. Eduardo Marcelo de Posada");
/*  81 */     this.lb8.setHorizontalAlignment(0);
/*     */ 
/*  83 */     this.lb9.setFont(new Font("Courier New", 0, 14));
/*  84 */     this.lb9.setText("CICATA Altamira - IPN  México 2015");
/*  85 */     this.lb9.setHorizontalAlignment(0);
/*     */   }
/*     */ 
/*     */   private void orderComponents()
/*     */   {
/*  91 */     setSize(300, 240);
/*  92 */     this.lb1.setBounds(0, 5, 290, 27);
/*  93 */     this.lb2.setBounds(0, 20, 290, 27);
/*  94 */     this.lb3.setBounds(0, 45, 290, 27);
/*  95 */     this.lb4.setBounds(0, 65, 290, 27);
/*  96 */     this.lb5.setBounds(0, 95, 290, 27);
/*  97 */     this.lb6.setBounds(0, 110, 290, 27);
/*  98 */     this.lb7.setBounds(0, 125, 290, 27);
/*  99 */     this.lb8.setBounds(0, 140, 290, 27);
/* 100 */     this.lb9.setBounds(0, 170, 290, 27);
/*     */ 
/* 103 */     this.deskGraph.add(this.lb1);
/* 104 */     this.deskGraph.add(this.lb2);
/* 105 */     this.deskGraph.add(this.lb3);
/* 106 */     this.deskGraph.add(this.lb4);
/* 107 */     this.deskGraph.add(this.lb5);
/* 108 */     this.deskGraph.add(this.lb6);
/* 109 */     this.deskGraph.add(this.lb7);
/* 110 */     this.deskGraph.add(this.lb8);
/* 111 */     this.deskGraph.add(this.lb9);
/*     */ 
/* 114 */     add(this.deskGraph);
/*     */   }
/*     */ }

/* Location:           /home/pardo/Desktop/material_beca_ciop/LIBS Analyzer/LIBS Analyzer Linux/LIBSAnalyzer.jar
 * Qualified Name:     IFrames.AboutIFrame
 * JD-Core Version:    0.6.2
 */