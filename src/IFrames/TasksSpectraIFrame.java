package IFrames;

import Algoritmos.Espectro;
import Algoritmos.SeriesDatos;
import Algoritmos.CreateGraphic;
import Algoritmos.GraficaEspectro;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;

import javax.swing.BoundedRangeModel;

import javax.swing.SwingConstants;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JTabbedPane;
import javax.swing.JDialog;
import javax.swing.JColorChooser;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import systemlibs.MDIFrame;
import java.awt.Dialog.ModalityType;
import javax.swing.BoxLayout;
import java.awt.Component;
import javax.swing.border.TitledBorder;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.Box;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TasksSpectraIFrame extends JDialog
{
  public Espectro espectro;
  public GraficaEspectro graficaEspectro;

  JLabel lb1 = new JLabel();
  JLabel lb2 = new JLabel();
  JLabel lb3 = new JLabel();
  JLabel lb4 = new JLabel();
  JLabel lb5 = new JLabel();
  JLabel lbOffset;
  JLabel lbNormalize = new JLabel();

  JDesktopPane pan1 = new JDesktopPane();
  JDesktopPane pan2 = new JDesktopPane();
  JDesktopPane pan3 = new JDesktopPane();
  JDesktopPane panOffset = new JDesktopPane();
  JDesktopPane panNormalize = new JDesktopPane();
  JDesktopPane panRange = new JDesktopPane();

  JLabel entOffset;

  JLabel lbLambdaMin;
  JLabel lbLambdaMax;

  JComboBox<String> comSmoothing = new JComboBox<String>();
  JComboBox<String> normalizeMode = new JComboBox<String>();

  JSlider valNormalizeMin = new JSlider();
  JSlider valNormalizeMax = new JSlider();

  JSlider valRangeMin = new JSlider();
  JSlider valRangeMax = new JSlider();

  JSlider valOffset = new JSlider();
  JSlider valSmooth = new JSlider();
  JSlider valBaseLine = new JSlider();
  JSlider valPeakDetect = new JSlider();
  JCheckBox chkPeakDetect = new JCheckBox();
  JCheckBox chkOffset = new JCheckBox();
  JCheckBox chkRange = new JCheckBox();
  JCheckBox chkBaseLine = new JCheckBox();
  JDesktopPane deskGraph = new JDesktopPane();
  JDesktopPane paneEstilo = new JDesktopPane();
  JTabbedPane tabsPane = new JTabbedPane();
  private double[][] spectraFilter;
  public static double[][] spectraPeak;
  private double[][] spectraBase;
  private double[][] spectraAux;
  private final JDesktopPane paneApariencia = new JDesktopPane();
  private final JDesktopPane desktopPane = new JDesktopPane();
  private final JCheckBox chkLabelsVisibility = new JCheckBox("Mostrar etiquetas");
  private final JDesktopPane desktopPane_1 = new JDesktopPane();
  private final JCheckBox chkPeaksVisibility = new JCheckBox("Mostrar picos");
  private final Box horizontalBox = Box.createHorizontalBox();
  private final JLabel lblColor = new JLabel("Color: ");
  private final JButton btnPeaksColor = new JButton(" ");
  private final Box horizontalBox_1 = Box.createHorizontalBox();
  private final JLabel label = new JLabel("Color: ");
  private final JButton btnSpectraColor = new JButton(" ");

  public TasksSpectraIFrame(Espectro e, GraficaEspectro g, boolean es_identificado)
  {
      // setModal(true);
      setAlwaysOnTop(true);
      setModalityType(ModalityType.APPLICATION_MODAL);
    this.espectro = e;
    this.graficaEspectro = g;
    initComponents();
    orderComponents(es_identificado);
    setDefaultValues();
    addEventHandlers();

  }

  public TasksSpectraIFrame(Espectro e, GraficaEspectro g)
  {
    this(e, g, false);
  }

  private void initComponents() {
    setTitle("Ajustar Espectro");

    this.pan1.setFont(new Font("Courier New", 0, 14));
    this.pan1.setBorder(BorderFactory.createTitledBorder("Suavizado"));

    this.pan2.setFont(new Font("Courier New", 0, 14));
    this.pan2.setBorder(BorderFactory.createTitledBorder("Línea Base"));

    this.pan3.setFont(new Font("Courier New", 0, 14));
    this.pan3.setBorder(BorderFactory.createTitledBorder("Detección de Picos"));

    this.panOffset.setFont(new Font("Courier New", 0, 14));
    this.panOffset.setBorder(BorderFactory.createTitledBorder("Offset en longitud de onda"));
    this.entOffset = new JLabel("0 nm.");

    this.panRange.setFont(new Font("Courier New", 0, 14));
    this.panRange.setBorder(BorderFactory.createTitledBorder("Restringir rango"));

    this.panNormalize.setFont(new Font("Courier New", 0, 14));
    this.panNormalize.setBorder(BorderFactory.createTitledBorder("Normalización"));

    this.lb1.setFont(new Font("Courier New", 0, 14));
    this.lb1.setText("Filtro de Suavizado: ");

    this.lb2.setFont(new Font("Courier New", 0, 14));
    this.lb2.setText("Amplitud: ");

    this.lb3.setFont(new Font("Courier New", 0, 14));
    this.lb3.setText("Nivel de Suavizado: ");

    this.lb4.setFont(new Font("Courier New", 0, 14));
    this.lb4.setText("Amplitud: ");

    this.lb5.setFont(new Font("Courier New", 0, 14));
    this.lb5.setText("Nivel de Suavizado: ");

    this.lbNormalize.setFont(new Font("Courier New", 0, 14));
    this.lbNormalize.setText("Modo de normalización: ");


    this.valOffset.setMaximum(1000);
    this.valOffset.setMinimum(-1000);
    this.valOffset.setValue(0);
    this.valOffset.setMajorTickSpacing(500);
    this.valOffset.setMinorTickSpacing(100);
    this.valOffset.setPaintTicks(true);
    this.valOffset.setPaintLabels(true);
    this.valOffset.setBackground(Color.white);

    Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
    labelTable.put( new Integer(    0  ), new JLabel("0.0") );
    labelTable.put( new Integer(   500 ), new JLabel("5")   );
    labelTable.put( new Integer(  -500 ), new JLabel("5")   );
    labelTable.put( new Integer(  1000 ), new JLabel("10")  );
    labelTable.put( new Integer( -1000 ), new JLabel("10")  );
    this.valOffset.setLabelTable(labelTable);

    this.valOffset.setFont(new Font("Courier New", 0, 11));

    this.comSmoothing.setFont(new Font("Courier New", 0, 14));
    this.comSmoothing.addItem("Ninguno");
    this.comSmoothing.addItem("Moving Average");
    this.comSmoothing.addItem("Savitzky-Golay");

    this.normalizeMode.setFont(new Font("Courier New", 0, 14));
    this.normalizeMode.addItem("Automático");
    this.normalizeMode.addItem("Rango Manual");

    this.lbLambdaMin = new JLabel("0 nm.");
    this.lbLambdaMax = new JLabel("1000 nm.");
    this.lbLambdaMax.setHorizontalAlignment(SwingConstants.RIGHT);
    this.lbLambdaMin.setHorizontalAlignment(SwingConstants.LEFT);


    this.valRangeMax.setMaximum(1000);
    this.valRangeMax.setMinimum(0);
    this.valRangeMax.setMajorTickSpacing(200);
    this.valRangeMax.setPaintTicks(true);
    this.valRangeMax.setPaintLabels(true);
    this.valRangeMax.setValue(1000);
    this.valRangeMax.setBackground(Color.white);
    this.valRangeMax.setFont(new Font("Courier New", 0, 11));

    this.valRangeMin.setMaximum(1000);
    this.valRangeMin.setMinimum(0);
    this.valRangeMin.setMajorTickSpacing(200);
    this.valRangeMin.setPaintTicks(true);
    this.valRangeMin.setPaintLabels(true);
    this.valRangeMin.setValue(0);
    this.valRangeMin.setBackground(Color.white);
    this.valRangeMin.setFont(new Font("Courier New", 0, 11));

    this.valNormalizeMax.setMaximum(1000);
    this.valNormalizeMax.setMinimum(0);
    this.valNormalizeMax.setMajorTickSpacing(200);
    this.valNormalizeMax.setPaintTicks(true);
    this.valNormalizeMax.setPaintLabels(true);
    this.valNormalizeMax.setValue(1000);
    this.valNormalizeMax.setBackground(Color.white);
    this.valNormalizeMax.setFont(new Font("Courier New", 0, 11));

    this.valNormalizeMin.setMaximum(1000);
    this.valNormalizeMin.setMinimum(0);
    this.valNormalizeMin.setMajorTickSpacing(200);
    this.valNormalizeMin.setPaintTicks(true);
    this.valNormalizeMin.setPaintLabels(true);
    this.valNormalizeMin.setValue(0);
    this.valNormalizeMin.setBackground(Color.white);
    this.valNormalizeMin.setFont(new Font("Courier New", 0, 11));

    this.valSmooth.setMaximum(10);
    this.valSmooth.setMinimum(1);
    this.valSmooth.setMajorTickSpacing(1);
    this.valSmooth.setPaintTicks(true);
    this.valSmooth.setPaintLabels(true);
    this.valSmooth.setValue(5);
    this.valSmooth.setBackground(Color.white);
    this.valSmooth.setFont(new Font("Courier New", 0, 11));

    this.valBaseLine.setMaximum(1000);
    this.valBaseLine.setMinimum(1);
    this.valBaseLine.setMajorTickSpacing(1);
    this.valBaseLine.setPaintTicks(false);
    this.valBaseLine.setPaintLabels(false);
    this.valBaseLine.setValue(1);
    this.valBaseLine.setBackground(Color.white);
    this.valBaseLine.setFont(new Font("Courier New", 0, 11));

    this.valPeakDetect.setMaximum(15);
    this.valPeakDetect.setMinimum(1);
    this.valPeakDetect.setMajorTickSpacing(1);
    this.valPeakDetect.setPaintTicks(false);
    this.valPeakDetect.setPaintTicks(true);
    this.valPeakDetect.setPaintLabels(true);
    this.valPeakDetect.setValue(8);
    this.valPeakDetect.setBackground(Color.white);
    this.valPeakDetect.setFont(new Font("Courier New", 0, 11));

    this.chkBaseLine.setFont(new Font("Courier New", 0, 14));
    this.chkBaseLine.setText("Corrección de Línea Base");
    this.chkBaseLine.setBackground(Color.white);

    this.chkPeakDetect.setFont(new Font("Courier New", 0, 14));
    this.chkPeakDetect.setText("Detección de Intensidades Pico");
    this.chkPeakDetect.setBackground(Color.white);

    this.chkOffset.setFont(new Font("Courier New", 0, 14));
    this.chkOffset.setText("Habilitar");
    this.chkOffset.setBackground(Color.white);

    this.chkRange.setFont(new Font("Courier New", 0, 14));
    this.chkRange.setText("Habilitar");
    this.chkRange.setBackground(Color.white);

  }

  private void orderComponents(boolean es_identificado)
  {
    setSize(370, 790);

    this.pan1.setBounds(10, 495, 340, 120);
    this.lb1.setBounds(10, 25, 180, 27);
    this.comSmoothing.setBounds(180, 25, 150, 27);
    this.valSmooth.setBounds(10, 65, 320, 42);

    this.pan1.add(this.lb1);
    this.pan1.add(this.comSmoothing);
    this.pan1.add(this.valSmooth);

    this.pan2.setBounds(10, 380, 340, 107);
    this.chkBaseLine.setBounds(10, 25, 320, 27);
    this.valBaseLine.setBounds(10, 55, 320, 42);
    this.pan2.add(this.chkBaseLine);
    this.pan2.add(this.valBaseLine);

    this.pan3.setBounds(10, 615, 340, 107);

    this.chkPeakDetect.setBounds(10, 25, 320, 27);
    this.valPeakDetect.setBounds(10, 55, 320, 42);

    this.pan3.add(this.chkPeakDetect);
    this.pan3.add(this.valPeakDetect);

    this.panOffset.setBounds(10,  10, 340, 120);
    this.chkOffset.setBounds(160, 25, 100, 27);
    this.entOffset.setBounds(10,  25, 150, 27);
    this.valOffset.setBounds(10,  65, 320, 42);
    this.panOffset.add(this.chkOffset);
    this.panOffset.add(this.entOffset);
    this.panOffset.add(this.valOffset);

    this.panRange.setBounds(10, 135, 340, 120);
    this.chkRange.setBounds(230, 25, 100, 27);
    this.lbLambdaMin.setBounds(20,  25, 90,  27);
    this.lbLambdaMax.setBounds(80,  25, 90,  27);
    this.valRangeMin.setBounds(10,  65, 160, 42);
    this.valRangeMax.setBounds(170, 65, 160, 42);
    this.panRange.add(this.chkRange);
    this.panRange.add(this.lbLambdaMin);
    this.panRange.add(this.lbLambdaMax);
    this.panRange.add(this.valRangeMin);
    this.panRange.add(this.valRangeMax);

    this.panNormalize.setBounds(10, 255, 340, 120);
    this.lbNormalize.setBounds(10, 25, 190, 27);
    this.normalizeMode.setBounds(180, 25, 150, 27);
    this.valNormalizeMin.setBounds(10, 65, 160, 42);
    this.valNormalizeMax.setBounds(170, 65, 160, 42);
    this.panNormalize.add(this.normalizeMode);
    this.panNormalize.add(this.valNormalizeMin);
    this.panNormalize.add(this.valNormalizeMax);
    this.panNormalize.add(this.lbNormalize);

    this.deskGraph.add(this.pan1);
    this.deskGraph.add(this.pan2);
    this.deskGraph.add(this.pan3);
    this.deskGraph.add(this.panOffset);
    this.deskGraph.add(this.panRange);
    this.deskGraph.add(this.panNormalize);

    getContentPane().add(this.tabsPane);
    
    paneApariencia.setLayout(new BoxLayout(paneApariencia, BoxLayout.Y_AXIS));
    
    desktopPane.setMaximumSize(new Dimension(2147483647, 80));
    desktopPane.setPreferredSize(new Dimension(0, 120));
    desktopPane.setAlignmentX(Component.LEFT_ALIGNMENT);
    desktopPane.setAlignmentY(Component.TOP_ALIGNMENT);
    desktopPane.setBorder(new TitledBorder(null, "Espectro", TitledBorder.LEADING, TitledBorder.TOP, null, null));
    desktopPane.setLayout(new GridLayout(3, 1, 0, 0));
    paneApariencia.add(desktopPane);
    
    desktopPane.add(horizontalBox_1);
    
    horizontalBox_1.add(label);
    btnSpectraColor.setPreferredSize(new Dimension(34, 34));
    
    horizontalBox_1.add(btnSpectraColor);
    desktopPane_1.setAlignmentX(Component.LEFT_ALIGNMENT);
    desktopPane_1.setMaximumSize(new Dimension(2147483647, 80));
    desktopPane_1.setBorder(new TitledBorder(null, "Picos", TitledBorder.LEADING, TitledBorder.TOP, null, null));
    paneApariencia.add(desktopPane_1);
    desktopPane_1.setLayout(new GridLayout(3, 1, 0, 0));
    chkPeaksVisibility.setSelected(true);
    chkPeaksVisibility.setBackground(Color.WHITE);
    chkLabelsVisibility.setBackground(Color.WHITE);
    chkLabelsVisibility.setSelected(true);
    
    desktopPane_1.add(chkPeaksVisibility);
    desktopPane_1.add(chkLabelsVisibility);
    
    desktopPane_1.add(horizontalBox);
    
    horizontalBox.add(lblColor);
    btnPeaksColor.setPreferredSize(new Dimension(34, 34));
    
    horizontalBox.add(btnPeaksColor);
    if (!es_identificado) {
        this.tabsPane.add("Ajustes", this.deskGraph);
    }
    this.tabsPane.addTab("Apariencia", null, paneApariencia, null);
  }

  private void tasksValues()
  {
    this.espectro.doOffset    = this.chkOffset.isSelected();
    this.espectro.doBaseline  = this.chkBaseLine.isSelected();
    this.espectro.doPeaks     = this.chkPeakDetect.isSelected();
    this.espectro.doSmoothing = this.comSmoothing.getSelectedIndex();
    this.espectro.doRange     = this.chkRange.isSelected();

    this.espectro.offsetFilter.setDesp( this.valOffset.getValue() / 100.0 );

    this.espectro.rangeClipFilter.lambda_min = this.valRangeMin.getValue();
    this.espectro.rangeClipFilter.lambda_max = this.valRangeMax.getValue();

    this.espectro.normalizeFilter.setMode(this.normalizeMode.getSelectedIndex());
    this.espectro.normalizeFilter.lambda_min = this.valNormalizeMin.getValue();
    this.espectro.normalizeFilter.lambda_max = this.valNormalizeMax.getValue();

    this.espectro.smoothingFilter.setType(this.comSmoothing.getSelectedIndex());
    this.espectro.smoothingFilter.k = this.valSmooth.getValue();

    this.espectro.baselineFilter.iterations = this.valBaseLine.getValue();

    this.espectro.peakDetectionFilter.altura = this.valPeakDetect.getValue() / 400.0D;

    // XXX FIXME!: estas son usadas, entre otras, por PeriodicTableIFrame para buscar elementos.
    this.spectraAux = this.espectro.process();
    this.spectraPeak = this.espectro.getPeaks();
    this.spectraFilter = this.spectraAux;
  }

  private void setDefaultValues()
  {
    this.chkPeaksVisibility.setSelected(this.graficaEspectro.peaksVisible);
    this.chkLabelsVisibility.setSelected(this.graficaEspectro.labelsVisible);

    this.btnPeaksColor.setBackground(this.graficaEspectro.getPeaksColor());
    this.btnSpectraColor.setBackground(this.graficaEspectro.getSpectraColor());

    this.chkOffset.setSelected(this.espectro.doOffset);
    this.chkBaseLine.setSelected(this.espectro.doBaseline);
    this.chkPeakDetect.setSelected(this.espectro.doPeaks);
    this.comSmoothing.setSelectedIndex(this.espectro.doSmoothing);
    this.normalizeMode.setSelectedIndex((int)this.espectro.normalizeFilter.mode.ordinal());

    this.chkRange.setSelected(this.espectro.doRange);
    this.valRangeMin.setValue(this.espectro.rangeClipFilter.lambda_min);
    this.valRangeMax.setValue(this.espectro.rangeClipFilter.lambda_max);
    this.lbLambdaMin.setText( String.format("%d nm.", this.espectro.rangeClipFilter.lambda_min) );
    this.lbLambdaMax.setText( String.format("%d nm.", this.espectro.rangeClipFilter.lambda_max) );

    this.valNormalizeMin.setValue(this.espectro.normalizeFilter.lambda_min);
    this.valNormalizeMax.setValue(this.espectro.normalizeFilter.lambda_max);

    this.entOffset.setText( String.format("%.2f nm.", this.espectro.offsetFilter.desp*10) );
    this.valOffset.setValue((int)(this.espectro.offsetFilter.desp * 100));
    this.valSmooth.setValue((int)(this.espectro.smoothingFilter.k));
    this.valBaseLine.setValue((int)(this.espectro.baselineFilter.iterations));
    this.valPeakDetect.setValue((int)(this.espectro.peakDetectionFilter.altura * 400.0D));
  }

  private void addEventHandlers()
  {
    this.chkPeaksVisibility.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TasksSpectraIFrame.this.graficaEspectro.setPeaksVisibility(((JCheckBox) evt.getSource()).isSelected());
      }
    });

    this.chkLabelsVisibility.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TasksSpectraIFrame.this.graficaEspectro.setLabelsVisibility(((JCheckBox) evt.getSource()).isSelected());
      }
    });

    this.btnPeaksColor.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
            Color c = JColorChooser.showDialog(null, "Seleccione un color", Color.BLACK);
            if (c != null) {
                TasksSpectraIFrame.this.graficaEspectro.setPeaksColor(c);
            }
        }
    });

    this.btnSpectraColor.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
            Color c = JColorChooser.showDialog(null, "Seleccione un color", Color.BLACK);
            if (c != null) {
                TasksSpectraIFrame.this.graficaEspectro.setSpectraColor(c);
            }
        }
    });

    this.comSmoothing.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        if (TasksSpectraIFrame.this.comSmoothing.getSelectedIndex() == 1) {
          TasksSpectraIFrame.this.valSmooth.setMaximum(10);
          TasksSpectraIFrame.this.valSmooth.setMinimum(1);
        } else if (TasksSpectraIFrame.this.comSmoothing.getSelectedIndex() == 2) {
          TasksSpectraIFrame.this.valSmooth.setMaximum(5);
          TasksSpectraIFrame.this.valSmooth.setMinimum(2);
        }
        TasksSpectraIFrame.this.tasksValues();
      }
    });

    this.valSmooth.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        if (TasksSpectraIFrame.this.comSmoothing.getSelectedIndex() > 0)
          TasksSpectraIFrame.this.tasksValues();
      }
    });

    this.valBaseLine.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        TasksSpectraIFrame.this.tasksValues();
      }
    });

    this.valPeakDetect.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        TasksSpectraIFrame.this.tasksValues();
      }
    });

    this.chkBaseLine.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TasksSpectraIFrame.this.tasksValues();
      }
    });

    this.chkPeakDetect.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TasksSpectraIFrame.this.tasksValues();
      }
    });

    this.chkOffset.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TasksSpectraIFrame.this.tasksValues();
      }
    });

    this.valOffset.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        BoundedRangeModel m = (BoundedRangeModel)((JSlider) e.getSource()).getModel();
        double value = m.getValue() / 100.0;

        TasksSpectraIFrame.this.entOffset.setText( String.format("%.2f nm.", value) );

        if (TasksSpectraIFrame.this.chkOffset.isSelected()) {
          TasksSpectraIFrame.this.tasksValues();
        }
      }
    });

    this.normalizeMode.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TasksSpectraIFrame.this.tasksValues();
      }
    });

    this.valNormalizeMin.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        TasksSpectraIFrame.this.tasksValues();
      }
    });

    this.valNormalizeMax.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        TasksSpectraIFrame.this.tasksValues();
      }
    });

    this.chkRange.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TasksSpectraIFrame.this.tasksValues();
      }
    });

    this.valRangeMin.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        int value = ((JSlider) e.getSource()).getValue();

        TasksSpectraIFrame.this.lbLambdaMin.setText( String.format("%d nm.", value) );

        if (TasksSpectraIFrame.this.chkRange.isSelected()) {
          TasksSpectraIFrame.this.tasksValues();
        }
      }
    });

    this.valRangeMax.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        int value = ((JSlider) e.getSource()).getValue();

        TasksSpectraIFrame.this.lbLambdaMax.setText( String.format("%d nm.", value) );

        if (TasksSpectraIFrame.this.chkRange.isSelected()) {
          TasksSpectraIFrame.this.tasksValues();
        }
      }
    });

  }

  private String trunkValue(double value)
  {
    double aux = Math.rint(value * 10.0D) / 10.0D;
    String val = String.valueOf(aux);
    return val;
  }
}

/* Location:           /home/pardo/Desktop/material_beca_ciop/LIBS Analyzer/LIBS Analyzer Linux/LIBSAnalyzer.jar
 * Qualified Name:     IFrames.TasksSpectraIFrame
 * JD-Core Version:    0.6.2
 */
