package IFrames;

import java.awt.Font;
import javax.swing.JDesktopPane;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.FlowLayout;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.Component;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Color;
import javax.swing.BoxLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class AboutCiop extends JDialog
{

  JDesktopPane deskGraph = new JDesktopPane();

  public AboutCiop()
  {
      this(false);
  }

  public AboutCiop(boolean isSplash)
  {
      setResizable(false);
      getContentPane().setBackground(Color.WHITE);
      setSize(new Dimension(950, 630));
      setModalityType(ModalityType.APPLICATION_MODAL);
      initComponents();
      orderComponents();

      if (isSplash) {
        new java.util.Timer().schedule(
          new java.util.TimerTask() {
            @Override
            public void run() {
              AboutCiop.this.dispose();
            }
          },
          60 * 1000
        );
      }
  }

  private void initComponents() {
    setTitle("Acerca de");
  }

  private void orderComponents()
  {
    setSize(950, 600);
    setLocationRelativeTo(null);
    getContentPane().setLayout(null);
    deskGraph.setBounds(0, 0, 930, 600);

    getContentPane().add(this.deskGraph);
    
    ImageIcon logoCicata = new ImageIcon(AboutCiop.class.getResource("/ImagesSys/logo_cicata.png"));
    ImageIcon logoCiop   = new ImageIcon(AboutCiop.class.getResource("/ImagesSys/logo_ciop.png"));
        deskGraph.setLayout(null);
        JLabel lblLogoCicata = new JLabel(logoCicata);
        lblLogoCicata.setLocation(0, 0);
        lblLogoCicata.setSize(new Dimension(930, 119));
            deskGraph.add(lblLogoCicata);
    
        JLabel label = new JLabel();
        label.setFont(new Font("Dialog", Font.BOLD, 14));
        label.setLocation(0, 144);
        label.setVerticalTextPosition(SwingConstants.TOP);
        label.setVerticalAlignment(SwingConstants.TOP);
        label.setText("<html> <center> <H2>PROGRAMA DE IDENTIFICACIÓN DE <br /> LÍNEAS DE EMISIÓN PARA ANÁLISIS DE <br /> ESPECTROS LIBS</H2> <br /> Version 2.0</center><br />Desarrollado por: <br /> <br /><center>Grupo de Tecnología Láser, Instituto Politecnico Nacional  <br /> CICATA, Altamira-México <br /> <br />Laboratorio de Ablación, Limpieza y Restauración con Láser <br /> Centro de Investigaciones Ópticas (CONICET-CIC-UNLP) <br /></center> <br /> Programadores: <br /> &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; MTA. Zeferino Pérez Báes &nbsp; -  &nbsp; Adrián Pardini  </html>");
        label.setSize(new Dimension(930, 358));
        label.setMaximumSize(new Dimension(460, 0));
        label.setHorizontalTextPosition(SwingConstants.LEFT);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setAutoscrolls(true);
        label.setAlignmentY(0.0f);
        label.setAlignmentX(0.5f);
        deskGraph.add(label);
    JLabel lblLogoCiop   = new JLabel(logoCiop);
    lblLogoCiop.setAlignmentX(Component.CENTER_ALIGNMENT);
    lblLogoCiop.setBounds(0, 480, 930, 84);
    deskGraph.add(lblLogoCiop);
  }
}

/* Location:           /home/pardo/Desktop/material_beca_ciop/LIBS Analyzer/LIBS Analyzer Linux/LIBSAnalyzer.jar
 * Qualified Name:     IFrames.AboutIFrame
 * JD-Core Version:    0.6.2
 */
