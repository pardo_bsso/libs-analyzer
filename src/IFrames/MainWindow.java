package IFrames;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JSplitPane;
import javax.swing.JDialog;
import java.awt.BorderLayout;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import javax.swing.JToolBar;
import javax.swing.ListModel;
import javax.swing.DefaultListModel;

import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;

import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

import java.io.IOException;
import java.io.File;
import java.util.List;
import java.util.Vector;
import java.util.HashMap;
import java.util.Map;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import org.jfree.chart.ChartPanel;

import Algoritmos.TxtManager;
import Algoritmos.Espectro;
import Algoritmos.EspectroIdentificado;

import Algoritmos.CreateGraphic;
import Algoritmos.SeriesDatos;
import Algoritmos.GraficaEspectro;

import Algoritmos.CSVExport;

import IFrames.TasksSpectraIFrame;
import IFrames.PeriodicTableIFrame;
import IFrames.AboutCiop;

import javax.swing.border.BevelBorder;
import java.awt.FlowLayout;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.Box;
import javax.swing.SwingConstants;
import javax.swing.event.MenuListener;
import javax.swing.event.MenuEvent;
import javax.swing.JCheckBoxMenuItem;

public class MainWindow {

    private JFrame frmLibsAnalyzerciop;
    private JSplitPane splitPane;
    private JPanel panel;
    private JMenuBar menuBar;
    private JToolBar toolBar;
    private JScrollPane scrollPane;
    private JList listEspectros;
    private JList listIdentificados;
    private Vector<GraficaEspectro> listGraficaEspectros;
    public Vector<GraficaEspectro> listGraficaEspectrosIdentificados;
    private JLabel lblNewLabel;
    private JButton btnCargarEspectro;

    public ChartPanel chartPanel;
    public static SeriesDatos sd = new SeriesDatos();
    public static CreateGraphic chart = new CreateGraphic();

    private boolean espectrosLabelsVisible = true;
    private boolean espectrosPeaksVisible  = true;
    private boolean identificadosLabelsVisible = true;
    private boolean identificadosPeaksVisible  = true;

    private JFileChooser fcImportar;
    private JFileChooser fcImageExport;
    private JFileChooser fcPDFExport;
    private JFileChooser fcCSVExport;

    private JPanel panelChart;

    private ChangeListener espectroListener;
    private ChangeListener identificacionListener;
    private JButton btnExportarImagen;
    private JMenu mnArchivo;
    private JMenuItem mntmImportarTabla;
    private JMenuItem mntmExportarJpg;
    private JMenuItem mntmExportarPdf;
    private JButton btnIdentificar;
    private Box verticalBox;
    private JLabel lblNewLabel_1;
    private JScrollPane scrollPane_1;
    private JLabel lblIdentificados;
    private JMenuItem mntmExportarCSV;
    private JMenu mnVer;
    private JMenu mnAyuda;
    private JMenuItem mntmAcercaDeEste;
    private JCheckBoxMenuItem chkEspectroPeaks;
    private JCheckBoxMenuItem chkEspectroLabels;
    private JLabel lblGeneral;
    private JCheckBoxMenuItem chckbxmntmMostrarLeyenda;
    private JButton btnPropiedades;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MainWindow window = new MainWindow();
                    window.frmLibsAnalyzerciop.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public MainWindow() {
        initialize();
        this.frmLibsAnalyzerciop.setExtendedState( this.frmLibsAnalyzerciop.getExtendedState()|JFrame.MAXIMIZED_BOTH );
        espectroListener = new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                Espectro E = (Espectro) e.getSource();
                MainWindow.this.renderEspectro(E);
            }
        };
        identificacionListener = new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                PeriodicTableIFrame perTable = (PeriodicTableIFrame) e.getSource();
                MainWindow.this.actualizarElementosIdentificados(perTable);
            }
        };

        AboutCiop dlg = new AboutCiop(true);
        dlg.show();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmLibsAnalyzerciop = new JFrame();
        frmLibsAnalyzerciop.setTitle("LIBS Analyzer (CIOP)");
        frmLibsAnalyzerciop.setBounds(100, 100, 675, 450);
        frmLibsAnalyzerciop.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmLibsAnalyzerciop.getContentPane().setLayout(new BorderLayout(0, 0));

        splitPane = new JSplitPane();
        splitPane.setResizeWeight(0.25);
        splitPane.setDividerSize(20);
        splitPane.setOneTouchExpandable(true);
        frmLibsAnalyzerciop.getContentPane().add(splitPane, BorderLayout.CENTER);
        verticalBox = Box.createVerticalBox();

        scrollPane = new JScrollPane();
        verticalBox.add(scrollPane);

        listEspectros = new JList(new DefaultListModel<Espectro>());
        listEspectros.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JList list = (JList)e.getSource();
                if (e.getClickCount() == 2) {
                    List<Espectro> espectros = list.getSelectedValuesList();
                    if (!espectros.isEmpty()) {
                        MainWindow.this.editarEspectro(espectros.get(0));
                    }
                }
            }
        });

        listEspectros.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                    JList list = (JList)e.getSource();
                    List<Espectro> espectros = list.getSelectedValuesList();
                    if (!espectros.isEmpty()) {
                        Espectro E = espectros.get(0);
                        MainWindow.this.eliminarEspectro(E);
                    }
                }
            }
        });

        listIdentificados = new JList(new DefaultListModel<EspectroIdentificado>());
        listIdentificados.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                    JList list = (JList)e.getSource();
                    List<EspectroIdentificado> espectros = list.getSelectedValuesList();
                    if (!espectros.isEmpty()) {
                        EspectroIdentificado E = espectros.get(0);
                        MainWindow.this.eliminarEspectro(E);
                    }
                }
            }
        });

        listIdentificados.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JList list = (JList)e.getSource();
                if (e.getClickCount() == 2) {
                    List<Espectro> espectros = list.getSelectedValuesList();
                    if (!espectros.isEmpty()) {
                        MainWindow.this.editarEspectro(espectros.get(0));
                    }
                }
            }
        });

        scrollPane.setViewportView(listEspectros);

        lblNewLabel = new JLabel("Espectros");
        scrollPane.setColumnHeaderView(lblNewLabel);

        scrollPane_1 = new JScrollPane();
        verticalBox.add(scrollPane_1);

        scrollPane_1.setViewportView(listIdentificados);

        splitPane.setLeftComponent(verticalBox);

        lblIdentificados = new JLabel("Elementos Identificados");
        lblIdentificados.setVerticalAlignment(SwingConstants.TOP);
        scrollPane_1.setColumnHeaderView(lblIdentificados);

        listGraficaEspectros = new Vector<GraficaEspectro>();
        listGraficaEspectrosIdentificados = new Vector<GraficaEspectro>();

        panelChart = new JPanel();
        panelChart.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        splitPane.setRightComponent(panelChart);
        panelChart.setLayout(new BorderLayout(0, 0));

        chartPanel = new ChartPanel(chart.creaChart());

        chartPanel.setLayout(new BorderLayout(0, 0));
        panelChart.add(chartPanel);

        panel = new JPanel();
        panel.setAlignmentY(Component.TOP_ALIGNMENT);
        panel.setAlignmentX(Component.LEFT_ALIGNMENT);
        frmLibsAnalyzerciop.getContentPane().add(panel, BorderLayout.NORTH);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        menuBar = new JMenuBar();
        menuBar.setAlignmentX(Component.LEFT_ALIGNMENT);
        panel.add(menuBar);

        mnArchivo = new JMenu("Archivo");
        menuBar.add(mnArchivo);

        mntmImportarTabla = new JMenuItem("Importar tabla");
        mntmImportarTabla.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainWindow.this.importarTxt();
            }
        });
        mnArchivo.add(mntmImportarTabla);

        mntmExportarJpg = new JMenuItem("Exportar JPG / PNG");
        mntmExportarJpg.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainWindow.this.exportarImagen();
            }
        });
        mnArchivo.add(mntmExportarJpg);

        mntmExportarPdf = new JMenuItem("Exportar PDF");
        mntmExportarPdf.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainWindow.this.exportarPDF();
            }
        });
        mnArchivo.add(mntmExportarPdf);
        
        mntmExportarCSV = new JMenuItem("Exportar todos los Espectros a CSV/TXT");
        mntmExportarCSV.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainWindow.this.exportarCSV();
            }
        });
        mnArchivo.add(mntmExportarCSV);
        
        mnVer = new JMenu("Ver");
        menuBar.add(mnVer);
        
        lblGeneral = new JLabel("General");
        mnVer.add(lblGeneral);
        
        chckbxmntmMostrarLeyenda = new JCheckBoxMenuItem("Mostrar leyenda");
        chckbxmntmMostrarLeyenda.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainWindow.this.chart.setLegendVisibility( ((JCheckBoxMenuItem)e.getSource()).isSelected() );
            }
        });
        chckbxmntmMostrarLeyenda.setSelected(true);
        mnVer.add(chckbxmntmMostrarLeyenda);
        
        JLabel lblEspectros = new JLabel("Espectros");
        mnVer.add(lblEspectros);
        
        chkEspectroPeaks = new JCheckBoxMenuItem("Mostrar marcadores de Picos");
        chkEspectroPeaks.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainWindow.this.espectrosPeaksVisible = ((JCheckBoxMenuItem)e.getSource()).isSelected();
                MainWindow.this.actualizarVisibilidadElementos();
        
            }
        });
        chkEspectroPeaks.setSelected(true);
        mnVer.add(chkEspectroPeaks);
        
        chkEspectroLabels = new JCheckBoxMenuItem("Mostrar texto de Picos");
        chkEspectroLabels.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainWindow.this.espectrosLabelsVisible = ((JCheckBoxMenuItem)e.getSource()).isSelected();
                MainWindow.this.actualizarVisibilidadElementos();
        
            }
        });
        chkEspectroLabels.setSelected(true);
        mnVer.add(chkEspectroLabels);
        
        JLabel lblElementosIdentificados = new JLabel("Elementos Identificados");
        mnVer.add(lblElementosIdentificados);
        
        JCheckBoxMenuItem chkIdentificadosPeaks = new JCheckBoxMenuItem("Mostrar marcadores de Picos");
        chkIdentificadosPeaks.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainWindow.this.identificadosPeaksVisible = ((JCheckBoxMenuItem)e.getSource()).isSelected();
                MainWindow.this.actualizarVisibilidadElementos();
            }
        });
        chkIdentificadosPeaks.setSelected(true);
        mnVer.add(chkIdentificadosPeaks);
        
        JCheckBoxMenuItem chkIdentificadosLabels = new JCheckBoxMenuItem("Mostrar texto de Picos");
        chkIdentificadosLabels.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainWindow.this.identificadosLabelsVisible = ((JCheckBoxMenuItem)e.getSource()).isSelected();
                MainWindow.this.actualizarVisibilidadElementos();
            }
        });
        chkIdentificadosLabels.setSelected(true);
        mnVer.add(chkIdentificadosLabels);
        
        mnAyuda = new JMenu("Ayuda");
        menuBar.add(mnAyuda);
        
        mntmAcercaDeEste = new JMenuItem("Acerca de este programa");
        mntmAcercaDeEste.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AboutCiop dlg = new AboutCiop();
                dlg.show();
            }
        });
        mnAyuda.add(mntmAcercaDeEste);

        toolBar = new JToolBar();
        toolBar.setAlignmentX(Component.LEFT_ALIGNMENT);
        panel.add(toolBar);

        btnCargarEspectro = new JButton("");
        btnCargarEspectro.setToolTipText("Importar desde CSV/TXT");
        btnCargarEspectro.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                MainWindow.this.importarTxt();
            }
        });
        btnCargarEspectro.setIcon(new ImageIcon(MainWindow.class.getResource("/ImagesSys/ImportT.png")));
        toolBar.add(btnCargarEspectro);

        btnExportarImagen = new JButton("");
        btnExportarImagen.setToolTipText("Exportar como imagen");
        btnExportarImagen.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                MainWindow.this.exportarImagen();
            }
        });
        btnExportarImagen.setIcon(new ImageIcon(MainWindow.class.getResource("/ImagesSys/ExportGraf.png")));
        toolBar.add(btnExportarImagen);

        btnIdentificar = new JButton("");
        btnIdentificar.setToolTipText("Identificar elementos");
        btnIdentificar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JList list = MainWindow.this.getEspectrosList();
                List<Espectro> espectros = list.getSelectedValuesList();
                if (!espectros.isEmpty()) {
                    MainWindow.this.identificarElementos(espectros.get(0));
                } else {
                    JOptionPane.showMessageDialog(frmLibsAnalyzerciop,
                        "Seleccione un espectro de la lista",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        btnIdentificar.setIcon(new ImageIcon(MainWindow.class.getResource("/ImagesSys/system-search.png")));
        toolBar.add(btnIdentificar);

        btnPropiedades = new JButton("");
        btnPropiedades.setToolTipText("Propiedades del gráfico");
        btnPropiedades.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                MainWindow.this.chartPanel.doEditChartProperties();
            }
        });
        btnPropiedades.setIcon(new ImageIcon(MainWindow.class.getResource("/ImagesSys/Engrane.png")));
        toolBar.add(btnPropiedades);

        JButton btnZoomIn = new JButton("");
        btnZoomIn.setToolTipText("Acercar");
        btnZoomIn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                MainWindow.this.chartPanel.zoomInBoth(0.5,0.5);
            }
        });
        btnZoomIn.setIcon(new ImageIcon(MainWindow.class.getResource("/ImagesSys/zoom-in.png")));
        toolBar.add(btnZoomIn);

        JButton btnZoomOut = new JButton("");
        btnZoomOut.setToolTipText("Alejar");
        btnZoomOut.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                MainWindow.this.chartPanel.zoomOutBoth(0.5,0.5);
            }
        });
        btnZoomOut.setIcon(new ImageIcon(MainWindow.class.getResource("/ImagesSys/zoom-out.png")));
        toolBar.add(btnZoomOut);

        JButton btnZoomOriginal = new JButton("");
        btnZoomOriginal.setToolTipText("Mostrar todo");
        btnZoomOriginal.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                MainWindow.this.chartPanel.restoreAutoBounds();
            }
        });
        btnZoomOriginal.setIcon(new ImageIcon(MainWindow.class.getResource("/ImagesSys/zoom-original.png")));
        toolBar.add(btnZoomOriginal);
    }

    public JSplitPane getSplitPane() {
        return splitPane;
    }

    public JList getEspectrosList() {
        return listEspectros;
    }

    public void actualizarVisibilidadElementos () {
        for (GraficaEspectro g: MainWindow.this.listGraficaEspectros) {
            g.setLabelsVisibility(this.espectrosLabelsVisible);
            g.setPeaksVisibility (this.espectrosPeaksVisible);
        }

        for (GraficaEspectro g: MainWindow.this.listGraficaEspectrosIdentificados) {
            g.setLabelsVisibility(this.identificadosLabelsVisible);
            g.setPeaksVisibility (this.identificadosPeaksVisible);
        }

    }

    public void identificarElementos (Espectro e) {
        for (GraficaEspectro g: listGraficaEspectros) {
            g.hidePeaks();
            g.hideLabels();
        }

        PeriodicTableIFrame perTable = new PeriodicTableIFrame(e);
        perTable.addChangeListener(this.identificacionListener);
        perTable.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                for (GraficaEspectro g: MainWindow.this.listGraficaEspectros) {
                    g.showPeaks();
                    g.showLabels();
                }
            }

            @Override
            public void windowClosing(WindowEvent e) {
                this.windowClosed(e);
            }

        });
        perTable.show();
    }

    public void eliminarEspectro (Espectro e) {
        DefaultListModel<Espectro> model = (DefaultListModel<Espectro>) this.listEspectros.getModel();
        model.removeElement(e);

        DefaultListModel<EspectroIdentificado> modelI = (DefaultListModel<EspectroIdentificado>) this.listIdentificados.getModel();
        modelI.removeElement(e);

        for (GraficaEspectro g: listGraficaEspectros) {
            if (g.getEspectro() == e) {
                chart.removeGraficaEspectro(g);
                listGraficaEspectros.remove(g);
                return;
            }
        }
        for (GraficaEspectro g: listGraficaEspectrosIdentificados) {
            if (g.getEspectro() == e) {
                chart.removeGraficaEspectro(g);
                listGraficaEspectrosIdentificados.remove(g);
                return;
            }
        }
    }

    public void editarEspectro (Espectro e) {
        for (GraficaEspectro g: listGraficaEspectros) {
            if (g.getEspectro() == e) {
                TasksSpectraIFrame tasksSpectra = new TasksSpectraIFrame(e, g);
                tasksSpectra.show();
                return;
            }
        }

        for (GraficaEspectro g: listGraficaEspectrosIdentificados) {
            if (g.getEspectro() == e) {
                TasksSpectraIFrame tasksSpectra = new TasksSpectraIFrame(e, g, true);
                tasksSpectra.show();
                return;
            }
        }
    }

    public void actualizarElementosIdentificados (PeriodicTableIFrame P) {
        DefaultListModel<EspectroIdentificado> model = (DefaultListModel<EspectroIdentificado>) this.listIdentificados.getModel();

        // XXX: eliminarEspectro modifica la lista.
        Vector<EspectroIdentificado> espectrosViejos = new Vector<EspectroIdentificado>();
        for (Enumeration<EspectroIdentificado> eEI = model.elements(); eEI.hasMoreElements(); ) {
            EspectroIdentificado e = eEI.nextElement();
            espectrosViejos.add(e);
        }
        for (EspectroIdentificado e: espectrosViejos) {
            this.eliminarEspectro(e);
        }


        for (Map.Entry<String, EspectroIdentificado> entry : P.espectrosIdentificados.entrySet()) {
            String key = entry.getKey();
            EspectroIdentificado E = entry.getValue();

            if (model.indexOf(E) == -1) {
                model.addElement(E);

                GraficaEspectro g = new GraficaEspectro(E, "(" + key + ") ", "");
                listGraficaEspectrosIdentificados.addElement(g);

                g.setLabelsVisibility(this.identificadosLabelsVisible);
                g.setPeaksVisibility (this.identificadosPeaksVisible);

                chart.addGraficaEspectro(g);
                g.render();
            }
        }
    }

    public void renderEspectro (Espectro e) {
        String[][] labelPeak;

        sd.setLine(1);
        sd.setSpectraGraph(e.spectra);
        sd.addSpectra();

        double[][] spectraPeak = e.getPeaks();

        sd.setSpectraPeak(spectraPeak);
        sd.addPeak();

        if (e.doPeaks) {
          labelPeak = new String[spectraPeak.length][3];
          for (int i = 0; i < spectraPeak.length; i++) {
            for (int j = 0; j < 2; j++) {
              labelPeak[i][j] = String.valueOf(spectraPeak[i][j]);
            }

            labelPeak[i][2] = String.valueOf(Math.rint(spectraPeak[i][0]*10)/10) + " nm";
          }
          chart.setLabels(labelPeak, 25);
          chart.viewLabels();
          sd.addPeak();
        } else {
          chart.clearLabels();
          sd.removePeaks();
        }
    }

    public void importarTxt () {
        JFileChooser fc = this.fcImportar;
        int respMsj = 0;

        if (this.fcImportar == null) {
           fc = this.fcImportar = new JFileChooser();
           fc.setDialogTitle("Importar Tabla");

           fc.setAcceptAllFileFilterUsed(false);
           fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
           fc.setFileFilter( new FileNameExtensionFilter("Archivos csv/txt", "txt",  "csv") );
        }

        respMsj = fc.showOpenDialog(null);
        if (respMsj == JFileChooser.APPROVE_OPTION) {
            FileNameExtensionFilter filter = (FileNameExtensionFilter) fc.getFileFilter();

            String extension = "." + filter.getExtensions()[0];
            String filename;
            try {
                filename = fc.getSelectedFile().getCanonicalPath();
                if (!filename.toLowerCase().endsWith(extension)) {
                  filename = filename + extension;
                }

                TxtManager tableTxt = new TxtManager();
                tableTxt.setInputFile(String.valueOf(filename));

                Espectro e = new Espectro( tableTxt.read() );
                e.name = fc.getSelectedFile().getName();

                DefaultListModel<Espectro> model = (DefaultListModel<Espectro>) this.listEspectros.getModel();
                model.addElement(e);

                e.process();
                GraficaEspectro g = new GraficaEspectro(e);
                listGraficaEspectros.addElement(g);

                g.setLabelsVisibility(this.espectrosLabelsVisible);
                g.setPeaksVisibility (this.espectrosPeaksVisible);


                chart.addGraficaEspectro(g);
                g.render();

                this.editarEspectro(e);

            } catch (java.io.IOException e) {
              System.out.println("importarTxt(): IOException");
              return;
            }
        }
    }

    public void exportarPDF () {
        JFileChooser fc = this.fcPDFExport;
        int respMsj = 0;

        SimpleDateFormat format = new SimpleDateFormat("yyyymmdd");
        String now = format.format(new Date());
        String template = "captura_espectro_" + now;

        if (this.fcPDFExport == null) {
            fc = this.fcPDFExport = new JFileChooser();
            fc.setDialogTitle("Exportar Grafica");

            fc.setAcceptAllFileFilterUsed(false);
            fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fc.setFileFilter( new FileNameExtensionFilter("Archivos PDF", "pdf") );
        }

        fc.setSelectedFile( new File(template));

        respMsj = fc.showSaveDialog(null);

        if (respMsj == JFileChooser.APPROVE_OPTION) {

            FileNameExtensionFilter filter = (FileNameExtensionFilter) fc.getFileFilter();

            String extension = "." + filter.getExtensions()[0];
            String filename;
            try {
              filename = fc.getSelectedFile().getCanonicalPath();
            } catch (java.io.IOException e) {
              System.out.println("methodExportPDF(): IOException");
              return;
            }

            if (!filename.toLowerCase().endsWith(extension)) {
              filename = filename + extension;
            }

            File target = new File(filename);

            chart.writeChartToPDF(this.chartPanel.getChart(), target);
        }
    }

    public void exportarImagen () {
        JFileChooser fc = this.fcImageExport;
        int respMsj = 0;

        SimpleDateFormat format = new SimpleDateFormat("yyyymmdd");
        String now = format.format(new Date());
        String template = "captura_espectro_" + now;

        if (this.fcImageExport == null) {
            fc = this.fcImageExport = new JFileChooser();
            fc.setDialogTitle("Exportar Grafica");

            fc.setAcceptAllFileFilterUsed(false);
            fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fc.setFileFilter( new FileNameExtensionFilter("Imágenes PNG", "png") );
            fc.setFileFilter( new FileNameExtensionFilter("Imágenes JPG", "jpg") );
        }

        fc.setSelectedFile( new File(template));

        respMsj = fc.showSaveDialog(null);

        if (respMsj == JFileChooser.APPROVE_OPTION) {

            FileNameExtensionFilter filter = (FileNameExtensionFilter) fc.getFileFilter();

            String extension = "." + filter.getExtensions()[0];
            String filename;
            try {
              filename = fc.getSelectedFile().getCanonicalPath();
            } catch (java.io.IOException e) {
              System.out.println("methodExportGraphic(): IOException");
              return;
            }

            if (!filename.toLowerCase().endsWith(extension)) {
              filename = filename + extension;
            }

            File target = new File(filename);

            // Switch con String despues de Java SE7. Ha.
            if (extension.equals(".jpg")) {
              chart.exportJPEGGraph(target);
            }
            if (extension.equals(".png")) {
              chart.exportPNGGraph(target);
            }
        }
    }

    public void exportarCSV () {
        JFileChooser fc = this.fcCSVExport;
        int respMsj = 0;

        SimpleDateFormat format = new SimpleDateFormat("yyyymmdd");
        String now = format.format(new Date());
        String template = "espectro_procesado_" + now;

        if (this.fcCSVExport == null) {
            fc = this.fcCSVExport = new JFileChooser();
            fc.setDialogTitle("Exportar todos los Espectros a CSV/TXT");

            fc.setAcceptAllFileFilterUsed(true);
            fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        }

        fc.setFileFilter( new FileNameExtensionFilter("Archivos txt", "txt") );
        fc.setSelectedFile( new File(template));

        respMsj = fc.showSaveDialog(null);

        if (respMsj == JFileChooser.APPROVE_OPTION) {

            String filePrefix;
            try {
              filePrefix = fc.getSelectedFile().getCanonicalPath();
            } catch (java.io.IOException e) {
              System.out.println("exportarCSV(): IOException");
              JOptionPane.showMessageDialog(null, "Error " + e.toString(), "Error", 0);
              e.printStackTrace();
              return;
            }

            CSVExport exporter = new CSVExport();

            exporter.open(filePrefix + "_identificados.txt");

            for (GraficaEspectro g: this.listGraficaEspectrosIdentificados) {
                Espectro E = g.getEspectro();
                // String fileName = filePrefix + "_" + E.name + "_identificado";

                exporter.appendData(E.getPeaks(), E.name);
                // exporter.write(fileName + ".csv", E.getPeaks());
            }

            exporter.close();

            for (GraficaEspectro g: this.listGraficaEspectros) {
                Espectro E = g.getEspectro();
                String fileName = filePrefix + "_" + E.name;

                // exporter.appendData(E.spectra, null);
                exporter.write(fileName + ".txt", E.spectra);
                // if (E.doPeaks) {
                //     exporter.write(fileName + "_picos.csv", E.getPeaks());
                // }
            }

        }
    }
}
