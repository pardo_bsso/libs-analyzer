/*     */ package IFrames;
/*     */ 
/*     */ import Algoritmos.CreateGraphic;
/*     */ import Algoritmos.DBCnn;
/*     */ import Algoritmos.SeriesDatos;
/*     */ import java.awt.Font;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.ActionListener;
/*     */ import java.awt.event.KeyAdapter;
/*     */ import java.awt.event.KeyEvent;
/*     */ import java.text.DecimalFormat;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JDesktopPane;
/*     */ import javax.swing.JInternalFrame;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JOptionPane;
/*     */ import javax.swing.JTextField;
/*     */ import javax.swing.table.DefaultTableModel;
/*     */ import systemlibs.MDIFrame;
/*     */ 
/*     */ public class GraphicEspecIFrame extends JInternalFrame
/*     */ {
/*  30 */   JLabel lb1 = new JLabel();
/*  31 */   JLabel lb2 = new JLabel();
/*  32 */   JLabel lb3 = new JLabel();
/*  33 */   JLabel lb4 = new JLabel();
/*  34 */   JTextField txtIon = new JTextField();
/*  35 */   JTextField lambInf = new JTextField();
/*  36 */   JTextField lambSup = new JTextField();
/*  37 */   JButton buttGraphElement = new JButton();
/*  38 */   JDesktopPane deskGraph = new JDesktopPane();
/*     */   CreateGraphic graf;
/*     */   String ion;
/*     */   float lmbInf;
/*     */   float lmbSup;
/*     */   double[][] spectraAux;
/*     */   double[][] spectraPeak;
/*  46 */   DecimalFormat df = new DecimalFormat("#.##");
/*     */   double Inf;
/*     */   double Sup;
/*     */   String str;
/*     */   DefaultTableModel tbMod;
/*     */ 
/*     */   public GraphicEspecIFrame()
/*     */   {
/*  53 */     initComponents();
/*  54 */     orderComponents();
/*     */   }
/*     */ 
/*     */   private void initComponents() {
/*  58 */     setClosable(true);
/*  59 */     setTitle("Gráfica de Espectro");
/*     */ 
/*  61 */     this.lb1.setFont(new Font("Courier New", 0, 14));
/*  62 */     this.lb1.setText("Elemento:");
/*     */ 
/*  64 */     this.lb2.setFont(new Font("Courier New", 0, 14));
/*  65 */     this.lb2.setText("Lambda Inf:");
/*     */ 
/*  67 */     this.lb3.setFont(new Font("Courier New", 0, 14));
/*  68 */     this.lb3.setText("Lambda Sup:");
/*     */ 
/*  70 */     this.txtIon.setFont(new Font("Courier New", 0, 18));
/*  71 */     this.txtIon.setHorizontalAlignment(0);
/*     */ 
/*  73 */     this.lambInf.setFont(new Font("Courier New", 0, 18));
/*  74 */     this.lambInf.setHorizontalAlignment(0);
/*  75 */     this.lambInf.addKeyListener(new KeyAdapter()
/*     */     {
/*     */       public void keyTyped(KeyEvent evt)
/*     */       {
/*     */       }
/*     */     });
/*  81 */     this.lambSup.setFont(new Font("Courier New", 0, 18));
/*  82 */     this.lambSup.setHorizontalAlignment(0);
/*  83 */     this.lambSup.addKeyListener(new KeyAdapter()
/*     */     {
/*     */       public void keyTyped(KeyEvent evt)
/*     */       {
/*     */       }
/*     */     });
/*  89 */     this.buttGraphElement.setFont(new Font("Courier New", 0, 15));
/*  90 */     this.buttGraphElement.setText("Graficar Elemento");
/*  91 */     this.buttGraphElement.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/*  93 */         GraphicEspecIFrame.this.getDatsEspec();
/*     */       }
/*     */     });
/*     */   }
/*     */ 
/*     */   private void orderComponents()
/*     */   {
/* 102 */     setSize(220, 190);
/* 103 */     this.lb1.setBounds(10, 10, 100, 27);
/* 104 */     this.txtIon.setBounds(100, 10, 100, 27);
/* 105 */     this.lb2.setBounds(10, 45, 100, 27);
/* 106 */     this.lambInf.setBounds(100, 45, 100, 27);
/* 107 */     this.lb3.setBounds(10, 80, 100, 27);
/* 108 */     this.lambSup.setBounds(100, 80, 100, 27);
/* 109 */     this.buttGraphElement.setBounds(10, 115, 190, 30);
/*     */ 
/* 111 */     this.deskGraph.add(this.lb1);
/* 112 */     this.deskGraph.add(this.txtIon);
/* 113 */     this.deskGraph.add(this.lb2);
/* 114 */     this.deskGraph.add(this.lambInf);
/* 115 */     this.deskGraph.add(this.lb3);
/* 116 */     this.deskGraph.add(this.lambSup);
/* 117 */     this.deskGraph.add(this.buttGraphElement);
/* 118 */     add(this.deskGraph);
/*     */   }
/*     */ 
/*     */   private void getDatsEspec() {
/* 122 */     this.ion = this.txtIon.getText();
/* 123 */     if (this.lambInf.getText().isEmpty())
/* 124 */       this.lmbInf = 0.0F;
/*     */     else {
/* 126 */       this.lmbInf = Float.valueOf(this.lambInf.getText()).floatValue();
/*     */     }
/* 128 */     if (this.lambSup.getText().isEmpty())
/* 129 */       this.lmbSup = 0.0F;
/*     */     else {
/* 131 */       this.lmbSup = Float.valueOf(this.lambSup.getText()).floatValue();
/*     */     }
/* 133 */     if (this.txtIon.getText().isEmpty()) {
/* 134 */       JOptionPane.showMessageDialog(null, "Especifique un elemento", "Atención", 0);
/* 135 */       this.txtIon.requestFocus();
/* 136 */     } else if ((this.lmbInf > this.lmbSup) || (this.lmbInf == 0.0F) || (this.lmbSup == 0.0F)) {
/* 137 */       JOptionPane.showMessageDialog(null, "Verifique las longitudes de onda", "Atención", 0);
/* 138 */       this.lambInf.requestFocus();
/*     */     } else {
/* 140 */       DBCnn dbcon = new DBCnn();
/* 141 */       dbcon.setTbName(this.ion);
/* 142 */       dbcon.setLambdainf(Float.valueOf(this.lmbInf).floatValue());
/* 143 */       dbcon.setLambdasup(Float.valueOf(this.lmbSup).floatValue());
/* 144 */       dbcon.busqLines();
/* 145 */       this.spectraAux = dbcon.getSpectraAux();
/* 146 */       getGraphic();
/*     */     }
/*     */   }
/*     */ 
/*     */   private void cancel() {
/* 151 */     this.txtIon.setText("");
/* 152 */     this.lambInf.setText("");
/* 153 */     this.lambSup.setText("");
/*     */   }
/*     */ 
/*     */   private void getGraphic() {
/* 157 */     MDIFrame.sd.setLine(0);
/* 158 */     MDIFrame.sd.setSpectraGraph(this.spectraAux);
/* 159 */     MDIFrame.sd.addSpectra();
/*     */   }
/*     */ 
/*     */   public boolean isNumberFloat(String cadena) {
/*     */     try {
/* 164 */       Float.parseFloat(cadena);
/* 165 */       return true; } catch (NumberFormatException nfe) {
/*     */     }
/* 167 */     return false;
/*     */   }
/*     */ }

/* Location:           /home/pardo/Desktop/material_beca_ciop/LIBS Analyzer/LIBS Analyzer Linux/LIBSAnalyzer.jar
 * Qualified Name:     IFrames.GraphicEspecIFrame
 * JD-Core Version:    0.6.2
 */