/*      */ package IFrames;
/*      */ 
/*      */ import Algoritmos.CreateGraphic;
/*      */ import Algoritmos.DBCnn;
/*      */ import Algoritmos.SeriesDatos;

           import Algoritmos.Espectro;
           import Algoritmos.EspectroIdentificado;

/*      */ import java.awt.Color;
/*      */ import java.awt.Desktop;
/*      */ import java.awt.Font;
/*      */ import java.awt.event.ActionEvent;
/*      */ import java.awt.event.ActionListener;
/*      */ import java.awt.event.MouseAdapter;
/*      */ import java.awt.event.MouseEvent;
/*      */ import java.awt.event.MouseMotionAdapter;
/*      */ import java.io.File;
/*      */ import java.util.ArrayList;

           import java.util.HashMap;

           import java.util.Map;
           import java.util.Vector;

           import javax.swing.event.ChangeListener;
           import javax.swing.event.ChangeEvent;


/*      */ import javax.swing.BorderFactory;
/*      */ import javax.swing.JButton;
/*      */ import javax.swing.JCheckBox;
/*      */ import javax.swing.JDesktopPane;
           import javax.swing.JDialog;
/*      */ import javax.swing.JInternalFrame;
/*      */ import javax.swing.JLabel;
/*      */ import javax.swing.JOptionPane;
/*      */ import javax.swing.JSpinner;
/*      */ import javax.swing.SpinnerModel;
/*      */ import javax.swing.SpinnerNumberModel;
/*      */ import systemlibs.MDIFrame;
import javax.swing.SwingConstants;
/*      */ 
/*      */ public class PeriodicTableIFrame extends JDialog
/*      */ {

             public HashMap<String, EspectroIdentificado> espectrosIdentificados = new HashMap<String, EspectroIdentificado>();
             public EspectroIdentificado elementosIdentificados = new EspectroIdentificado();

             private Espectro espectro;

             DBCnn cnn;

             private Vector<ChangeListener> listenerList = new Vector<ChangeListener>();

/*   40 */   private final JDesktopPane deskTable = new JDesktopPane();
/*      */   private JLabel H;
/*      */   private JLabel He;
/*      */   private JLabel Li;
/*      */   private JLabel Be;
/*      */   private JLabel B;
/*      */   private JLabel C;
/*      */   private JLabel N;
/*      */   private JLabel O;
/*      */   private JLabel F;
/*      */   private JLabel Ne;
/*      */   private JLabel Na;
/*      */   private JLabel Mg;
/*      */   private JLabel Al;
/*      */   private JLabel Si;
/*      */   private JLabel P;
/*      */   private JLabel S;
/*      */   private JLabel Cl;
/*      */   private JLabel Ar;
/*      */   private JLabel K;
/*      */   private JLabel Ca;
/*      */   private JLabel Sc;
/*      */   private JLabel Ti;
/*      */   private JLabel V;
/*      */   private JLabel Cr;
/*      */   private JLabel Mn;
/*      */   private JLabel Fe;
/*      */   private JLabel Co;
/*      */   private JLabel Ni;
/*      */   private JLabel Cu;
/*      */   private JLabel Zn;
/*      */   private JLabel Ga;
/*      */   private JLabel Ge;
/*      */   private JLabel As;
/*      */   private JLabel Se;
/*      */   private JLabel Br;
/*      */   private JLabel Kr;
/*      */   private JLabel Rb;
/*      */   private JLabel Sr;
/*      */   private JLabel Y;
/*      */   private JLabel Zr;
/*      */   private JLabel Nb;
/*      */   private JLabel Tc;
/*      */   private JLabel Mo;
/*      */   private JLabel Ru;
/*      */   private JLabel Rh;
/*      */   private JLabel Pd;
/*      */   private JLabel Ag;
/*      */   private JLabel Cd;
/*      */   private JLabel In;
/*      */   private JLabel Sn;
/*      */   private JLabel Sb;
/*      */   private JLabel Te;
/*      */   private JLabel I;
/*      */   private JLabel Xe;
/*      */   private JLabel Cs;
/*      */   private JLabel Ba;
/*      */   private JLabel La;
/*      */   private JLabel Ce;
/*      */   private JLabel Pr;
/*      */   private JLabel Nd;
/*      */   private JLabel Pm;
/*      */   private JLabel Sm;
/*      */   private JLabel Eu;
/*      */   private JLabel Gd;
/*      */   private JLabel Tb;
/*      */   private JLabel Dy;
/*      */   private JLabel Ho;
/*      */   private JLabel Er;
/*      */   private JLabel Tm;
/*      */   private JLabel Yb;
/*      */   private JLabel Lu;
/*      */   private JLabel Hf;
/*      */   private JLabel Ta;
/*      */   private JLabel W;
/*      */   private JLabel Re;
/*      */   private JLabel Os;
/*      */   private JLabel Ir;
/*      */   private JLabel Pt;
/*      */   private JLabel Au;
/*      */   private JLabel Hg;
/*      */   private JLabel Tl;
/*      */   private JLabel Pb;
/*      */   private JLabel Bi;
/*      */   private JLabel Po;
/*      */   private JLabel At;
/*      */   private JLabel Rn;
/*      */   private JLabel Fr;
/*      */   private JLabel Ra;
/*      */   private JLabel Ac;
/*      */   private JLabel Th;
/*      */   private JLabel Pa;
/*      */   private JLabel U;
/*      */   private JLabel Np;
/*      */   private JLabel Pu;
/*      */   private JLabel Am;
/*      */   private JLabel Cm;
/*      */   private JLabel Bk;
/*      */   private JLabel Cf;
/*      */   private JLabel Es;
/*      */   private JLabel Fm;
/*      */   private JLabel Md;
/*      */   private JLabel No;
/*      */   private JLabel Lr;
/*      */   private JLabel Rf;
/*      */   private JLabel Db;
/*      */   private JLabel Sg;
/*      */   private JLabel Bh;
/*      */   private JLabel Hs;
/*      */   private JLabel Mt;
/*      */   private JLabel Ds;
/*      */   private JLabel Rg;
/*      */   private JLabel Cn;
/*      */   private JLabel Uut;
/*      */   private JLabel Fl;
/*      */   private JLabel Uup;
/*      */   private JLabel Lv;
/*      */   private JLabel Uus;
/*      */   private JLabel Uuo;
/*      */   private JLabel Lantanidos;
/*      */   private JLabel Actinidos;
/*   46 */   private final JCheckBox chkFileOpen = new JCheckBox();
/*   47 */   private final JCheckBox chkMultipleIonization = new JCheckBox();
/*   48 */   private final JCheckBox chkPersistenteLine = new JCheckBox();
/*      */ 
/*   50 */   private final JLabel toler = new JLabel();
/*   51 */   private final JButton buttIdentify = new JButton();
/*      */ 
/*   53 */   private final SpinnerModel spinnerModel = new SpinnerNumberModel(0.3D, 0.0D, 3.0D, 0.1D);
/*   54 */   private final JSpinner spinnerTolerance = new JSpinner(this.spinnerModel);
/*      */ 
/*   56 */   private final Font fnt = new Font("Calibri", 0, 12);
/*      */   private int f1;
/*      */   private int f2;
/*      */   private int f3;
/*      */   private int f4;
/*      */   private int f5;
/*      */   private int f6;
/*      */   private int f7;
/*      */   private int f8;
/*      */   private int f9;
/*      */   private int c1;
/*      */   private int c2;
/*      */   private int c3;
/*      */   private int c4;
/*      */   private int c5;
/*      */   private int c6;
/*      */   private int c7;
/*      */   private int c8;
/*      */   private int c9;
/*      */   private int c10;
/*      */   private int c11;
/*      */   private int c12;
/*      */   private int c13;
/*      */   private int c14;
/*      */   private int c15;
/*      */   private int c16;
/*      */   private int c17;
/*      */   private int c18;
/*      */   private int width;
/*      */   private int height;
/*      */   private int espH;
/*      */   private int espV;
/*   58 */   private final Color g0Color = new Color(220, 220, 255);
/*   59 */   private final Color g1Color = new Color(220, 255, 220);
/*   60 */   private final Color g2Color = new Color(255, 220, 200);
/*   61 */   private final Color g3Color = new Color(255, 220, 255);
/*   62 */   private final Color g4Color = new Color(255, 0, 0);
/*   63 */   private boolean fileElement = false;
/*      */   private boolean elementSelected;
/*      */   private ArrayList elemSelected;
/*      */   private ArrayList peakFind;
/*      */   public double[][] spectraPeakFind;
/*      */   public String[][] spectraFind;
/*      */   private float err;
/*   71 */   private boolean vH = false; private boolean vHe = false; private boolean vLi = false; private boolean vBe = false; private boolean vB = false; private boolean vC = false; private boolean vN = false; private boolean vO = false; private boolean vF = false; private boolean vNe = false; private boolean vNa = false; private boolean vMg = false; private boolean vAl = false; private boolean vSi = false; private boolean vP = false; private boolean vS = false; private boolean vCl = false; private boolean vAr = false; private boolean vK = false; private boolean vCa = false; private boolean vSc = false; private boolean vTi = false; private boolean vV = false; private boolean vCr = false; private boolean vMn = false; private boolean vFe = false; private boolean vCo = false; private boolean vNi = false; private boolean vCu = false; private boolean vZn = false; private boolean vGa = false; private boolean vGe = false; private boolean vAs = false; private boolean vSe = false; private boolean vBr = false; private boolean vKr = false; private boolean vRb = false; private boolean vSr = false; private boolean vY = false; private boolean vZr = false; private boolean vNb = false; private boolean vTc = false; private boolean vMo = false; private boolean vRu = false; private boolean vRh = false; private boolean vPd = false; private boolean vAg = false; private boolean vCd = false; private boolean vIn = false; private boolean vSn = false; private boolean vSb = false; private boolean vTe = false; private boolean vI = false; private boolean vXe = false; private boolean vCs = false; private boolean vBa = false; private boolean vLa = false; private boolean vCe = false; private boolean vPr = false; private boolean vNd = false; private boolean vPm = false; private boolean vSm = false; private boolean vEu = false; private boolean vGd = false; private boolean vTb = false; private boolean vDy = false; private boolean vHo = false; private boolean vEr = false; private boolean vTm = false; private boolean vYb = false; private boolean vLu = false; private boolean vHf = false; private boolean vTa = false; private boolean vW = false; private boolean vRe = false; private boolean vOs = false; private boolean vIr = false; private boolean vPt = false; private boolean vAu = false; private boolean vHg = false; private boolean vTl = false; private boolean vPb = false; private boolean vBi = false; private boolean vPo = false; private boolean vAt = false; private boolean vRn = false; private boolean vFr = false; private boolean vRa = false; private boolean vAc = false; private boolean vTh = false; private boolean vPa = false; private boolean vU = false; private boolean vNp = false; private boolean vPu = false; private boolean vAm = false; private boolean vCm = false; private boolean vBk = false; private boolean vCf = false; private boolean vEs = false; private boolean vFm = false; private boolean vMd = false; private boolean vNo = false; private boolean vLr = false; private boolean vRf = false; private boolean vDb = false; private boolean vSg = false; private boolean vBh = false; private boolean vHs = false; private boolean vMt = false; private boolean vDs = false; private boolean vRg = false; private boolean vCn = false; private boolean vUut = false; private boolean vFl = false; private boolean vUup = false; private boolean vLv = false; private boolean vUus = false; private boolean vUuo = false;

/*      */ 
/*      */   public PeriodicTableIFrame(Espectro E)
/*      */   {
/*   78 */     this.espectro = E;
/*   78 */     this.peakFind = new ArrayList();
/*   79 */     this.elemSelected = new ArrayList();
               this.elementosIdentificados.name = "Elementos Identificados";
/*   80 */     this.elementSelected = true;
/*   81 */     initComponents();
/*   82 */     orderComponents();
/*      */   }
/*      */   private void initComponents() {
               setModal(true);
               setAlwaysOnTop(true);
               setModalityType(ModalityType.APPLICATION_MODAL);
/*   86 */     setTitle("Tabla Periodica");
/*      */ 
/*   88 */     this.width = 28;
/*   89 */     this.height = 36;
/*   90 */     this.espH = 2;
/*   91 */     this.espV = 2;
/*      */ 
/*   93 */     this.f1 = 10;
/*   94 */     this.f2 = (this.f1 + this.height + this.espV);
/*   95 */     this.f3 = (this.f2 + this.height + this.espV);
/*   96 */     this.f4 = (this.f3 + this.height + this.espV);
/*   97 */     this.f5 = (this.f4 + this.height + this.espV);
/*   98 */     this.f6 = (this.f5 + this.height + this.espV);
/*   99 */     this.f7 = (this.f6 + this.height + this.espV);
/*  100 */     this.f8 = (this.f7 + this.height + this.espV + 10);
/*  101 */     this.f9 = (this.f8 + this.height + this.espV);
/*      */ 
/*  103 */     this.c1 = 10;
/*  104 */     this.c2 = (this.c1 + this.width + this.espH);
/*  105 */     this.c3 = (this.c2 + this.width + this.espH);
/*  106 */     this.c4 = (this.c3 + this.width + this.espH);
/*  107 */     this.c5 = (this.c4 + this.width + this.espH);
/*  108 */     this.c6 = (this.c5 + this.width + this.espH);
/*  109 */     this.c7 = (this.c6 + this.width + this.espH);
/*  110 */     this.c8 = (this.c7 + this.width + this.espH);
/*  111 */     this.c9 = (this.c8 + this.width + this.espH);
/*  112 */     this.c10 = (this.c9 + this.width + this.espH);
/*  113 */     this.c11 = (this.c10 + this.width + this.espH);
/*  114 */     this.c12 = (this.c11 + this.width + this.espH);
/*  115 */     this.c13 = (this.c12 + this.width + this.espH);
/*  116 */     this.c14 = (this.c13 + this.width + this.espH);
/*  117 */     this.c15 = (this.c14 + this.width + this.espH);
/*  118 */     this.c16 = (this.c15 + this.width + this.espH);
/*  119 */     this.c17 = (this.c16 + this.width + this.espH);
/*  120 */     this.c18 = (this.c17 + this.width + this.espH);
/*      */ 
/*  122 */     this.H = new JLabel(convMultL("1//H")); this.H.setFont(this.fnt);
/*  123 */     this.H.setBounds(this.c1, this.f1, this.width, this.height);
/*  124 */     this.deskTable.add(this.H); this.H.setBackground(this.g1Color);
/*  125 */     this.H.setOpaque(true); this.H.setHorizontalAlignment(0);
/*  126 */     this.H.setVerticalAlignment(1);
/*      */ 
/*  128 */     this.He = new JLabel(convMultL("2//He")); this.He.setFont(this.fnt);
/*  129 */     this.He.setBounds(this.c18, this.f1, this.width, this.height);
/*  130 */     this.deskTable.add(this.He); this.He.setBackground(this.g1Color);
/*  131 */     this.He.setOpaque(true); this.He.setHorizontalAlignment(0);
/*  132 */     this.He.setVerticalAlignment(1);
/*      */ 
/*  134 */     this.Li = new JLabel(convMultL("3//Li")); this.Li.setFont(this.fnt);
/*  135 */     this.Li.setBounds(this.c1, this.f2, this.width, this.height);
/*  136 */     this.deskTable.add(this.Li); this.Li.setBackground(this.g0Color);
/*  137 */     this.Li.setOpaque(true); this.Li.setHorizontalAlignment(0);
/*  138 */     this.Li.setVerticalAlignment(1);
/*      */ 
/*  140 */     this.Be = new JLabel(convMultL("4//Be")); this.Be.setFont(this.fnt);
/*  141 */     this.Be.setBounds(this.c2, this.f2, this.width, this.height);
/*  142 */     this.deskTable.add(this.Be); this.Be.setBackground(this.g0Color);
/*  143 */     this.Be.setOpaque(true); this.Be.setHorizontalAlignment(0);
/*  144 */     this.Be.setVerticalAlignment(1);
/*      */ 
/*  146 */     this.B = new JLabel(convMultL("5//B")); this.B.setFont(this.fnt);
/*  147 */     this.B.setBounds(this.c13, this.f2, this.width, this.height);
/*  148 */     this.deskTable.add(this.B); this.B.setBackground(this.g2Color);
/*  149 */     this.B.setOpaque(true); this.B.setHorizontalAlignment(0);
/*  150 */     this.B.setVerticalAlignment(1);
/*      */ 
/*  152 */     this.C = new JLabel(convMultL("6//C")); this.C.setFont(this.fnt);
/*  153 */     this.C.setBounds(this.c14, this.f2, this.width, this.height);
/*  154 */     this.deskTable.add(this.C); this.C.setBackground(this.g1Color);
/*  155 */     this.C.setOpaque(true); this.C.setHorizontalAlignment(0);
/*  156 */     this.C.setVerticalAlignment(1);
/*      */ 
/*  158 */     this.N = new JLabel(convMultL("7//N")); this.N.setFont(this.fnt);
/*  159 */     this.N.setBounds(this.c15, this.f2, this.width, this.height);
/*  160 */     this.deskTable.add(this.N); this.N.setBackground(this.g1Color);
/*  161 */     this.N.setOpaque(true); this.N.setHorizontalAlignment(0);
/*  162 */     this.N.setVerticalAlignment(1);
/*      */ 
/*  164 */     this.O = new JLabel(convMultL("8//O")); this.O.setFont(this.fnt);
/*  165 */     this.O.setBounds(this.c16, this.f2, this.width, this.height);
/*  166 */     this.deskTable.add(this.O); this.O.setBackground(this.g1Color);
/*  167 */     this.O.setOpaque(true); this.O.setHorizontalAlignment(0);
/*  168 */     this.O.setVerticalAlignment(1);
/*      */ 
/*  170 */     this.F = new JLabel(convMultL("9//F")); this.F.setFont(this.fnt);
/*  171 */     this.F.setBounds(this.c17, this.f2, this.width, this.height);
/*  172 */     this.deskTable.add(this.F); this.F.setBackground(this.g1Color);
/*  173 */     this.F.setOpaque(true); this.F.setHorizontalAlignment(0);
/*  174 */     this.F.setVerticalAlignment(1);
/*      */ 
/*  176 */     this.Ne = new JLabel(convMultL("10//Ne")); this.Ne.setFont(this.fnt);
/*  177 */     this.Ne.setBounds(this.c18, this.f2, this.width, this.height);
/*  178 */     this.deskTable.add(this.Ne); this.Ne.setBackground(this.g1Color);
/*  179 */     this.Ne.setOpaque(true); this.Ne.setHorizontalAlignment(0);
/*  180 */     this.Ne.setVerticalAlignment(1);
/*      */ 
/*  182 */     this.Na = new JLabel(convMultL("11//Na")); this.Na.setFont(this.fnt);
/*  183 */     this.Na.setBounds(this.c1, this.f3, this.width, this.height);
/*  184 */     this.deskTable.add(this.Na); this.Na.setBackground(this.g0Color);
/*  185 */     this.Na.setOpaque(true); this.Na.setHorizontalAlignment(0);
/*  186 */     this.Na.setVerticalAlignment(1);
/*      */ 
/*  188 */     this.Mg = new JLabel(convMultL("12//Mg")); this.Mg.setFont(this.fnt);
/*  189 */     this.Mg.setBounds(this.c2, this.f3, this.width, this.height);
/*  190 */     this.deskTable.add(this.Mg); this.Mg.setBackground(this.g0Color);
/*  191 */     this.Mg.setOpaque(true); this.Mg.setHorizontalAlignment(0);
/*  192 */     this.Mg.setVerticalAlignment(1);
/*      */ 
/*  194 */     this.Al = new JLabel(convMultL("13//Al")); this.Al.setFont(this.fnt);
/*  195 */     this.Al.setBounds(this.c13, this.f3, this.width, this.height);
/*  196 */     this.deskTable.add(this.Al); this.Al.setBackground(this.g0Color);
/*  197 */     this.Al.setOpaque(true); this.Al.setHorizontalAlignment(0);
/*  198 */     this.Al.setVerticalAlignment(1);
/*      */ 
/*  200 */     this.Si = new JLabel(convMultL("14//Si")); this.Si.setFont(this.fnt);
/*  201 */     this.Si.setBounds(this.c14, this.f3, this.width, this.height);
/*  202 */     this.deskTable.add(this.Si); this.Si.setBackground(this.g2Color);
/*  203 */     this.Si.setOpaque(true); this.Si.setHorizontalAlignment(0);
/*  204 */     this.Si.setVerticalAlignment(1);
/*      */ 
/*  206 */     this.P = new JLabel(convMultL("15//P")); this.P.setFont(this.fnt);
/*  207 */     this.P.setBounds(this.c15, this.f3, this.width, this.height);
/*  208 */     this.deskTable.add(this.P); this.P.setBackground(this.g1Color);
/*  209 */     this.P.setOpaque(true); this.P.setHorizontalAlignment(0);
/*  210 */     this.P.setVerticalAlignment(1);
/*      */ 
/*  212 */     this.S = new JLabel(convMultL("16//S")); this.S.setFont(this.fnt);
/*  213 */     this.S.setBounds(this.c16, this.f3, this.width, this.height);
/*  214 */     this.deskTable.add(this.S); this.S.setBackground(this.g1Color);
/*  215 */     this.S.setOpaque(true); this.S.setHorizontalAlignment(0);
/*  216 */     this.S.setVerticalAlignment(1);
/*      */ 
/*  218 */     this.Cl = new JLabel(convMultL("17//Cl")); this.Cl.setFont(this.fnt);
/*  219 */     this.Cl.setBounds(this.c17, this.f3, this.width, this.height);
/*  220 */     this.deskTable.add(this.Cl); this.Cl.setBackground(this.g1Color);
/*  221 */     this.Cl.setOpaque(true); this.Cl.setHorizontalAlignment(0);
/*  222 */     this.Cl.setVerticalAlignment(1);
/*      */ 
/*  224 */     this.Ar = new JLabel(convMultL("18//Ar")); this.Ar.setFont(this.fnt);
/*  225 */     this.Ar.setBounds(this.c18, this.f3, this.width, this.height);
/*  226 */     this.deskTable.add(this.Ar); this.Ar.setBackground(this.g1Color);
/*  227 */     this.Ar.setOpaque(true); this.Ar.setHorizontalAlignment(0);
/*  228 */     this.Ar.setVerticalAlignment(1);
/*      */ 
/*  230 */     this.K = new JLabel(convMultL("19//K")); this.K.setFont(this.fnt);
/*  231 */     this.K.setBounds(this.c1, this.f4, this.width, this.height);
/*  232 */     this.deskTable.add(this.K); this.K.setBackground(this.g0Color);
/*  233 */     this.K.setOpaque(true); this.K.setHorizontalAlignment(0);
/*  234 */     this.K.setVerticalAlignment(1);
/*      */ 
/*  236 */     this.Ca = new JLabel(convMultL("20//Ca")); this.Ca.setFont(this.fnt);
/*  237 */     this.Ca.setBounds(this.c2, this.f4, this.width, this.height);
/*  238 */     this.deskTable.add(this.Ca); this.Ca.setBackground(this.g0Color);
/*  239 */     this.Ca.setOpaque(true); this.Ca.setHorizontalAlignment(0);
/*  240 */     this.Ca.setVerticalAlignment(1);
/*      */ 
/*  242 */     this.Sc = new JLabel(convMultL("21//Sc")); this.Sc.setFont(this.fnt);
/*  243 */     this.Sc.setBounds(this.c3, this.f4, this.width, this.height);
/*  244 */     this.deskTable.add(this.Sc); this.Sc.setBackground(this.g0Color);
/*  245 */     this.Sc.setOpaque(true); this.Sc.setHorizontalAlignment(0);
/*  246 */     this.Sc.setVerticalAlignment(1);
/*      */ 
/*  248 */     this.Ti = new JLabel(convMultL("22//Ti")); this.Ti.setFont(this.fnt);
/*  249 */     this.Ti.setBounds(this.c4, this.f4, this.width, this.height);
/*  250 */     this.deskTable.add(this.Ti); this.Ti.setBackground(this.g0Color);
/*  251 */     this.Ti.setOpaque(true); this.Ti.setHorizontalAlignment(0);
/*  252 */     this.Ti.setVerticalAlignment(1);
/*      */ 
/*  254 */     this.V = new JLabel(convMultL("23//V")); this.V.setFont(this.fnt);
/*  255 */     this.V.setBounds(this.c5, this.f4, this.width, this.height);
/*  256 */     this.deskTable.add(this.V); this.V.setBackground(this.g0Color);
/*  257 */     this.V.setOpaque(true); this.V.setHorizontalAlignment(0);
/*  258 */     this.V.setVerticalAlignment(1);
/*      */ 
/*  260 */     this.Cr = new JLabel(convMultL("24//Cr")); this.Cr.setFont(this.fnt);
/*  261 */     this.Cr.setBounds(this.c6, this.f4, this.width, this.height);
/*  262 */     this.deskTable.add(this.Cr); this.Cr.setBackground(this.g0Color);
/*  263 */     this.Cr.setOpaque(true); this.Cr.setHorizontalAlignment(0);
/*  264 */     this.Cr.setVerticalAlignment(1);
/*      */ 
/*  266 */     this.Mn = new JLabel(convMultL("25//Mn")); this.Mn.setFont(this.fnt);
/*  267 */     this.Mn.setBounds(this.c7, this.f4, this.width, this.height);
/*  268 */     this.deskTable.add(this.Mn); this.Mn.setBackground(this.g0Color);
/*  269 */     this.Mn.setOpaque(true); this.Mn.setHorizontalAlignment(0);
/*  270 */     this.Mn.setVerticalAlignment(1);
/*      */ 
/*  272 */     this.Fe = new JLabel(convMultL("26//Fe")); this.Fe.setFont(this.fnt);
/*  273 */     this.Fe.setBounds(this.c8, this.f4, this.width, this.height);
/*  274 */     this.deskTable.add(this.Fe); this.Fe.setBackground(this.g0Color);
/*  275 */     this.Fe.setOpaque(true); this.Fe.setHorizontalAlignment(0);
/*  276 */     this.Fe.setVerticalAlignment(1);
/*      */ 
/*  278 */     this.Co = new JLabel(convMultL("27//Co")); this.Co.setFont(this.fnt);
/*  279 */     this.Co.setBounds(this.c9, this.f4, this.width, this.height);
/*  280 */     this.deskTable.add(this.Co); this.Co.setBackground(this.g0Color);
/*  281 */     this.Co.setOpaque(true); this.Co.setHorizontalAlignment(0);
/*  282 */     this.Co.setVerticalAlignment(1);
/*      */ 
/*  284 */     this.Ni = new JLabel(convMultL("28//Ni")); this.Ni.setFont(this.fnt);
/*  285 */     this.Ni.setBounds(this.c10, this.f4, this.width, this.height);
/*  286 */     this.deskTable.add(this.Ni); this.Ni.setBackground(this.g0Color);
/*  287 */     this.Ni.setOpaque(true); this.Ni.setHorizontalAlignment(0);
/*  288 */     this.Ni.setVerticalAlignment(1);
/*      */ 
/*  290 */     this.Cu = new JLabel(convMultL("29//Cu")); this.Cu.setFont(this.fnt);
/*  291 */     this.Cu.setBounds(this.c11, this.f4, this.width, this.height);
/*  292 */     this.deskTable.add(this.Cu); this.Cu.setBackground(this.g0Color);
/*  293 */     this.Cu.setOpaque(true); this.Cu.setHorizontalAlignment(0);
/*  294 */     this.Cu.setVerticalAlignment(1);
/*      */ 
/*  296 */     this.Zn = new JLabel(convMultL("30//Zn")); this.Zn.setFont(this.fnt);
/*  297 */     this.Zn.setBounds(this.c12, this.f4, this.width, this.height);
/*  298 */     this.deskTable.add(this.Zn); this.Zn.setBackground(this.g0Color);
/*  299 */     this.Zn.setOpaque(true); this.Zn.setHorizontalAlignment(0);
/*  300 */     this.Zn.setVerticalAlignment(1);
/*      */ 
/*  302 */     this.Ga = new JLabel(convMultL("31//Ga")); this.Ga.setFont(this.fnt);
/*  303 */     this.Ga.setBounds(this.c13, this.f4, this.width, this.height);
/*  304 */     this.deskTable.add(this.Ga); this.Ga.setBackground(this.g0Color);
/*  305 */     this.Ga.setOpaque(true); this.Ga.setHorizontalAlignment(0);
/*  306 */     this.Ga.setVerticalAlignment(1);
/*      */ 
/*  308 */     this.Ge = new JLabel(convMultL("32//Ge")); this.Ge.setFont(this.fnt);
/*  309 */     this.Ge.setBounds(this.c14, this.f4, this.width, this.height);
/*  310 */     this.deskTable.add(this.Ge); this.Ge.setBackground(this.g2Color);
/*  311 */     this.Ge.setOpaque(true); this.Ge.setHorizontalAlignment(0);
/*  312 */     this.Ge.setVerticalAlignment(1);
/*      */ 
/*  314 */     this.As = new JLabel(convMultL("33//As")); this.As.setFont(this.fnt);
/*  315 */     this.As.setBounds(this.c15, this.f4, this.width, this.height);
/*  316 */     this.deskTable.add(this.As); this.As.setBackground(this.g2Color);
/*  317 */     this.As.setOpaque(true); this.As.setHorizontalAlignment(0);
/*  318 */     this.As.setVerticalAlignment(1);
/*      */ 
/*  320 */     this.Se = new JLabel(convMultL("34//Se")); this.Se.setFont(this.fnt);
/*  321 */     this.Se.setBounds(this.c16, this.f4, this.width, this.height);
/*  322 */     this.deskTable.add(this.Se); this.Se.setBackground(this.g1Color);
/*  323 */     this.Se.setOpaque(true); this.Se.setHorizontalAlignment(0);
/*  324 */     this.Se.setVerticalAlignment(1);
/*      */ 
/*  326 */     this.Br = new JLabel(convMultL("35//Br")); this.Br.setFont(this.fnt);
/*  327 */     this.Br.setBounds(this.c17, this.f4, this.width, this.height);
/*  328 */     this.deskTable.add(this.Br); this.Br.setBackground(this.g1Color);
/*  329 */     this.Br.setOpaque(true); this.Br.setHorizontalAlignment(0);
/*  330 */     this.Br.setVerticalAlignment(1);
/*      */ 
/*  332 */     this.Kr = new JLabel(convMultL("36//Kr")); this.Kr.setFont(this.fnt);
/*  333 */     this.Kr.setBounds(this.c18, this.f4, this.width, this.height);
/*  334 */     this.deskTable.add(this.Kr); this.Kr.setBackground(this.g1Color);
/*  335 */     this.Kr.setOpaque(true); this.Kr.setHorizontalAlignment(0);
/*  336 */     this.Kr.setVerticalAlignment(1);
/*      */ 
/*  338 */     this.Rb = new JLabel(convMultL("37//Rb")); this.Rb.setFont(this.fnt);
/*  339 */     this.Rb.setBounds(this.c1, this.f5, this.width, this.height);
/*  340 */     this.deskTable.add(this.Rb); this.Rb.setBackground(this.g0Color);
/*  341 */     this.Rb.setOpaque(true); this.Rb.setHorizontalAlignment(0);
/*  342 */     this.Rb.setVerticalAlignment(1);
/*      */ 
/*  344 */     this.Sr = new JLabel(convMultL("38//Sr")); this.Sr.setFont(this.fnt);
/*  345 */     this.Sr.setBounds(this.c2, this.f5, this.width, this.height);
/*  346 */     this.deskTable.add(this.Sr); this.Sr.setBackground(this.g0Color);
/*  347 */     this.Sr.setOpaque(true); this.Sr.setHorizontalAlignment(0);
/*  348 */     this.Sr.setVerticalAlignment(1);
/*      */ 
/*  350 */     this.Y = new JLabel(convMultL("39//Y")); this.Y.setFont(this.fnt);
/*  351 */     this.Y.setBounds(this.c3, this.f5, this.width, this.height);
/*  352 */     this.deskTable.add(this.Y); this.Y.setBackground(this.g0Color);
/*  353 */     this.Y.setOpaque(true); this.Y.setHorizontalAlignment(0);
/*  354 */     this.Y.setVerticalAlignment(1);
/*      */ 
/*  356 */     this.Zr = new JLabel(convMultL("40//Zr")); this.Zr.setFont(this.fnt);
/*  357 */     this.Zr.setBounds(this.c4, this.f5, this.width, this.height);
/*  358 */     this.deskTable.add(this.Zr); this.Zr.setBackground(this.g0Color);
/*  359 */     this.Zr.setOpaque(true); this.Zr.setHorizontalAlignment(0);
/*  360 */     this.Zr.setVerticalAlignment(1);
/*      */ 
/*  362 */     this.Nb = new JLabel(convMultL("41//Nb")); this.Nb.setFont(this.fnt);
/*  363 */     this.Nb.setBounds(this.c5, this.f5, this.width, this.height);
/*  364 */     this.deskTable.add(this.Nb); this.Nb.setBackground(this.g0Color);
/*  365 */     this.Nb.setOpaque(true); this.Nb.setHorizontalAlignment(0);
/*  366 */     this.Nb.setVerticalAlignment(1);
/*      */ 
/*  368 */     this.Mo = new JLabel(convMultL("42//Mo")); this.Mo.setFont(this.fnt);
/*  369 */     this.Mo.setBounds(this.c6, this.f5, this.width, this.height);
/*  370 */     this.deskTable.add(this.Mo); this.Mo.setBackground(this.g0Color);
/*  371 */     this.Mo.setOpaque(true); this.Mo.setHorizontalAlignment(0);
/*  372 */     this.Mo.setVerticalAlignment(1);
/*      */ 
/*  374 */     this.Tc = new JLabel(convMultL("43//Tc")); this.Tc.setFont(this.fnt);
/*  375 */     this.Tc.setBounds(this.c7, this.f5, this.width, this.height);
/*  376 */     this.deskTable.add(this.Tc); this.Tc.setBackground(this.g0Color);
/*  377 */     this.Tc.setOpaque(true); this.Tc.setHorizontalAlignment(0);
/*  378 */     this.Tc.setVerticalAlignment(1);
/*      */ 
/*  380 */     this.Ru = new JLabel(convMultL("44//Ru")); this.Ru.setFont(this.fnt);
/*  381 */     this.Ru.setBounds(this.c8, this.f5, this.width, this.height);
/*  382 */     this.deskTable.add(this.Ru); this.Ru.setBackground(this.g0Color);
/*  383 */     this.Ru.setOpaque(true); this.Ru.setHorizontalAlignment(0);
/*  384 */     this.Ru.setVerticalAlignment(1);
/*      */ 
/*  386 */     this.Rh = new JLabel(convMultL("45//Rh")); this.Rh.setFont(this.fnt);
/*  387 */     this.Rh.setBounds(this.c9, this.f5, this.width, this.height);
/*  388 */     this.deskTable.add(this.Rh); this.Rh.setBackground(this.g0Color);
/*  389 */     this.Rh.setOpaque(true); this.Rh.setHorizontalAlignment(0);
/*  390 */     this.Rh.setVerticalAlignment(1);
/*      */ 
/*  392 */     this.Pd = new JLabel(convMultL("46//Pd")); this.Pd.setFont(this.fnt);
/*  393 */     this.Pd.setBounds(this.c10, this.f5, this.width, this.height);
/*  394 */     this.deskTable.add(this.Pd); this.Pd.setBackground(this.g0Color);
/*  395 */     this.Pd.setOpaque(true); this.Pd.setHorizontalAlignment(0);
/*  396 */     this.Pd.setVerticalAlignment(1);
/*      */ 
/*  398 */     this.Ag = new JLabel(convMultL("47//Ag")); this.Ag.setFont(this.fnt);
/*  399 */     this.Ag.setBounds(this.c11, this.f5, this.width, this.height);
/*  400 */     this.deskTable.add(this.Ag); this.Ag.setBackground(this.g0Color);
/*  401 */     this.Ag.setOpaque(true); this.Ag.setHorizontalAlignment(0);
/*  402 */     this.Ag.setVerticalAlignment(1);
/*      */ 
/*  404 */     this.Cd = new JLabel(convMultL("48//Cd")); this.Cd.setFont(this.fnt);
/*  405 */     this.Cd.setBounds(this.c12, this.f5, this.width, this.height);
/*  406 */     this.deskTable.add(this.Cd); this.Cd.setBackground(this.g0Color);
/*  407 */     this.Cd.setOpaque(true); this.Cd.setHorizontalAlignment(0);
/*  408 */     this.Cd.setVerticalAlignment(1);
/*      */ 
/*  410 */     this.In = new JLabel(convMultL("49//In")); this.In.setFont(this.fnt);
/*  411 */     this.In.setBounds(this.c13, this.f5, this.width, this.height);
/*  412 */     this.deskTable.add(this.In); this.In.setBackground(this.g0Color);
/*  413 */     this.In.setOpaque(true); this.In.setHorizontalAlignment(0);
/*  414 */     this.In.setVerticalAlignment(1);
/*      */ 
/*  416 */     this.Sn = new JLabel(convMultL("50//Sn")); this.Sn.setFont(this.fnt);
/*  417 */     this.Sn.setBounds(this.c14, this.f5, this.width, this.height);
/*  418 */     this.deskTable.add(this.Sn); this.Sn.setBackground(this.g0Color);
/*  419 */     this.Sn.setOpaque(true); this.Sn.setHorizontalAlignment(0);
/*  420 */     this.Sn.setVerticalAlignment(1);
/*      */ 
/*  422 */     this.Sb = new JLabel(convMultL("51//Sb")); this.Sb.setFont(this.fnt);
/*  423 */     this.Sb.setBounds(this.c15, this.f5, this.width, this.height);
/*  424 */     this.deskTable.add(this.Sb); this.Sb.setBackground(this.g2Color);
/*  425 */     this.Sb.setOpaque(true); this.Sb.setHorizontalAlignment(0);
/*  426 */     this.Sb.setVerticalAlignment(1);
/*      */ 
/*  428 */     this.Te = new JLabel(convMultL("52//Te")); this.Te.setFont(this.fnt);
/*  429 */     this.Te.setBounds(this.c16, this.f5, this.width, this.height);
/*  430 */     this.deskTable.add(this.Te); this.Te.setBackground(this.g2Color);
/*  431 */     this.Te.setOpaque(true); this.Te.setHorizontalAlignment(0);
/*  432 */     this.Te.setVerticalAlignment(1);
/*      */ 
/*  434 */     this.I = new JLabel(convMultL("53//I")); this.I.setFont(this.fnt);
/*  435 */     this.I.setBounds(this.c17, this.f5, this.width, this.height);
/*  436 */     this.deskTable.add(this.I); this.I.setBackground(this.g1Color);
/*  437 */     this.I.setOpaque(true); this.I.setHorizontalAlignment(0);
/*  438 */     this.I.setVerticalAlignment(1);
/*      */ 
/*  440 */     this.Xe = new JLabel(convMultL("54//Xe")); this.Xe.setFont(this.fnt);
/*  441 */     this.Xe.setBounds(this.c18, this.f5, this.width, this.height);
/*  442 */     this.deskTable.add(this.Xe); this.Xe.setBackground(this.g1Color);
/*  443 */     this.Xe.setOpaque(true); this.Xe.setHorizontalAlignment(0);
/*  444 */     this.Xe.setVerticalAlignment(1);
/*      */ 
/*  446 */     this.Cs = new JLabel(convMultL("55//Cs")); this.Cs.setFont(this.fnt);
/*  447 */     this.Cs.setBounds(this.c1, this.f6, this.width, this.height);
/*  448 */     this.deskTable.add(this.Cs); this.Cs.setBackground(this.g0Color);
/*  449 */     this.Cs.setOpaque(true); this.Cs.setHorizontalAlignment(0);
/*  450 */     this.Cs.setVerticalAlignment(1);
/*      */ 
/*  452 */     this.Ba = new JLabel(convMultL("56//Ba")); this.Ba.setFont(this.fnt);
/*  453 */     this.Ba.setBounds(this.c2, this.f6, this.width, this.height);
/*  454 */     this.deskTable.add(this.Ba); this.Ba.setBackground(this.g0Color);
/*  455 */     this.Ba.setOpaque(true); this.Ba.setHorizontalAlignment(0);
/*  456 */     this.Ba.setVerticalAlignment(1);
/*      */ 
/*  458 */     this.Lantanidos = new JLabel(convMultL("//")); this.Lantanidos.setFont(this.fnt);
/*  459 */     this.Lantanidos.setBounds(this.c3, this.f6, this.width, this.height);
/*  460 */     this.deskTable.add(this.Lantanidos); this.Lantanidos.setBackground(this.g3Color);
/*  461 */     this.Lantanidos.setOpaque(true); this.Lantanidos.setHorizontalAlignment(0);
/*      */ 
/*  463 */     this.Hf = new JLabel(convMultL("72//Hf")); this.Hf.setFont(this.fnt);
/*  464 */     this.Hf.setBounds(this.c4, this.f6, this.width, this.height);
/*  465 */     this.deskTable.add(this.Hf); this.Hf.setBackground(this.g0Color);
/*  466 */     this.Hf.setOpaque(true); this.Hf.setHorizontalAlignment(0);
/*  467 */     this.Hf.setVerticalAlignment(1);
/*      */ 
/*  469 */     this.Ta = new JLabel(convMultL("73//Ta")); this.Ta.setFont(this.fnt);
/*  470 */     this.Ta.setBounds(this.c5, this.f6, this.width, this.height);
/*  471 */     this.deskTable.add(this.Ta); this.Ta.setBackground(this.g0Color);
/*  472 */     this.Ta.setOpaque(true); this.Ta.setHorizontalAlignment(0);
/*  473 */     this.Ta.setVerticalAlignment(1);
/*      */ 
/*  475 */     this.W = new JLabel(convMultL("74//W")); this.W.setFont(this.fnt);
/*  476 */     this.W.setBounds(this.c6, this.f6, this.width, this.height);
/*  477 */     this.deskTable.add(this.W); this.W.setBackground(this.g0Color);
/*  478 */     this.W.setOpaque(true); this.W.setHorizontalAlignment(0);
/*  479 */     this.W.setVerticalAlignment(1);
/*      */ 
/*  481 */     this.Re = new JLabel(convMultL("75//Re")); this.Re.setFont(this.fnt);
/*  482 */     this.Re.setBounds(this.c7, this.f6, this.width, this.height);
/*  483 */     this.deskTable.add(this.Re); this.Re.setBackground(this.g0Color);
/*  484 */     this.Re.setOpaque(true); this.Re.setHorizontalAlignment(0);
/*  485 */     this.Re.setVerticalAlignment(1);
/*      */ 
/*  487 */     this.Os = new JLabel(convMultL("76//Os")); this.Os.setFont(this.fnt);
/*  488 */     this.Os.setBounds(this.c8, this.f6, this.width, this.height);
/*  489 */     this.deskTable.add(this.Os); this.Os.setBackground(this.g0Color);
/*  490 */     this.Os.setOpaque(true); this.Os.setHorizontalAlignment(0);
/*  491 */     this.Os.setVerticalAlignment(1);
/*      */ 
/*  493 */     this.Ir = new JLabel(convMultL("77//Ir")); this.Ir.setFont(this.fnt);
/*  494 */     this.Ir.setBounds(this.c9, this.f6, this.width, this.height);
/*  495 */     this.deskTable.add(this.Ir); this.Ir.setBackground(this.g0Color);
/*  496 */     this.Ir.setOpaque(true); this.Ir.setHorizontalAlignment(0);
/*  497 */     this.Ir.setVerticalAlignment(1);
/*      */ 
/*  499 */     this.Pt = new JLabel(convMultL("78//Pt")); this.Pt.setFont(this.fnt);
/*  500 */     this.Pt.setBounds(this.c10, this.f6, this.width, this.height);
/*  501 */     this.deskTable.add(this.Pt); this.Pt.setBackground(this.g0Color);
/*  502 */     this.Pt.setOpaque(true); this.Pt.setHorizontalAlignment(0);
/*  503 */     this.Pt.setVerticalAlignment(1);
/*      */ 
/*  505 */     this.Au = new JLabel(convMultL("79//Au")); this.Au.setFont(this.fnt);
/*  506 */     this.Au.setBounds(this.c11, this.f6, this.width, this.height);
/*  507 */     this.deskTable.add(this.Au); this.Au.setBackground(this.g0Color);
/*  508 */     this.Au.setOpaque(true); this.Au.setHorizontalAlignment(0);
/*  509 */     this.Au.setVerticalAlignment(1);
/*      */ 
/*  511 */     this.Hg = new JLabel(convMultL("80//Hg")); this.Hg.setFont(this.fnt);
/*  512 */     this.Hg.setBounds(this.c12, this.f6, this.width, this.height);
/*  513 */     this.deskTable.add(this.Hg); this.Hg.setBackground(this.g0Color);
/*  514 */     this.Hg.setOpaque(true); this.Hg.setHorizontalAlignment(0);
/*  515 */     this.Hg.setVerticalAlignment(1);
/*      */ 
/*  517 */     this.Tl = new JLabel(convMultL("81//Tl")); this.Tl.setFont(this.fnt);
/*  518 */     this.Tl.setBounds(this.c13, this.f6, this.width, this.height);
/*  519 */     this.deskTable.add(this.Tl); this.Tl.setBackground(this.g0Color);
/*  520 */     this.Tl.setOpaque(true); this.Tl.setHorizontalAlignment(0);
/*  521 */     this.Tl.setVerticalAlignment(1);
/*      */ 
/*  523 */     this.Pb = new JLabel(convMultL("82//Pb")); this.Pb.setFont(this.fnt);
/*  524 */     this.Pb.setBounds(this.c14, this.f6, this.width, this.height);
/*  525 */     this.deskTable.add(this.Pb); this.Pb.setBackground(this.g0Color);
/*  526 */     this.Pb.setOpaque(true); this.Pb.setHorizontalAlignment(0);
/*  527 */     this.Pb.setVerticalAlignment(1);
/*      */ 
/*  529 */     this.Bi = new JLabel(convMultL("83//Bi")); this.Bi.setFont(this.fnt);
/*  530 */     this.Bi.setBounds(this.c15, this.f6, this.width, this.height);
/*  531 */     this.deskTable.add(this.Bi); this.Bi.setBackground(this.g0Color);
/*  532 */     this.Bi.setOpaque(true); this.Bi.setHorizontalAlignment(0);
/*  533 */     this.Bi.setVerticalAlignment(1);
/*      */ 
/*  535 */     this.Po = new JLabel(convMultL("84//Po")); this.Po.setFont(this.fnt);
/*  536 */     this.Po.setBounds(this.c16, this.f6, this.width, this.height);
/*  537 */     this.deskTable.add(this.Po); this.Po.setBackground(this.g2Color);
/*  538 */     this.Po.setOpaque(true); this.Po.setHorizontalAlignment(0);
/*  539 */     this.Po.setVerticalAlignment(1);
/*      */ 
/*  541 */     this.At = new JLabel(convMultL("85//At")); this.At.setFont(this.fnt);
/*  542 */     this.At.setBounds(this.c17, this.f6, this.width, this.height);
/*  543 */     this.deskTable.add(this.At); this.At.setBackground(this.g1Color);
/*  544 */     this.At.setOpaque(true); this.At.setHorizontalAlignment(0);
/*  545 */     this.At.setVerticalAlignment(1);
/*      */ 
/*  547 */     this.Rn = new JLabel(convMultL("86//Rn")); this.Rn.setFont(this.fnt);
/*  548 */     this.Rn.setBounds(this.c18, this.f6, this.width, this.height);
/*  549 */     this.deskTable.add(this.Rn); this.Rn.setBackground(this.g1Color);
/*  550 */     this.Rn.setOpaque(true); this.Rn.setHorizontalAlignment(0);
/*  551 */     this.Rn.setVerticalAlignment(1);
/*      */ 
/*  553 */     this.Fr = new JLabel(convMultL("87//Fr")); this.Fr.setFont(this.fnt);
/*  554 */     this.Fr.setBounds(this.c1, this.f7, this.width, this.height);
/*  555 */     this.deskTable.add(this.Fr); this.Fr.setBackground(this.g0Color);
/*  556 */     this.Fr.setOpaque(true); this.Fr.setHorizontalAlignment(0);
/*  557 */     this.Fr.setVerticalAlignment(1);
/*      */ 
/*  559 */     this.Ra = new JLabel(convMultL("88//Ra")); this.Ra.setFont(this.fnt);
/*  560 */     this.Ra.setBounds(this.c2, this.f7, this.width, this.height);
/*  561 */     this.deskTable.add(this.Ra); this.Ra.setBackground(this.g0Color);
/*  562 */     this.Ra.setOpaque(true); this.Ra.setHorizontalAlignment(0);
/*  563 */     this.Ra.setVerticalAlignment(1);
/*      */ 
/*  565 */     this.Actinidos = new JLabel(convMultL("//")); this.Actinidos.setFont(this.fnt);
/*  566 */     this.Actinidos.setBounds(this.c3, this.f7, this.width, this.height);
/*  567 */     this.deskTable.add(this.Actinidos); this.Actinidos.setBackground(this.g3Color);
/*  568 */     this.Actinidos.setOpaque(true); this.Actinidos.setHorizontalAlignment(0);
/*      */ 
/*  570 */     this.Rf = new JLabel(convMultL("104//Rf")); this.Rf.setFont(this.fnt);
/*  571 */     this.Rf.setBounds(this.c4, this.f7, this.width, this.height);
/*  572 */     this.deskTable.add(this.Rf); this.Rf.setBackground(this.g0Color);
/*  573 */     this.Rf.setOpaque(true); this.Rf.setHorizontalAlignment(0);
/*  574 */     this.Rf.setVerticalAlignment(1);
/*      */ 
/*  576 */     this.Db = new JLabel(convMultL("105//Db")); this.Db.setFont(this.fnt);
/*  577 */     this.Db.setBounds(this.c5, this.f7, this.width, this.height);
/*  578 */     this.deskTable.add(this.Db); this.Db.setBackground(this.g0Color);
/*  579 */     this.Db.setOpaque(true); this.Db.setHorizontalAlignment(0);
/*  580 */     this.Db.setVerticalAlignment(1);
/*      */ 
/*  582 */     this.Sg = new JLabel(convMultL("106//Sg")); this.Sg.setFont(this.fnt);
/*  583 */     this.Sg.setBounds(this.c6, this.f7, this.width, this.height);
/*  584 */     this.deskTable.add(this.Sg); this.Sg.setBackground(this.g0Color);
/*  585 */     this.Sg.setOpaque(true); this.Sg.setHorizontalAlignment(0);
/*  586 */     this.Sg.setVerticalAlignment(1);
/*      */ 
/*  588 */     this.Bh = new JLabel(convMultL("107//Bh")); this.Bh.setFont(this.fnt);
/*  589 */     this.Bh.setBounds(this.c7, this.f7, this.width, this.height);
/*  590 */     this.deskTable.add(this.Bh); this.Bh.setBackground(this.g0Color);
/*  591 */     this.Bh.setOpaque(true); this.Bh.setHorizontalAlignment(0);
/*  592 */     this.Bh.setVerticalAlignment(1);
/*      */ 
/*  594 */     this.Hs = new JLabel(convMultL("108//Hs")); this.Hs.setFont(this.fnt);
/*  595 */     this.Hs.setBounds(this.c8, this.f7, this.width, this.height);
/*  596 */     this.deskTable.add(this.Hs); this.Hs.setBackground(this.g0Color);
/*  597 */     this.Hs.setOpaque(true); this.Hs.setHorizontalAlignment(0);
/*  598 */     this.Hs.setVerticalAlignment(1);
/*      */ 
/*  600 */     this.Mt = new JLabel(convMultL("109//Mt")); this.Mt.setFont(this.fnt);
/*  601 */     this.Mt.setBounds(this.c9, this.f7, this.width, this.height);
/*  602 */     this.deskTable.add(this.Mt); this.Mt.setBackground(this.g0Color);
/*  603 */     this.Mt.setOpaque(true); this.Mt.setHorizontalAlignment(0);
/*  604 */     this.Mt.setVerticalAlignment(1);
/*      */ 
/*  606 */     this.Ds = new JLabel(convMultL("110//Ds")); this.Ds.setFont(this.fnt);
/*  607 */     this.Ds.setBounds(this.c10, this.f7, this.width, this.height);
/*  608 */     this.deskTable.add(this.Ds); this.Ds.setBackground(this.g0Color);
/*  609 */     this.Ds.setOpaque(true); this.Ds.setHorizontalAlignment(0);
/*  610 */     this.Ds.setVerticalAlignment(1);
/*      */ 
/*  612 */     this.Rg = new JLabel(convMultL("111//Rg")); this.Rg.setFont(this.fnt);
/*  613 */     this.Rg.setBounds(this.c11, this.f7, this.width, this.height);
/*  614 */     this.deskTable.add(this.Rg); this.Rg.setBackground(this.g0Color);
/*  615 */     this.Rg.setOpaque(true); this.Rg.setHorizontalAlignment(0);
/*  616 */     this.Rg.setVerticalAlignment(1);
/*      */ 
/*  618 */     this.Cn = new JLabel(convMultL("112//Cn")); this.Cn.setFont(this.fnt);
/*  619 */     this.Cn.setBounds(this.c12, this.f7, this.width, this.height);
/*  620 */     this.deskTable.add(this.Cn); this.Cn.setBackground(this.g0Color);
/*  621 */     this.Cn.setOpaque(true); this.Cn.setHorizontalAlignment(0);
/*  622 */     this.Cn.setVerticalAlignment(1);
/*      */ 
/*  624 */     this.Uut = new JLabel(convMultL("113//Uut")); this.Uut.setFont(this.fnt);
/*  625 */     this.Uut.setBounds(this.c13, this.f7, this.width, this.height);
/*  626 */     this.deskTable.add(this.Uut); this.Uut.setBackground(this.g0Color);
/*  627 */     this.Uut.setOpaque(true); this.Uut.setHorizontalAlignment(0);
/*  628 */     this.Uut.setVerticalAlignment(1);
/*      */ 
/*  630 */     this.Fl = new JLabel(convMultL("114//Fl")); this.Fl.setFont(this.fnt);
/*  631 */     this.Fl.setBounds(this.c14, this.f7, this.width, this.height);
/*  632 */     this.deskTable.add(this.Fl); this.Fl.setBackground(this.g0Color);
/*  633 */     this.Fl.setOpaque(true); this.Fl.setHorizontalAlignment(0);
/*  634 */     this.Fl.setVerticalAlignment(1);
/*      */ 
/*  636 */     this.Uup = new JLabel(convMultL("115//Uup")); this.Uup.setFont(this.fnt);
/*  637 */     this.Uup.setBounds(this.c15, this.f7, this.width, this.height);
/*  638 */     this.deskTable.add(this.Uup); this.Uup.setBackground(this.g0Color);
/*  639 */     this.Uup.setOpaque(true); this.Uup.setHorizontalAlignment(0);
/*  640 */     this.Uup.setVerticalAlignment(1);
/*      */ 
/*  642 */     this.Lv = new JLabel(convMultL("116//Lv")); this.Lv.setFont(this.fnt);
/*  643 */     this.Lv.setBounds(this.c16, this.f7, this.width, this.height);
/*  644 */     this.deskTable.add(this.Lv); this.Lv.setBackground(this.g0Color);
/*  645 */     this.Lv.setOpaque(true); this.Lv.setHorizontalAlignment(0);
/*  646 */     this.Lv.setVerticalAlignment(1);
/*      */ 
/*  648 */     this.Uus = new JLabel(convMultL("117//Uus")); this.Uus.setFont(this.fnt);
/*  649 */     this.Uus.setBounds(this.c17, this.f7, this.width, this.height);
/*  650 */     this.deskTable.add(this.Uus); this.Uus.setBackground(this.g0Color);
/*  651 */     this.Uus.setOpaque(true); this.Uus.setHorizontalAlignment(0);
/*  652 */     this.Uus.setVerticalAlignment(1);
/*      */ 
/*  654 */     this.Uuo = new JLabel(convMultL("118//Uuo")); this.Uuo.setFont(this.fnt);
/*  655 */     this.Uuo.setBounds(this.c18, this.f7, this.width, this.height);
/*  656 */     this.deskTable.add(this.Uuo); this.Uuo.setBackground(this.g0Color);
/*  657 */     this.Uuo.setOpaque(true); this.Uuo.setHorizontalAlignment(0);
/*  658 */     this.Uuo.setVerticalAlignment(1);
/*      */ 
/*  660 */     this.La = new JLabel(convMultL("57//La")); this.La.setFont(this.fnt);
/*  661 */     this.La.setBounds(this.c4, this.f8, this.width, this.height);
/*  662 */     this.deskTable.add(this.La); this.La.setBackground(this.g3Color);
/*  663 */     this.La.setOpaque(true); this.La.setHorizontalAlignment(0);
/*  664 */     this.La.setVerticalAlignment(1);
/*      */ 
/*  666 */     this.Ce = new JLabel(convMultL("58//Ce")); this.Ce.setFont(this.fnt);
/*  667 */     this.Ce.setBounds(this.c5, this.f8, this.width, this.height);
/*  668 */     this.deskTable.add(this.Ce); this.Ce.setBackground(this.g3Color);
/*  669 */     this.Ce.setOpaque(true); this.Ce.setHorizontalAlignment(0);
/*  670 */     this.Ce.setVerticalAlignment(1);
/*      */ 
/*  672 */     this.Pr = new JLabel(convMultL("59//Pr")); this.Pr.setFont(this.fnt);
/*  673 */     this.Pr.setBounds(this.c6, this.f8, this.width, this.height);
/*  674 */     this.deskTable.add(this.Pr); this.Pr.setBackground(this.g3Color);
/*  675 */     this.Pr.setOpaque(true); this.Pr.setHorizontalAlignment(0);
/*  676 */     this.Pr.setVerticalAlignment(1);
/*      */ 
/*  678 */     this.Nd = new JLabel(convMultL("60//Nd")); this.Nd.setFont(this.fnt);
/*  679 */     this.Nd.setBounds(this.c7, this.f8, this.width, this.height);
/*  680 */     this.deskTable.add(this.Nd); this.Nd.setBackground(this.g3Color);
/*  681 */     this.Nd.setOpaque(true); this.Nd.setHorizontalAlignment(0);
/*  682 */     this.Nd.setVerticalAlignment(1);
/*      */ 
/*  684 */     this.Pm = new JLabel(convMultL("61//Pm")); this.Pm.setFont(this.fnt);
/*  685 */     this.Pm.setBounds(this.c8, this.f8, this.width, this.height);
/*  686 */     this.deskTable.add(this.Pm); this.Pm.setBackground(this.g3Color);
/*  687 */     this.Pm.setOpaque(true); this.Pm.setHorizontalAlignment(0);
/*  688 */     this.Pm.setVerticalAlignment(1);
/*      */ 
/*  690 */     this.Sm = new JLabel(convMultL("62//Sm")); this.Sm.setFont(this.fnt);
/*  691 */     this.Sm.setBounds(this.c9, this.f8, this.width, this.height);
/*  692 */     this.deskTable.add(this.Sm); this.Sm.setBackground(this.g3Color);
/*  693 */     this.Sm.setOpaque(true); this.Sm.setHorizontalAlignment(0);
/*  694 */     this.Sm.setVerticalAlignment(1);
/*      */ 
/*  696 */     this.Eu = new JLabel(convMultL("63//Eu")); this.Eu.setFont(this.fnt);
/*  697 */     this.Eu.setBounds(this.c10, this.f8, this.width, this.height);
/*  698 */     this.deskTable.add(this.Eu); this.Eu.setBackground(this.g3Color);
/*  699 */     this.Eu.setOpaque(true); this.Eu.setHorizontalAlignment(0);
/*  700 */     this.Eu.setVerticalAlignment(1);
/*      */ 
/*  702 */     this.Gd = new JLabel(convMultL("64//Gd")); this.Gd.setFont(this.fnt);
/*  703 */     this.Gd.setBounds(this.c11, this.f8, this.width, this.height);
/*  704 */     this.deskTable.add(this.Gd); this.Gd.setBackground(this.g3Color);
/*  705 */     this.Gd.setOpaque(true); this.Gd.setHorizontalAlignment(0);
/*  706 */     this.Gd.setVerticalAlignment(1);
/*      */ 
/*  708 */     this.Tb = new JLabel(convMultL("65//Tb")); this.Tb.setFont(this.fnt);
/*  709 */     this.Tb.setBounds(this.c12, this.f8, this.width, this.height);
/*  710 */     this.deskTable.add(this.Tb); this.Tb.setBackground(this.g3Color);
/*  711 */     this.Tb.setOpaque(true); this.Tb.setHorizontalAlignment(0);
/*  712 */     this.Tb.setVerticalAlignment(1);
/*      */ 
/*  714 */     this.Dy = new JLabel(convMultL("66//Dy")); this.Dy.setFont(this.fnt);
/*  715 */     this.Dy.setBounds(this.c13, this.f8, this.width, this.height);
/*  716 */     this.deskTable.add(this.Dy); this.Dy.setBackground(this.g3Color);
/*  717 */     this.Dy.setOpaque(true); this.Dy.setHorizontalAlignment(0);
/*  718 */     this.Dy.setVerticalAlignment(1);
/*      */ 
/*  720 */     this.Ho = new JLabel(convMultL("67//Ho")); this.Ho.setFont(this.fnt);
/*  721 */     this.Ho.setBounds(this.c14, this.f8, this.width, this.height);
/*  722 */     this.deskTable.add(this.Ho); this.Ho.setBackground(this.g3Color);
/*  723 */     this.Ho.setOpaque(true); this.Ho.setHorizontalAlignment(0);
/*  724 */     this.Ho.setVerticalAlignment(1);
/*      */ 
/*  726 */     this.Er = new JLabel(convMultL("68//Er")); this.Er.setFont(this.fnt);
/*  727 */     this.Er.setBounds(this.c15, this.f8, this.width, this.height);
/*  728 */     this.deskTable.add(this.Er); this.Er.setBackground(this.g3Color);
/*  729 */     this.Er.setOpaque(true); this.Er.setHorizontalAlignment(0);
/*  730 */     this.Er.setVerticalAlignment(1);
/*      */ 
/*  732 */     this.Tm = new JLabel(convMultL("69//Tm")); this.Tm.setFont(this.fnt);
/*  733 */     this.Tm.setBounds(this.c16, this.f8, this.width, this.height);
/*  734 */     this.deskTable.add(this.Tm); this.Tm.setBackground(this.g3Color);
/*  735 */     this.Tm.setOpaque(true); this.Tm.setHorizontalAlignment(0);
/*  736 */     this.Tm.setVerticalAlignment(1);
/*      */ 
/*  738 */     this.Yb = new JLabel(convMultL("70//Dy")); this.Yb.setFont(this.fnt);
/*  739 */     this.Yb.setBounds(this.c17, this.f8, this.width, this.height);
/*  740 */     this.deskTable.add(this.Yb); this.Yb.setBackground(this.g3Color);
/*  741 */     this.Yb.setOpaque(true); this.Yb.setHorizontalAlignment(0);
/*  742 */     this.Yb.setVerticalAlignment(1);
/*      */ 
/*  744 */     this.Lu = new JLabel(convMultL("71//Dy")); this.Lu.setFont(this.fnt);
/*  745 */     this.Lu.setBounds(this.c18, this.f8, this.width, this.height);
/*  746 */     this.deskTable.add(this.Lu); this.Lu.setBackground(this.g3Color);
/*  747 */     this.Lu.setOpaque(true); this.Lu.setHorizontalAlignment(0);
/*  748 */     this.Lu.setVerticalAlignment(1);
/*      */ 
/*  750 */     this.Ac = new JLabel(convMultL("89//Ac")); this.Ac.setFont(this.fnt);
/*  751 */     this.Ac.setBounds(this.c4, this.f9, this.width, this.height);
/*  752 */     this.deskTable.add(this.Ac); this.Ac.setBackground(this.g3Color);
/*  753 */     this.Ac.setOpaque(true); this.Ac.setHorizontalAlignment(0);
/*  754 */     this.Ac.setVerticalAlignment(1);
/*      */ 
/*  756 */     this.Th = new JLabel(convMultL("90//Th")); this.Th.setFont(this.fnt);
/*  757 */     this.Th.setBounds(this.c5, this.f9, this.width, this.height);
/*  758 */     this.deskTable.add(this.Th); this.Th.setBackground(this.g3Color);
/*  759 */     this.Th.setOpaque(true); this.Th.setHorizontalAlignment(0);
/*  760 */     this.Th.setVerticalAlignment(1);
/*      */ 
/*  762 */     this.Pa = new JLabel(convMultL("91//Pa")); this.Pa.setFont(this.fnt);
/*  763 */     this.Pa.setBounds(this.c6, this.f9, this.width, this.height);
/*  764 */     this.deskTable.add(this.Pa); this.Pa.setBackground(this.g3Color);
/*  765 */     this.Pa.setOpaque(true); this.Pa.setHorizontalAlignment(0);
/*  766 */     this.Pa.setVerticalAlignment(1);
/*      */ 
/*  768 */     this.U = new JLabel(convMultL("92//U")); this.U.setFont(this.fnt);
/*  769 */     this.U.setBounds(this.c7, this.f9, this.width, this.height);
/*  770 */     this.deskTable.add(this.U); this.U.setBackground(this.g3Color);
/*  771 */     this.U.setOpaque(true); this.U.setHorizontalAlignment(0);
/*  772 */     this.U.setVerticalAlignment(1);
/*      */ 
/*  774 */     this.Np = new JLabel(convMultL("93//Np")); this.Np.setFont(this.fnt);
/*  775 */     this.Np.setBounds(this.c8, this.f9, this.width, this.height);
/*  776 */     this.deskTable.add(this.Np); this.Np.setBackground(this.g3Color);
/*  777 */     this.Np.setOpaque(true); this.Np.setHorizontalAlignment(0);
/*  778 */     this.Np.setVerticalAlignment(1);
/*      */ 
/*  780 */     this.Pu = new JLabel(convMultL("94//Pu")); this.Pu.setFont(this.fnt);
/*  781 */     this.Pu.setBounds(this.c9, this.f9, this.width, this.height);
/*  782 */     this.deskTable.add(this.Pu); this.Pu.setBackground(this.g3Color);
/*  783 */     this.Pu.setOpaque(true); this.Pu.setHorizontalAlignment(0);
/*  784 */     this.Pu.setVerticalAlignment(1);
/*      */ 
/*  786 */     this.Am = new JLabel(convMultL("95//Am")); this.Am.setFont(this.fnt);
/*  787 */     this.Am.setBounds(this.c10, this.f9, this.width, this.height);
/*  788 */     this.deskTable.add(this.Am); this.Am.setBackground(this.g3Color);
/*  789 */     this.Am.setOpaque(true); this.Am.setHorizontalAlignment(0);
/*  790 */     this.Am.setVerticalAlignment(1);
/*      */ 
/*  792 */     this.Cm = new JLabel(convMultL("96//Cm")); this.Cm.setFont(this.fnt);
/*  793 */     this.Cm.setBounds(this.c11, this.f9, this.width, this.height);
/*  794 */     this.deskTable.add(this.Cm); this.Cm.setBackground(this.g3Color);
/*  795 */     this.Cm.setOpaque(true); this.Cm.setHorizontalAlignment(0);
/*  796 */     this.Cm.setVerticalAlignment(1);
/*      */ 
/*  798 */     this.Bk = new JLabel(convMultL("97//Bk")); this.Bk.setFont(this.fnt);
/*  799 */     this.Bk.setBounds(this.c12, this.f9, this.width, this.height);
/*  800 */     this.deskTable.add(this.Bk); this.Bk.setBackground(this.g3Color);
/*  801 */     this.Bk.setOpaque(true); this.Bk.setHorizontalAlignment(0);
/*  802 */     this.Bk.setVerticalAlignment(1);
/*      */ 
/*  804 */     this.Cf = new JLabel(convMultL("98//Cf")); this.Cf.setFont(this.fnt);
/*  805 */     this.Cf.setBounds(this.c13, this.f9, this.width, this.height);
/*  806 */     this.deskTable.add(this.Cf); this.Cf.setBackground(this.g3Color);
/*  807 */     this.Cf.setOpaque(true); this.Cf.setHorizontalAlignment(0);
/*  808 */     this.Cf.setVerticalAlignment(1);
/*      */ 
/*  810 */     this.Es = new JLabel(convMultL("99//Es")); this.Es.setFont(this.fnt);
/*  811 */     this.Es.setBounds(this.c14, this.f9, this.width, this.height);
/*  812 */     this.deskTable.add(this.Es); this.Es.setBackground(this.g3Color);
/*  813 */     this.Es.setOpaque(true); this.Es.setHorizontalAlignment(0);
/*  814 */     this.Es.setVerticalAlignment(1);
/*      */ 
/*  816 */     this.Fm = new JLabel(convMultL("100//Fm")); this.Fm.setFont(this.fnt);
/*  817 */     this.Fm.setBounds(this.c15, this.f9, this.width, this.height);
/*  818 */     this.deskTable.add(this.Fm); this.Fm.setBackground(this.g3Color);
/*  819 */     this.Fm.setOpaque(true); this.Fm.setHorizontalAlignment(0);
/*  820 */     this.Fm.setVerticalAlignment(1);
/*      */ 
/*  822 */     this.Md = new JLabel(convMultL("101//Ac")); this.Md.setFont(this.fnt);
/*  823 */     this.Md.setBounds(this.c16, this.f9, this.width, this.height);
/*  824 */     this.deskTable.add(this.Md); this.Md.setBackground(this.g3Color);
/*  825 */     this.Md.setOpaque(true); this.Md.setHorizontalAlignment(0);
/*  826 */     this.Md.setVerticalAlignment(1);
/*      */ 
/*  828 */     this.No = new JLabel(convMultL("102//No")); this.No.setFont(this.fnt);
/*  829 */     this.No.setBounds(this.c17, this.f9, this.width, this.height);
/*  830 */     this.deskTable.add(this.No); this.No.setBackground(this.g3Color);
/*  831 */     this.No.setOpaque(true); this.No.setHorizontalAlignment(0);
/*  832 */     this.No.setVerticalAlignment(1);
/*      */ 
/*  834 */     this.Lr = new JLabel(convMultL("103//Lr")); this.Lr.setFont(this.fnt);
/*  835 */     this.Lr.setBounds(this.c18, this.f9, this.width, this.height);
/*  836 */     this.deskTable.add(this.Lr); this.Lr.setBackground(this.g3Color);
/*  837 */     this.Lr.setOpaque(true); this.Lr.setHorizontalAlignment(0);
/*  838 */     this.Lr.setVerticalAlignment(1);
/*      */ 
/*  840 */     this.chkMultipleIonization.setFont(new Font("Courier New", 0, 14));
/*  841 */     this.chkMultipleIonization.setText("Multiple ionización");
/*  842 */     this.chkMultipleIonization.setBounds(550, 160, 180, 20);
/*  843 */     this.chkMultipleIonization.setOpaque(false);
/*  844 */     this.chkMultipleIonization.setSelected(false);
/*      */ 
/*  846 */     this.chkFileOpen.setFont(new Font("Courier New", 0, 14));
/*  847 */     this.chkFileOpen.setText("Ver Documento");
/*  848 */     this.chkFileOpen.setBounds(550, 100, 140, 20);
/*  849 */     this.chkFileOpen.setOpaque(false);
/*  850 */     this.chkFileOpen.setSelected(false);
/*      */ 
/*  852 */     chkPersistenteLine.setVerticalTextPosition(SwingConstants.TOP);
     this.chkPersistenteLine.setFont(new Font("Courier New", 0, 14));
/*  853 */     this.chkPersistenteLine.setText("<html> Líneas características &nbsp; <br />  LIBS<html> ");
/*  854 */     this.chkPersistenteLine.setBounds(550, 190, 250, 40);
/*  855 */     this.chkPersistenteLine.setOpaque(false);
/*  856 */     this.chkPersistenteLine.setSelected(false);
/*      */ 
/*  858 */     this.toler.setFont(new Font("Courier New", 0, 14));
/*  859 */     this.toler.setText("Tolerancia: ");
/*  860 */     this.toler.setBounds(560, 130, 160, 20);
/*  861 */     this.spinnerTolerance.setBounds(650, 130, 40, 20);
/*      */ 
/*  863 */     this.buttIdentify.setFont(new Font("Courier New", 0, 14));
/*  864 */     this.buttIdentify.setText("Identificar");
/*  865 */     this.buttIdentify.setBounds(560, 270, 140, 40);
/*  866 */     this.buttIdentify.addActionListener(new ActionListener()
/*      */     {
/*      */       public void actionPerformed(ActionEvent evt) {
/*  869 */         PeriodicTableIFrame.this.selectElements();
/*  870 */         PeriodicTableIFrame.this.getPeaks();
/*      */ 
/*  872 */         PeriodicTableIFrame.this.findElements();
/*      */       }
/*      */     });
/*  877 */     this.deskTable.add(this.spinnerTolerance);
/*  878 */     this.deskTable.add(this.toler);
/*  879 */     this.deskTable.add(this.chkMultipleIonization);
/*  880 */     this.deskTable.add(this.chkPersistenteLine);
/*  881 */     this.deskTable.add(this.buttIdentify);
/*      */ 
/*  883 */     getContentPane().add(this.deskTable);
/*      */   }
/*      */   private String convMultL(String Org) {
/*  886 */     return "<html><font size=2>" + Org.replaceAll("//", "</font><br>");
/*      */   }
/*      */   private void orderComponents() {
/*  889 */     setSize(760, 400);
/*      */ 
/*  891 */     this.H.addMouseMotionListener(new MouseMotionAdapter()
/*      */     {
/*      */       public void mouseMoved(MouseEvent evt) {
/*  894 */         PeriodicTableIFrame.this.H.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/*  895 */     this.H.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/*  897 */         PeriodicTableIFrame.this.H.setBorder(null);
/*      */       }
/*  899 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("H");
/*  900 */         if (PeriodicTableIFrame.this.vH == true) {
/*  901 */           PeriodicTableIFrame.this.vH = false;
/*  902 */           PeriodicTableIFrame.this.H.setBackground(PeriodicTableIFrame.this.g1Color);
/*  903 */         } else if (!PeriodicTableIFrame.this.vH) {
/*  904 */           PeriodicTableIFrame.this.vH = true;
/*  905 */           PeriodicTableIFrame.this.H.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/*  908 */     this.He.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/*  910 */         PeriodicTableIFrame.this.He.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/*  911 */     this.He.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/*  913 */         PeriodicTableIFrame.this.He.setBorder(null);
/*      */       }
/*  915 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("He");
/*  916 */         if (PeriodicTableIFrame.this.vHe == true) {
/*  917 */           PeriodicTableIFrame.this.vHe = false;
/*  918 */           PeriodicTableIFrame.this.He.setBackground(PeriodicTableIFrame.this.g1Color);
/*  919 */         } else if (!PeriodicTableIFrame.this.vHe) {
/*  920 */           PeriodicTableIFrame.this.vHe = true;
/*  921 */           PeriodicTableIFrame.this.He.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/*  923 */     this.Li.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/*  925 */         PeriodicTableIFrame.this.Li.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/*  926 */     this.Li.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/*  928 */         PeriodicTableIFrame.this.Li.setBorder(null);
/*      */       }
/*  930 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Li");
/*  931 */         if (PeriodicTableIFrame.this.vLi == true) {
/*  932 */           PeriodicTableIFrame.this.vLi = false;
/*  933 */           PeriodicTableIFrame.this.Li.setBackground(PeriodicTableIFrame.this.g0Color);
/*  934 */         } else if (!PeriodicTableIFrame.this.vLi) {
/*  935 */           PeriodicTableIFrame.this.vLi = true;
/*  936 */           PeriodicTableIFrame.this.Li.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/*  939 */     this.Be.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/*  941 */         PeriodicTableIFrame.this.Be.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/*  942 */     this.Be.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/*  944 */         PeriodicTableIFrame.this.Be.setBorder(null);
/*      */       }
/*  946 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Be");
/*  947 */         if (PeriodicTableIFrame.this.vBe == true) {
/*  948 */           PeriodicTableIFrame.this.vBe = false;
/*  949 */           PeriodicTableIFrame.this.Be.setBackground(PeriodicTableIFrame.this.g0Color);
/*  950 */         } else if (!PeriodicTableIFrame.this.vBe) {
/*  951 */           PeriodicTableIFrame.this.vBe = true;
/*  952 */           PeriodicTableIFrame.this.Be.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/*  955 */     this.B.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/*  957 */         PeriodicTableIFrame.this.B.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/*  958 */     this.B.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/*  960 */         PeriodicTableIFrame.this.B.setBorder(null);
/*      */       }
/*  962 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("B");
/*  963 */         if (PeriodicTableIFrame.this.vB == true) {
/*  964 */           PeriodicTableIFrame.this.vB = false;
/*  965 */           PeriodicTableIFrame.this.B.setBackground(PeriodicTableIFrame.this.g2Color);
/*  966 */         } else if (!PeriodicTableIFrame.this.vB) {
/*  967 */           PeriodicTableIFrame.this.vB = true;
/*  968 */           PeriodicTableIFrame.this.B.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/*  971 */     this.C.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/*  973 */         PeriodicTableIFrame.this.C.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/*  974 */     this.C.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/*  976 */         PeriodicTableIFrame.this.C.setBorder(null);
/*      */       }
/*  978 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("C");
/*  979 */         if (PeriodicTableIFrame.this.vC == true) {
/*  980 */           PeriodicTableIFrame.this.vC = false;
/*  981 */           PeriodicTableIFrame.this.C.setBackground(PeriodicTableIFrame.this.g1Color);
/*  982 */         } else if (!PeriodicTableIFrame.this.vC) {
/*  983 */           PeriodicTableIFrame.this.vC = true;
/*  984 */           PeriodicTableIFrame.this.C.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/*  987 */     this.N.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/*  989 */         PeriodicTableIFrame.this.N.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/*  990 */     this.N.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/*  992 */         PeriodicTableIFrame.this.N.setBorder(null);
/*      */       }
/*  994 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("N");
/*  995 */         if (PeriodicTableIFrame.this.vN == true) {
/*  996 */           PeriodicTableIFrame.this.vN = false;
/*  997 */           PeriodicTableIFrame.this.N.setBackground(PeriodicTableIFrame.this.g1Color);
/*  998 */         } else if (!PeriodicTableIFrame.this.vN) {
/*  999 */           PeriodicTableIFrame.this.vN = true;
/* 1000 */           PeriodicTableIFrame.this.N.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1003 */     this.O.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1005 */         PeriodicTableIFrame.this.O.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1006 */     this.O.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1008 */         PeriodicTableIFrame.this.O.setBorder(null);
/*      */       }
/* 1010 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("O");
/* 1011 */         if (PeriodicTableIFrame.this.vO == true) {
/* 1012 */           PeriodicTableIFrame.this.vO = false;
/* 1013 */           PeriodicTableIFrame.this.O.setBackground(PeriodicTableIFrame.this.g1Color);
/* 1014 */         } else if (!PeriodicTableIFrame.this.vO) {
/* 1015 */           PeriodicTableIFrame.this.vO = true;
/* 1016 */           PeriodicTableIFrame.this.O.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1019 */     this.F.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1021 */         PeriodicTableIFrame.this.F.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1022 */     this.F.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1024 */         PeriodicTableIFrame.this.F.setBorder(null);
/*      */       }
/* 1026 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("F");
/* 1027 */         if (PeriodicTableIFrame.this.vF == true) {
/* 1028 */           PeriodicTableIFrame.this.vF = false;
/* 1029 */           PeriodicTableIFrame.this.F.setBackground(PeriodicTableIFrame.this.g1Color);
/* 1030 */         } else if (!PeriodicTableIFrame.this.vF) {
/* 1031 */           PeriodicTableIFrame.this.vF = true;
/* 1032 */           PeriodicTableIFrame.this.F.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1035 */     this.Ne.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1037 */         PeriodicTableIFrame.this.Ne.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1038 */     this.Ne.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1040 */         PeriodicTableIFrame.this.Ne.setBorder(null);
/*      */       }
/* 1042 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Ne");
/* 1043 */         if (PeriodicTableIFrame.this.vNe == true) {
/* 1044 */           PeriodicTableIFrame.this.vNe = false;
/* 1045 */           PeriodicTableIFrame.this.Ne.setBackground(PeriodicTableIFrame.this.g1Color);
/* 1046 */         } else if (!PeriodicTableIFrame.this.vNe) {
/* 1047 */           PeriodicTableIFrame.this.vNe = true;
/* 1048 */           PeriodicTableIFrame.this.Ne.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1051 */     this.Na.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1053 */         PeriodicTableIFrame.this.Na.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1054 */     this.Na.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1056 */         PeriodicTableIFrame.this.Na.setBorder(null);
/*      */       }
/* 1058 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Na");
/* 1059 */         if (PeriodicTableIFrame.this.vNa == true) {
/* 1060 */           PeriodicTableIFrame.this.vNa = false;
/* 1061 */           PeriodicTableIFrame.this.Na.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1062 */         } else if (!PeriodicTableIFrame.this.vNa) {
/* 1063 */           PeriodicTableIFrame.this.vNa = true;
/* 1064 */           PeriodicTableIFrame.this.Na.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1067 */     this.Mg.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1069 */         PeriodicTableIFrame.this.Mg.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1070 */     this.Mg.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1072 */         PeriodicTableIFrame.this.Mg.setBorder(null);
/*      */       }
/* 1074 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Mg");
/* 1075 */         if (PeriodicTableIFrame.this.vMg == true) {
/* 1076 */           PeriodicTableIFrame.this.vMg = false;
/* 1077 */           PeriodicTableIFrame.this.Mg.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1078 */         } else if (!PeriodicTableIFrame.this.vMg) {
/* 1079 */           PeriodicTableIFrame.this.vMg = true;
/* 1080 */           PeriodicTableIFrame.this.Mg.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1083 */     this.Al.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1085 */         PeriodicTableIFrame.this.Al.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1086 */     this.Al.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1088 */         PeriodicTableIFrame.this.Al.setBorder(null);
/*      */       }
/* 1090 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Al");
/* 1091 */         if (PeriodicTableIFrame.this.vAl == true) {
/* 1092 */           PeriodicTableIFrame.this.vAl = false;
/* 1093 */           PeriodicTableIFrame.this.Al.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1094 */         } else if (!PeriodicTableIFrame.this.vAl) {
/* 1095 */           PeriodicTableIFrame.this.vAl = true;
/* 1096 */           PeriodicTableIFrame.this.Al.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1099 */     this.Si.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1101 */         PeriodicTableIFrame.this.Si.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1102 */     this.Si.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1104 */         PeriodicTableIFrame.this.Si.setBorder(null);
/*      */       }
/* 1106 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Si");
/* 1107 */         if (PeriodicTableIFrame.this.vSi == true) {
/* 1108 */           PeriodicTableIFrame.this.vSi = false;
/* 1109 */           PeriodicTableIFrame.this.Si.setBackground(PeriodicTableIFrame.this.g2Color);
/* 1110 */         } else if (!PeriodicTableIFrame.this.vSi) {
/* 1111 */           PeriodicTableIFrame.this.vSi = true;
/* 1112 */           PeriodicTableIFrame.this.Si.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1115 */     this.P.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1117 */         PeriodicTableIFrame.this.P.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1118 */     this.P.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1120 */         PeriodicTableIFrame.this.P.setBorder(null);
/*      */       }
/* 1122 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("P");
/* 1123 */         if (PeriodicTableIFrame.this.vP == true) {
/* 1124 */           PeriodicTableIFrame.this.vP = false;
/* 1125 */           PeriodicTableIFrame.this.P.setBackground(PeriodicTableIFrame.this.g1Color);
/* 1126 */         } else if (!PeriodicTableIFrame.this.vP) {
/* 1127 */           PeriodicTableIFrame.this.vP = true;
/* 1128 */           PeriodicTableIFrame.this.P.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1131 */     this.S.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1133 */         PeriodicTableIFrame.this.S.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1134 */     this.S.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1136 */         PeriodicTableIFrame.this.S.setBorder(null);
/*      */       }
/* 1138 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("S");
/* 1139 */         if (PeriodicTableIFrame.this.vS == true) {
/* 1140 */           PeriodicTableIFrame.this.vS = false;
/* 1141 */           PeriodicTableIFrame.this.S.setBackground(PeriodicTableIFrame.this.g1Color);
/* 1142 */         } else if (!PeriodicTableIFrame.this.vS) {
/* 1143 */           PeriodicTableIFrame.this.vS = true;
/* 1144 */           PeriodicTableIFrame.this.S.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1147 */     this.Cl.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1149 */         PeriodicTableIFrame.this.Cl.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1150 */     this.Cl.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1152 */         PeriodicTableIFrame.this.Cl.setBorder(null);
/*      */       }
/* 1154 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Cl");
/* 1155 */         if (PeriodicTableIFrame.this.vCl == true) {
/* 1156 */           PeriodicTableIFrame.this.vCl = false;
/* 1157 */           PeriodicTableIFrame.this.Cl.setBackground(PeriodicTableIFrame.this.g1Color);
/* 1158 */         } else if (!PeriodicTableIFrame.this.vCl) {
/* 1159 */           PeriodicTableIFrame.this.vCl = true;
/* 1160 */           PeriodicTableIFrame.this.Cl.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1163 */     this.Ar.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1165 */         PeriodicTableIFrame.this.Ar.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1166 */     this.Ar.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1168 */         PeriodicTableIFrame.this.Ar.setBorder(null);
/*      */       }
/* 1170 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Ar");
/* 1171 */         if (PeriodicTableIFrame.this.vAr == true) {
/* 1172 */           PeriodicTableIFrame.this.vAr = false;
/* 1173 */           PeriodicTableIFrame.this.Ar.setBackground(PeriodicTableIFrame.this.g1Color);
/* 1174 */         } else if (!PeriodicTableIFrame.this.vAr) {
/* 1175 */           PeriodicTableIFrame.this.vAr = true;
/* 1176 */           PeriodicTableIFrame.this.Ar.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1179 */     this.K.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1181 */         PeriodicTableIFrame.this.K.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1182 */     this.K.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1184 */         PeriodicTableIFrame.this.K.setBorder(null);
/*      */       }
/* 1186 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("K");
/* 1187 */         if (PeriodicTableIFrame.this.vK == true) {
/* 1188 */           PeriodicTableIFrame.this.vK = false;
/* 1189 */           PeriodicTableIFrame.this.K.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1190 */         } else if (!PeriodicTableIFrame.this.vK) {
/* 1191 */           PeriodicTableIFrame.this.vK = true;
/* 1192 */           PeriodicTableIFrame.this.K.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1195 */     this.Ca.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1197 */         PeriodicTableIFrame.this.Ca.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1198 */     this.Ca.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1200 */         PeriodicTableIFrame.this.Ca.setBorder(null);
/*      */       }
/* 1202 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Ca");
/* 1203 */         if (PeriodicTableIFrame.this.vCa == true) {
/* 1204 */           PeriodicTableIFrame.this.vCa = false;
/* 1205 */           PeriodicTableIFrame.this.Ca.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1206 */         } else if (!PeriodicTableIFrame.this.vCa) {
/* 1207 */           PeriodicTableIFrame.this.vCa = true;
/* 1208 */           PeriodicTableIFrame.this.Ca.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1211 */     this.Sc.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1213 */         PeriodicTableIFrame.this.Sc.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1214 */     this.Sc.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1216 */         PeriodicTableIFrame.this.Sc.setBorder(null);
/*      */       }
/* 1218 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Sc");
/* 1219 */         if (PeriodicTableIFrame.this.vSc == true) {
/* 1220 */           PeriodicTableIFrame.this.vSc = false;
/* 1221 */           PeriodicTableIFrame.this.Sc.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1222 */         } else if (!PeriodicTableIFrame.this.vSc) {
/* 1223 */           PeriodicTableIFrame.this.vSc = true;
/* 1224 */           PeriodicTableIFrame.this.Sc.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1227 */     this.Ti.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1229 */         PeriodicTableIFrame.this.Ti.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1230 */     this.Ti.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1232 */         PeriodicTableIFrame.this.Ti.setBorder(null);
/*      */       }
/* 1234 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Ti");
/* 1235 */         if (PeriodicTableIFrame.this.vTi == true) {
/* 1236 */           PeriodicTableIFrame.this.vTi = false;
/* 1237 */           PeriodicTableIFrame.this.Ti.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1238 */         } else if (!PeriodicTableIFrame.this.vTi) {
/* 1239 */           PeriodicTableIFrame.this.vTi = true;
/* 1240 */           PeriodicTableIFrame.this.Ti.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1244 */     this.V.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1246 */         PeriodicTableIFrame.this.V.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1247 */     this.V.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1249 */         PeriodicTableIFrame.this.V.setBorder(null);
/*      */       }
/* 1251 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("V");
/* 1252 */         if (PeriodicTableIFrame.this.vV == true) {
/* 1253 */           PeriodicTableIFrame.this.vV = false;
/* 1254 */           PeriodicTableIFrame.this.V.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1255 */         } else if (!PeriodicTableIFrame.this.vV) {
/* 1256 */           PeriodicTableIFrame.this.vV = true;
/* 1257 */           PeriodicTableIFrame.this.V.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1260 */     this.Cr.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1262 */         PeriodicTableIFrame.this.Cr.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1263 */     this.Cr.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1265 */         PeriodicTableIFrame.this.Cr.setBorder(null);
/*      */       }
/* 1267 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Cr");
/* 1268 */         if (PeriodicTableIFrame.this.vCr == true) {
/* 1269 */           PeriodicTableIFrame.this.vCr = false;
/* 1270 */           PeriodicTableIFrame.this.Cr.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1271 */         } else if (!PeriodicTableIFrame.this.vCr) {
/* 1272 */           PeriodicTableIFrame.this.vCr = true;
/* 1273 */           PeriodicTableIFrame.this.Cr.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1276 */     this.Mn.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1278 */         PeriodicTableIFrame.this.Mn.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1279 */     this.Mn.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1281 */         PeriodicTableIFrame.this.Mn.setBorder(null);
/*      */       }
/* 1283 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Mn");
/* 1284 */         if (PeriodicTableIFrame.this.vMn == true) {
/* 1285 */           PeriodicTableIFrame.this.vMn = false;
/* 1286 */           PeriodicTableIFrame.this.Mn.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1287 */         } else if (!PeriodicTableIFrame.this.vMn) {
/* 1288 */           PeriodicTableIFrame.this.vMn = true;
/* 1289 */           PeriodicTableIFrame.this.Mn.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1292 */     this.Fe.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1294 */         PeriodicTableIFrame.this.Fe.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1295 */     this.Fe.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1297 */         PeriodicTableIFrame.this.Fe.setBorder(null);
/*      */       }
/* 1299 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Fe");
/* 1300 */         if (PeriodicTableIFrame.this.vFe == true) {
/* 1301 */           PeriodicTableIFrame.this.vFe = false;
/* 1302 */           PeriodicTableIFrame.this.Fe.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1303 */         } else if (!PeriodicTableIFrame.this.vLi) {
/* 1304 */           PeriodicTableIFrame.this.vFe = true;
/* 1305 */           PeriodicTableIFrame.this.Fe.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1308 */     this.Co.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1310 */         PeriodicTableIFrame.this.Co.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1311 */     this.Co.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1313 */         PeriodicTableIFrame.this.Co.setBorder(null);
/*      */       }
/* 1315 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Co");
/* 1316 */         if (PeriodicTableIFrame.this.vCo == true) {
/* 1317 */           PeriodicTableIFrame.this.vCo = false;
/* 1318 */           PeriodicTableIFrame.this.Co.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1319 */         } else if (!PeriodicTableIFrame.this.vCo) {
/* 1320 */           PeriodicTableIFrame.this.vCo = true;
/* 1321 */           PeriodicTableIFrame.this.Co.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1324 */     this.Ni.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1326 */         PeriodicTableIFrame.this.Ni.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1327 */     this.Ni.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1329 */         PeriodicTableIFrame.this.Ni.setBorder(null);
/*      */       }
/* 1331 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Ni");
/* 1332 */         if (PeriodicTableIFrame.this.vNi == true) {
/* 1333 */           PeriodicTableIFrame.this.vNi = false;
/* 1334 */           PeriodicTableIFrame.this.Ni.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1335 */         } else if (!PeriodicTableIFrame.this.vNi) {
/* 1336 */           PeriodicTableIFrame.this.vNi = true;
/* 1337 */           PeriodicTableIFrame.this.Ni.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1340 */     this.Cu.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1342 */         PeriodicTableIFrame.this.Cu.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1343 */     this.Cu.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1345 */         PeriodicTableIFrame.this.Cu.setBorder(null);
/*      */       }
/* 1347 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Cu");
/* 1348 */         if (PeriodicTableIFrame.this.vCu == true) {
/* 1349 */           PeriodicTableIFrame.this.vCu = false;
/* 1350 */           PeriodicTableIFrame.this.Cu.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1351 */         } else if (!PeriodicTableIFrame.this.vCu) {
/* 1352 */           PeriodicTableIFrame.this.vCu = true;
/* 1353 */           PeriodicTableIFrame.this.Cu.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1356 */     this.Zn.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1358 */         PeriodicTableIFrame.this.Zn.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1359 */     this.Zn.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1361 */         PeriodicTableIFrame.this.Zn.setBorder(null);
/*      */       }
/* 1363 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Zn");
/* 1364 */         if (PeriodicTableIFrame.this.vZn == true) {
/* 1365 */           PeriodicTableIFrame.this.vZn = false;
/* 1366 */           PeriodicTableIFrame.this.Zn.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1367 */         } else if (!PeriodicTableIFrame.this.vZn) {
/* 1368 */           PeriodicTableIFrame.this.vZn = true;
/* 1369 */           PeriodicTableIFrame.this.Zn.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1372 */     this.Ga.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1374 */         PeriodicTableIFrame.this.Ga.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1375 */     this.Ga.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1377 */         PeriodicTableIFrame.this.Ga.setBorder(null);
/*      */       }
/* 1379 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Ga");
/* 1380 */         if (PeriodicTableIFrame.this.vGa == true) {
/* 1381 */           PeriodicTableIFrame.this.vGa = false;
/* 1382 */           PeriodicTableIFrame.this.Ga.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1383 */         } else if (!PeriodicTableIFrame.this.vGa) {
/* 1384 */           PeriodicTableIFrame.this.vGa = true;
/* 1385 */           PeriodicTableIFrame.this.Ga.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1388 */     this.Ge.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1390 */         PeriodicTableIFrame.this.Ge.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1391 */     this.Ge.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1393 */         PeriodicTableIFrame.this.Ge.setBorder(null);
/*      */       }
/* 1395 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Ge");
/* 1396 */         if (PeriodicTableIFrame.this.vGe == true) {
/* 1397 */           PeriodicTableIFrame.this.vGe = false;
/* 1398 */           PeriodicTableIFrame.this.Ge.setBackground(PeriodicTableIFrame.this.g2Color);
/* 1399 */         } else if (!PeriodicTableIFrame.this.vGe) {
/* 1400 */           PeriodicTableIFrame.this.vGe = true;
/* 1401 */           PeriodicTableIFrame.this.Ge.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1404 */     this.As.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1406 */         PeriodicTableIFrame.this.As.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1407 */     this.As.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1409 */         PeriodicTableIFrame.this.As.setBorder(null);
/*      */       }
/* 1411 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("As");
/* 1412 */         if (PeriodicTableIFrame.this.vAs == true) {
/* 1413 */           PeriodicTableIFrame.this.vAs = false;
/* 1414 */           PeriodicTableIFrame.this.As.setBackground(PeriodicTableIFrame.this.g2Color);
/* 1415 */         } else if (!PeriodicTableIFrame.this.vAs) {
/* 1416 */           PeriodicTableIFrame.this.vAs = true;
/* 1417 */           PeriodicTableIFrame.this.As.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1420 */     this.Se.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1422 */         PeriodicTableIFrame.this.Se.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1423 */     this.Se.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1425 */         PeriodicTableIFrame.this.Se.setBorder(null);
/*      */       }
/* 1427 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Se");
/* 1428 */         if (PeriodicTableIFrame.this.vSe == true) {
/* 1429 */           PeriodicTableIFrame.this.vSe = false;
/* 1430 */           PeriodicTableIFrame.this.Se.setBackground(PeriodicTableIFrame.this.g1Color);
/* 1431 */         } else if (!PeriodicTableIFrame.this.vSe) {
/* 1432 */           PeriodicTableIFrame.this.vSe = true;
/* 1433 */           PeriodicTableIFrame.this.Se.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1436 */     this.Br.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1438 */         PeriodicTableIFrame.this.Br.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1439 */     this.Br.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1441 */         PeriodicTableIFrame.this.Br.setBorder(null);
/*      */       }
/* 1443 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Br");
/* 1444 */         if (PeriodicTableIFrame.this.vBr == true) {
/* 1445 */           PeriodicTableIFrame.this.vBr = false;
/* 1446 */           PeriodicTableIFrame.this.Br.setBackground(PeriodicTableIFrame.this.g1Color);
/* 1447 */         } else if (!PeriodicTableIFrame.this.vBr) {
/* 1448 */           PeriodicTableIFrame.this.vBr = true;
/* 1449 */           PeriodicTableIFrame.this.Br.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1452 */     this.Kr.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1454 */         PeriodicTableIFrame.this.Kr.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1455 */     this.Kr.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1457 */         PeriodicTableIFrame.this.Kr.setBorder(null);
/*      */       }
/* 1459 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Kr");
/* 1460 */         if (PeriodicTableIFrame.this.vKr == true) {
/* 1461 */           PeriodicTableIFrame.this.vKr = false;
/* 1462 */           PeriodicTableIFrame.this.Kr.setBackground(PeriodicTableIFrame.this.g1Color);
/* 1463 */         } else if (!PeriodicTableIFrame.this.vKr) {
/* 1464 */           PeriodicTableIFrame.this.vKr = true;
/* 1465 */           PeriodicTableIFrame.this.Kr.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1468 */     this.Rb.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1470 */         PeriodicTableIFrame.this.Rb.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1471 */     this.Rb.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1473 */         PeriodicTableIFrame.this.Rb.setBorder(null);
/*      */       }
/* 1475 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Rb");
/* 1476 */         if (PeriodicTableIFrame.this.vRb == true) {
/* 1477 */           PeriodicTableIFrame.this.vRb = false;
/* 1478 */           PeriodicTableIFrame.this.Rb.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1479 */         } else if (!PeriodicTableIFrame.this.vRb) {
/* 1480 */           PeriodicTableIFrame.this.vRb = true;
/* 1481 */           PeriodicTableIFrame.this.Rb.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1484 */     this.Sr.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1486 */         PeriodicTableIFrame.this.Sr.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1487 */     this.Sr.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1489 */         PeriodicTableIFrame.this.Sr.setBorder(null);
/*      */       }
/* 1491 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Sr");
/* 1492 */         if (PeriodicTableIFrame.this.vSr == true) {
/* 1493 */           PeriodicTableIFrame.this.vSr = false;
/* 1494 */           PeriodicTableIFrame.this.Sr.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1495 */         } else if (!PeriodicTableIFrame.this.vSr) {
/* 1496 */           PeriodicTableIFrame.this.vSr = true;
/* 1497 */           PeriodicTableIFrame.this.Sr.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1500 */     this.Y.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1502 */         PeriodicTableIFrame.this.Y.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1503 */     this.Y.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1505 */         PeriodicTableIFrame.this.Y.setBorder(null);
/*      */       }
/* 1507 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Y");
/* 1508 */         if (PeriodicTableIFrame.this.vY == true) {
/* 1509 */           PeriodicTableIFrame.this.vY = false;
/* 1510 */           PeriodicTableIFrame.this.Y.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1511 */         } else if (!PeriodicTableIFrame.this.vY) {
/* 1512 */           PeriodicTableIFrame.this.vY = true;
/* 1513 */           PeriodicTableIFrame.this.Y.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1516 */     this.Zr.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1518 */         PeriodicTableIFrame.this.Zr.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1519 */     this.Zr.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1521 */         PeriodicTableIFrame.this.Zr.setBorder(null);
/*      */       }
/* 1523 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Zr");
/* 1524 */         if (PeriodicTableIFrame.this.vZr == true) {
/* 1525 */           PeriodicTableIFrame.this.vZr = false;
/* 1526 */           PeriodicTableIFrame.this.Zr.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1527 */         } else if (!PeriodicTableIFrame.this.vZr) {
/* 1528 */           PeriodicTableIFrame.this.vZr = true;
/* 1529 */           PeriodicTableIFrame.this.Zr.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1532 */     this.Nb.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1534 */         PeriodicTableIFrame.this.Nb.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1535 */     this.Nb.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1537 */         PeriodicTableIFrame.this.Nb.setBorder(null);
/*      */       }
/* 1539 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Nb");
/* 1540 */         if (PeriodicTableIFrame.this.vNb == true) {
/* 1541 */           PeriodicTableIFrame.this.vNb = false;
/* 1542 */           PeriodicTableIFrame.this.Nb.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1543 */         } else if (!PeriodicTableIFrame.this.vNb) {
/* 1544 */           PeriodicTableIFrame.this.vNb = true;
/* 1545 */           PeriodicTableIFrame.this.Nb.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1548 */     this.Mo.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1550 */         PeriodicTableIFrame.this.Mo.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1551 */     this.Mo.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1553 */         PeriodicTableIFrame.this.Mo.setBorder(null);
/*      */       }
/* 1555 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Mo");
/* 1556 */         if (PeriodicTableIFrame.this.vMo == true) {
/* 1557 */           PeriodicTableIFrame.this.vMo = false;
/* 1558 */           PeriodicTableIFrame.this.Mo.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1559 */         } else if (!PeriodicTableIFrame.this.vMo) {
/* 1560 */           PeriodicTableIFrame.this.vMo = true;
/* 1561 */           PeriodicTableIFrame.this.Mo.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1564 */     this.Tc.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1566 */         PeriodicTableIFrame.this.Tc.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1567 */     this.Tc.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1569 */         PeriodicTableIFrame.this.Tc.setBorder(null);
/*      */       }
/* 1571 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Tc");
/* 1572 */         if (PeriodicTableIFrame.this.vTc == true) {
/* 1573 */           PeriodicTableIFrame.this.vTc = false;
/* 1574 */           PeriodicTableIFrame.this.Tc.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1575 */         } else if (!PeriodicTableIFrame.this.vTc) {
/* 1576 */           PeriodicTableIFrame.this.vTc = true;
/* 1577 */           PeriodicTableIFrame.this.Tc.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1580 */     this.Ru.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1582 */         PeriodicTableIFrame.this.Ru.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1583 */     this.Ru.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1585 */         PeriodicTableIFrame.this.Ru.setBorder(null);
/*      */       }
/* 1587 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Ru");
/* 1588 */         if (PeriodicTableIFrame.this.vRu == true) {
/* 1589 */           PeriodicTableIFrame.this.vRu = false;
/* 1590 */           PeriodicTableIFrame.this.Ru.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1591 */         } else if (!PeriodicTableIFrame.this.vRu) {
/* 1592 */           PeriodicTableIFrame.this.vRu = true;
/* 1593 */           PeriodicTableIFrame.this.Ru.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1596 */     this.Rh.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1598 */         PeriodicTableIFrame.this.Rh.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1599 */     this.Rh.addMouseListener(new MouseAdapter()
/*      */     {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1602 */         PeriodicTableIFrame.this.Rh.setBorder(null);
/*      */       }
/* 1604 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Rh");
/* 1605 */         if (PeriodicTableIFrame.this.vRh == true) {
/* 1606 */           PeriodicTableIFrame.this.vRh = false;
/* 1607 */           PeriodicTableIFrame.this.Rh.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1608 */         } else if (!PeriodicTableIFrame.this.vRh) {
/* 1609 */           PeriodicTableIFrame.this.vRh = true;
/* 1610 */           PeriodicTableIFrame.this.Rh.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1613 */     this.Pd.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1615 */         PeriodicTableIFrame.this.Pd.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1616 */     this.Pd.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1618 */         PeriodicTableIFrame.this.Pd.setBorder(null);
/*      */       }
/* 1620 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Pd");
/* 1621 */         if (PeriodicTableIFrame.this.vPd == true) {
/* 1622 */           PeriodicTableIFrame.this.vPd = false;
/* 1623 */           PeriodicTableIFrame.this.Pd.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1624 */         } else if (!PeriodicTableIFrame.this.vPd) {
/* 1625 */           PeriodicTableIFrame.this.vPd = true;
/* 1626 */           PeriodicTableIFrame.this.Pd.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1629 */     this.Ag.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1631 */         PeriodicTableIFrame.this.Ag.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1632 */     this.Ag.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1634 */         PeriodicTableIFrame.this.Ag.setBorder(null);
/*      */       }
/* 1636 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Ag");
/* 1637 */         if (PeriodicTableIFrame.this.vAg == true) {
/* 1638 */           PeriodicTableIFrame.this.vAg = false;
/* 1639 */           PeriodicTableIFrame.this.Ag.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1640 */         } else if (!PeriodicTableIFrame.this.vAg) {
/* 1641 */           PeriodicTableIFrame.this.vAg = true;
/* 1642 */           PeriodicTableIFrame.this.Ag.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1645 */     this.Cd.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1647 */         PeriodicTableIFrame.this.Cd.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1648 */     this.Cd.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1650 */         PeriodicTableIFrame.this.Cd.setBorder(null);
/*      */       }
/* 1652 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Cd");
/* 1653 */         if (PeriodicTableIFrame.this.vCd == true) {
/* 1654 */           PeriodicTableIFrame.this.vCd = false;
/* 1655 */           PeriodicTableIFrame.this.Cd.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1656 */         } else if (!PeriodicTableIFrame.this.vCd) {
/* 1657 */           PeriodicTableIFrame.this.vCd = true;
/* 1658 */           PeriodicTableIFrame.this.Cd.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1661 */     this.In.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1663 */         PeriodicTableIFrame.this.In.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1664 */     this.In.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1666 */         PeriodicTableIFrame.this.In.setBorder(null);
/*      */       }
/* 1668 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("In");
/* 1669 */         if (PeriodicTableIFrame.this.vIn == true) {
/* 1670 */           PeriodicTableIFrame.this.vIn = false;
/* 1671 */           PeriodicTableIFrame.this.In.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1672 */         } else if (!PeriodicTableIFrame.this.vIn) {
/* 1673 */           PeriodicTableIFrame.this.vIn = true;
/* 1674 */           PeriodicTableIFrame.this.In.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1677 */     this.Sn.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1679 */         PeriodicTableIFrame.this.Sn.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1680 */     this.Sn.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1682 */         PeriodicTableIFrame.this.Sn.setBorder(null);
/*      */       }
/* 1684 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Sn");
/* 1685 */         if (PeriodicTableIFrame.this.vSn == true) {
/* 1686 */           PeriodicTableIFrame.this.vSn = false;
/* 1687 */           PeriodicTableIFrame.this.Sn.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1688 */         } else if (!PeriodicTableIFrame.this.vSn) {
/* 1689 */           PeriodicTableIFrame.this.vSn = true;
/* 1690 */           PeriodicTableIFrame.this.Sn.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1693 */     this.Sb.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1695 */         PeriodicTableIFrame.this.Sb.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1696 */     this.Sb.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1698 */         PeriodicTableIFrame.this.Sb.setBorder(null);
/*      */       }
/* 1700 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Sb");
/* 1701 */         if (PeriodicTableIFrame.this.vSb == true) {
/* 1702 */           PeriodicTableIFrame.this.vSb = false;
/* 1703 */           PeriodicTableIFrame.this.Sb.setBackground(PeriodicTableIFrame.this.g2Color);
/* 1704 */         } else if (!PeriodicTableIFrame.this.vSb) {
/* 1705 */           PeriodicTableIFrame.this.vSb = true;
/* 1706 */           PeriodicTableIFrame.this.Sb.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1709 */     this.Te.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1711 */         PeriodicTableIFrame.this.Te.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1712 */     this.Te.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1714 */         PeriodicTableIFrame.this.Te.setBorder(null);
/*      */       }
/* 1716 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Te");
/* 1717 */         if (PeriodicTableIFrame.this.vTe == true) {
/* 1718 */           PeriodicTableIFrame.this.vTe = false;
/* 1719 */           PeriodicTableIFrame.this.Te.setBackground(PeriodicTableIFrame.this.g2Color);
/* 1720 */         } else if (!PeriodicTableIFrame.this.vTe) {
/* 1721 */           PeriodicTableIFrame.this.vTe = true;
/* 1722 */           PeriodicTableIFrame.this.Te.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1725 */     this.I.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1727 */         PeriodicTableIFrame.this.I.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1728 */     this.I.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1730 */         PeriodicTableIFrame.this.I.setBorder(null);
/*      */       }
/* 1732 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("I");
/* 1733 */         if (PeriodicTableIFrame.this.vI == true) {
/* 1734 */           PeriodicTableIFrame.this.vI = false;
/* 1735 */           PeriodicTableIFrame.this.I.setBackground(PeriodicTableIFrame.this.g1Color);
/* 1736 */         } else if (!PeriodicTableIFrame.this.vI) {
/* 1737 */           PeriodicTableIFrame.this.vI = true;
/* 1738 */           PeriodicTableIFrame.this.I.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1741 */     this.Xe.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1743 */         PeriodicTableIFrame.this.Xe.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1744 */     this.Xe.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1746 */         PeriodicTableIFrame.this.Xe.setBorder(null);
/*      */       }
/* 1748 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Xe");
/* 1749 */         if (PeriodicTableIFrame.this.vXe == true) {
/* 1750 */           PeriodicTableIFrame.this.vXe = false;
/* 1751 */           PeriodicTableIFrame.this.Xe.setBackground(PeriodicTableIFrame.this.g1Color);
/* 1752 */         } else if (!PeriodicTableIFrame.this.vXe) {
/* 1753 */           PeriodicTableIFrame.this.vXe = true;
/* 1754 */           PeriodicTableIFrame.this.Xe.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1757 */     this.Cs.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1759 */         PeriodicTableIFrame.this.Cs.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1760 */     this.Cs.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1762 */         PeriodicTableIFrame.this.Cs.setBorder(null);
/*      */       }
/* 1764 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Cs");
/* 1765 */         if (PeriodicTableIFrame.this.vCs == true) {
/* 1766 */           PeriodicTableIFrame.this.vCs = false;
/* 1767 */           PeriodicTableIFrame.this.Cs.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1768 */         } else if (!PeriodicTableIFrame.this.vCs) {
/* 1769 */           PeriodicTableIFrame.this.vCs = true;
/* 1770 */           PeriodicTableIFrame.this.Cs.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1773 */     this.Ba.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1775 */         PeriodicTableIFrame.this.Ba.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1776 */     this.Ba.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1778 */         PeriodicTableIFrame.this.Ba.setBorder(null);
/*      */       }
/* 1780 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Ba");
/* 1781 */         if (PeriodicTableIFrame.this.vBa == true) {
/* 1782 */           PeriodicTableIFrame.this.vBa = false;
/* 1783 */           PeriodicTableIFrame.this.Ba.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1784 */         } else if (!PeriodicTableIFrame.this.vBa) {
/* 1785 */           PeriodicTableIFrame.this.vBa = true;
/* 1786 */           PeriodicTableIFrame.this.Ba.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1789 */     this.Hf.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1791 */         PeriodicTableIFrame.this.Hf.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1792 */     this.Hf.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1794 */         PeriodicTableIFrame.this.Hf.setBorder(null);
/*      */       }
/* 1796 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Hf");
/* 1797 */         if (PeriodicTableIFrame.this.vHf == true) {
/* 1798 */           PeriodicTableIFrame.this.vHf = false;
/* 1799 */           PeriodicTableIFrame.this.Hf.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1800 */         } else if (!PeriodicTableIFrame.this.vHf) {
/* 1801 */           PeriodicTableIFrame.this.vHf = true;
/* 1802 */           PeriodicTableIFrame.this.Hf.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1805 */     this.Ta.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1807 */         PeriodicTableIFrame.this.Ta.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1808 */     this.Ta.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1810 */         PeriodicTableIFrame.this.Ta.setBorder(null);
/*      */       }
/* 1812 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Ta");
/* 1813 */         if (PeriodicTableIFrame.this.vTa == true) {
/* 1814 */           PeriodicTableIFrame.this.vTa = false;
/* 1815 */           PeriodicTableIFrame.this.Ta.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1816 */         } else if (!PeriodicTableIFrame.this.vTa) {
/* 1817 */           PeriodicTableIFrame.this.vTa = true;
/* 1818 */           PeriodicTableIFrame.this.Ta.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1821 */     this.W.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1823 */         PeriodicTableIFrame.this.W.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1824 */     this.W.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1826 */         PeriodicTableIFrame.this.W.setBorder(null);
/*      */       }
/* 1828 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("W");
/* 1829 */         if (PeriodicTableIFrame.this.vW == true) {
/* 1830 */           PeriodicTableIFrame.this.vW = false;
/* 1831 */           PeriodicTableIFrame.this.W.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1832 */         } else if (!PeriodicTableIFrame.this.vW) {
/* 1833 */           PeriodicTableIFrame.this.vW = true;
/* 1834 */           PeriodicTableIFrame.this.W.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1837 */     this.Re.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1839 */         PeriodicTableIFrame.this.Re.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1840 */     this.Re.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1842 */         PeriodicTableIFrame.this.Re.setBorder(null);
/*      */       }
/* 1844 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Re");
/* 1845 */         if (PeriodicTableIFrame.this.vRe == true) {
/* 1846 */           PeriodicTableIFrame.this.vRe = false;
/* 1847 */           PeriodicTableIFrame.this.Re.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1848 */         } else if (!PeriodicTableIFrame.this.vRe) {
/* 1849 */           PeriodicTableIFrame.this.vRe = true;
/* 1850 */           PeriodicTableIFrame.this.Re.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1853 */     this.Os.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1855 */         PeriodicTableIFrame.this.Os.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1856 */     this.Os.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1858 */         PeriodicTableIFrame.this.Os.setBorder(null);
/*      */       }
/* 1860 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Os");
/* 1861 */         if (PeriodicTableIFrame.this.vOs == true) {
/* 1862 */           PeriodicTableIFrame.this.vOs = false;
/* 1863 */           PeriodicTableIFrame.this.Os.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1864 */         } else if (!PeriodicTableIFrame.this.vOs) {
/* 1865 */           PeriodicTableIFrame.this.vOs = true;
/* 1866 */           PeriodicTableIFrame.this.Os.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1869 */     this.Ir.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1871 */         PeriodicTableIFrame.this.Ir.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1872 */     this.Ir.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1874 */         PeriodicTableIFrame.this.Ir.setBorder(null);
/*      */       }
/* 1876 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Ir");
/* 1877 */         if (PeriodicTableIFrame.this.vIr == true) {
/* 1878 */           PeriodicTableIFrame.this.vIr = false;
/* 1879 */           PeriodicTableIFrame.this.Ir.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1880 */         } else if (!PeriodicTableIFrame.this.vIr) {
/* 1881 */           PeriodicTableIFrame.this.vIr = true;
/* 1882 */           PeriodicTableIFrame.this.Ir.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1885 */     this.Pt.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1887 */         PeriodicTableIFrame.this.Pt.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1888 */     this.Pt.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1890 */         PeriodicTableIFrame.this.Pt.setBorder(null);
/*      */       }
/* 1892 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Pt");
/* 1893 */         if (PeriodicTableIFrame.this.vPt == true) {
/* 1894 */           PeriodicTableIFrame.this.vPt = false;
/* 1895 */           PeriodicTableIFrame.this.Pt.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1896 */         } else if (!PeriodicTableIFrame.this.vPt) {
/* 1897 */           PeriodicTableIFrame.this.vPt = true;
/* 1898 */           PeriodicTableIFrame.this.Pt.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1901 */     this.Au.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1903 */         PeriodicTableIFrame.this.Au.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1904 */     this.Au.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1906 */         PeriodicTableIFrame.this.Au.setBorder(null);
/*      */       }
/* 1908 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Au");
/* 1909 */         if (PeriodicTableIFrame.this.vAu == true) {
/* 1910 */           PeriodicTableIFrame.this.vAu = false;
/* 1911 */           PeriodicTableIFrame.this.Au.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1912 */         } else if (!PeriodicTableIFrame.this.vAu) {
/* 1913 */           PeriodicTableIFrame.this.vAu = true;
/* 1914 */           PeriodicTableIFrame.this.Au.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1917 */     this.Hg.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1919 */         PeriodicTableIFrame.this.Hg.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1920 */     this.Hg.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1922 */         PeriodicTableIFrame.this.Hg.setBorder(null);
/*      */       }
/* 1924 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Hg");
/* 1925 */         if (PeriodicTableIFrame.this.vHg == true) {
/* 1926 */           PeriodicTableIFrame.this.vHg = false;
/* 1927 */           PeriodicTableIFrame.this.Hg.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1928 */         } else if (!PeriodicTableIFrame.this.vHg) {
/* 1929 */           PeriodicTableIFrame.this.vHg = true;
/* 1930 */           PeriodicTableIFrame.this.Hg.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1933 */     this.Tl.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1935 */         PeriodicTableIFrame.this.Tl.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1936 */     this.Tl.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1938 */         PeriodicTableIFrame.this.Tl.setBorder(null);
/*      */       }
/* 1940 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Tl");
/* 1941 */         if (PeriodicTableIFrame.this.vTl == true) {
/* 1942 */           PeriodicTableIFrame.this.vTl = false;
/* 1943 */           PeriodicTableIFrame.this.Tl.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1944 */         } else if (!PeriodicTableIFrame.this.vTl) {
/* 1945 */           PeriodicTableIFrame.this.vTl = true;
/* 1946 */           PeriodicTableIFrame.this.Tl.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1949 */     this.Pb.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1951 */         PeriodicTableIFrame.this.Pb.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1952 */     this.Pb.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1954 */         PeriodicTableIFrame.this.Pb.setBorder(null);
/*      */       }
/* 1956 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Pb");
/* 1957 */         if (PeriodicTableIFrame.this.vPb == true) {
/* 1958 */           PeriodicTableIFrame.this.vPb = false;
/* 1959 */           PeriodicTableIFrame.this.Pb.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1960 */         } else if (!PeriodicTableIFrame.this.vPb) {
/* 1961 */           PeriodicTableIFrame.this.vPb = true;
/* 1962 */           PeriodicTableIFrame.this.Pb.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1965 */     this.Bi.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1967 */         PeriodicTableIFrame.this.Bi.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1968 */     this.Bi.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1970 */         PeriodicTableIFrame.this.Bi.setBorder(null);
/*      */       }
/* 1972 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Bi");
/* 1973 */         if (PeriodicTableIFrame.this.vBi == true) {
/* 1974 */           PeriodicTableIFrame.this.vBi = false;
/* 1975 */           PeriodicTableIFrame.this.Bi.setBackground(PeriodicTableIFrame.this.g0Color);
/* 1976 */         } else if (!PeriodicTableIFrame.this.vBi) {
/* 1977 */           PeriodicTableIFrame.this.vBi = true;
/* 1978 */           PeriodicTableIFrame.this.Bi.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1981 */     this.Po.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1983 */         PeriodicTableIFrame.this.Po.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 1984 */     this.Po.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 1986 */         PeriodicTableIFrame.this.Po.setBorder(null);
/*      */       }
/* 1988 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Po");
/* 1989 */         if (PeriodicTableIFrame.this.vPo == true) {
/* 1990 */           PeriodicTableIFrame.this.vPo = false;
/* 1991 */           PeriodicTableIFrame.this.Po.setBackground(PeriodicTableIFrame.this.g2Color);
/* 1992 */         } else if (!PeriodicTableIFrame.this.vPo) {
/* 1993 */           PeriodicTableIFrame.this.vPo = true;
/* 1994 */           PeriodicTableIFrame.this.Po.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 1997 */     this.At.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 1999 */         PeriodicTableIFrame.this.At.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2000 */     this.At.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2002 */         PeriodicTableIFrame.this.At.setBorder(null);
/*      */       }
/* 2004 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("At");
/* 2005 */         if (PeriodicTableIFrame.this.vAt == true) {
/* 2006 */           PeriodicTableIFrame.this.vAt = false;
/* 2007 */           PeriodicTableIFrame.this.At.setBackground(PeriodicTableIFrame.this.g1Color);
/* 2008 */         } else if (!PeriodicTableIFrame.this.vAt) {
/* 2009 */           PeriodicTableIFrame.this.vAt = true;
/* 2010 */           PeriodicTableIFrame.this.At.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2013 */     this.Rn.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2015 */         PeriodicTableIFrame.this.Rn.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2016 */     this.Rn.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2018 */         PeriodicTableIFrame.this.Rn.setBorder(null);
/*      */       }
/* 2020 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Rn");
/* 2021 */         if (PeriodicTableIFrame.this.vRn == true) {
/* 2022 */           PeriodicTableIFrame.this.vRn = false;
/* 2023 */           PeriodicTableIFrame.this.Rn.setBackground(PeriodicTableIFrame.this.g1Color);
/* 2024 */         } else if (!PeriodicTableIFrame.this.vRn) {
/* 2025 */           PeriodicTableIFrame.this.vRn = true;
/* 2026 */           PeriodicTableIFrame.this.Rn.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2029 */     this.Fr.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2031 */         PeriodicTableIFrame.this.Fr.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2032 */     this.Fr.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2034 */         PeriodicTableIFrame.this.Fr.setBorder(null);
/*      */       }
/* 2036 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Fr");
/* 2037 */         if (PeriodicTableIFrame.this.vFr == true) {
/* 2038 */           PeriodicTableIFrame.this.vFr = false;
/* 2039 */           PeriodicTableIFrame.this.Fr.setBackground(PeriodicTableIFrame.this.g0Color);
/* 2040 */         } else if (!PeriodicTableIFrame.this.vFr) {
/* 2041 */           PeriodicTableIFrame.this.vFr = true;
/* 2042 */           PeriodicTableIFrame.this.Fr.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2045 */     this.Ra.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2047 */         PeriodicTableIFrame.this.Ra.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2048 */     this.Ra.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2050 */         PeriodicTableIFrame.this.Ra.setBorder(null);
/*      */       }
/* 2052 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Ra");
/* 2053 */         if (PeriodicTableIFrame.this.vRa == true) {
/* 2054 */           PeriodicTableIFrame.this.vRa = false;
/* 2055 */           PeriodicTableIFrame.this.Ra.setBackground(PeriodicTableIFrame.this.g0Color);
/* 2056 */         } else if (!PeriodicTableIFrame.this.vRa) {
/* 2057 */           PeriodicTableIFrame.this.vRa = true;
/* 2058 */           PeriodicTableIFrame.this.Ra.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2061 */     this.Rf.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2063 */         PeriodicTableIFrame.this.Rf.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2064 */     this.Rf.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2066 */         PeriodicTableIFrame.this.Rf.setBorder(null);
/*      */       }
/* 2068 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Rf");
/* 2069 */         if (PeriodicTableIFrame.this.vRf == true) {
/* 2070 */           PeriodicTableIFrame.this.vRf = false;
/* 2071 */           PeriodicTableIFrame.this.Rf.setBackground(PeriodicTableIFrame.this.g0Color);
/* 2072 */         } else if (!PeriodicTableIFrame.this.vRf) {
/* 2073 */           PeriodicTableIFrame.this.vRf = true;
/* 2074 */           PeriodicTableIFrame.this.Rf.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2077 */     this.Db.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2079 */         PeriodicTableIFrame.this.Db.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2080 */     this.Db.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2082 */         PeriodicTableIFrame.this.Db.setBorder(null);
/*      */       }
/* 2084 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Db");
/* 2085 */         if (PeriodicTableIFrame.this.vDb == true) {
/* 2086 */           PeriodicTableIFrame.this.vDb = false;
/* 2087 */           PeriodicTableIFrame.this.Db.setBackground(PeriodicTableIFrame.this.g0Color);
/* 2088 */         } else if (!PeriodicTableIFrame.this.vDb) {
/* 2089 */           PeriodicTableIFrame.this.vDb = true;
/* 2090 */           PeriodicTableIFrame.this.Db.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2093 */     this.Sg.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2095 */         PeriodicTableIFrame.this.Sg.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2096 */     this.Sg.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2098 */         PeriodicTableIFrame.this.Sg.setBorder(null);
/*      */       }
/* 2100 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Sg");
/* 2101 */         if (PeriodicTableIFrame.this.vSg == true) {
/* 2102 */           PeriodicTableIFrame.this.vSg = false;
/* 2103 */           PeriodicTableIFrame.this.Sg.setBackground(PeriodicTableIFrame.this.g0Color);
/* 2104 */         } else if (!PeriodicTableIFrame.this.vSg) {
/* 2105 */           PeriodicTableIFrame.this.vSg = true;
/* 2106 */           PeriodicTableIFrame.this.Sg.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2109 */     this.Bh.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2111 */         PeriodicTableIFrame.this.Bh.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2112 */     this.Bh.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2114 */         PeriodicTableIFrame.this.Bh.setBorder(null);
/*      */       }
/* 2116 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Bh");
/* 2117 */         if (PeriodicTableIFrame.this.vBh == true) {
/* 2118 */           PeriodicTableIFrame.this.vBh = false;
/* 2119 */           PeriodicTableIFrame.this.Bh.setBackground(PeriodicTableIFrame.this.g0Color);
/* 2120 */         } else if (!PeriodicTableIFrame.this.vBh) {
/* 2121 */           PeriodicTableIFrame.this.vBh = true;
/* 2122 */           PeriodicTableIFrame.this.Bh.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2125 */     this.Hs.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2127 */         PeriodicTableIFrame.this.Hs.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2128 */     this.Hs.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2130 */         PeriodicTableIFrame.this.Hs.setBorder(null);
/*      */       }
/* 2132 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Hs");
/* 2133 */         if (PeriodicTableIFrame.this.vHs == true) {
/* 2134 */           PeriodicTableIFrame.this.vHs = false;
/* 2135 */           PeriodicTableIFrame.this.Hs.setBackground(PeriodicTableIFrame.this.g0Color);
/* 2136 */         } else if (!PeriodicTableIFrame.this.vHs) {
/* 2137 */           PeriodicTableIFrame.this.vHs = true;
/* 2138 */           PeriodicTableIFrame.this.Hs.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2141 */     this.Mt.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2143 */         PeriodicTableIFrame.this.Mt.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2144 */     this.Mt.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2146 */         PeriodicTableIFrame.this.Mt.setBorder(null);
/*      */       }
/* 2148 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Mt");
/* 2149 */         if (PeriodicTableIFrame.this.vMt == true) {
/* 2150 */           PeriodicTableIFrame.this.vMt = false;
/* 2151 */           PeriodicTableIFrame.this.Mt.setBackground(PeriodicTableIFrame.this.g0Color);
/* 2152 */         } else if (!PeriodicTableIFrame.this.vMt) {
/* 2153 */           PeriodicTableIFrame.this.vMt = true;
/* 2154 */           PeriodicTableIFrame.this.Mt.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2157 */     this.Ds.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2159 */         PeriodicTableIFrame.this.Ds.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2160 */     this.Ds.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2162 */         PeriodicTableIFrame.this.Ds.setBorder(null);
/*      */       }
/* 2164 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Ds");
/* 2165 */         if (PeriodicTableIFrame.this.vDs == true) {
/* 2166 */           PeriodicTableIFrame.this.vDs = false;
/* 2167 */           PeriodicTableIFrame.this.Ds.setBackground(PeriodicTableIFrame.this.g0Color);
/* 2168 */         } else if (!PeriodicTableIFrame.this.vDs) {
/* 2169 */           PeriodicTableIFrame.this.vDs = true;
/* 2170 */           PeriodicTableIFrame.this.Ds.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2173 */     this.Rg.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2175 */         PeriodicTableIFrame.this.Rg.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2176 */     this.Rg.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2178 */         PeriodicTableIFrame.this.Rg.setBorder(null);
/*      */       }
/* 2180 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Rg");
/* 2181 */         if (PeriodicTableIFrame.this.vRg == true) {
/* 2182 */           PeriodicTableIFrame.this.vRg = false;
/* 2183 */           PeriodicTableIFrame.this.Rg.setBackground(PeriodicTableIFrame.this.g0Color);
/* 2184 */         } else if (!PeriodicTableIFrame.this.vRg) {
/* 2185 */           PeriodicTableIFrame.this.vRg = true;
/* 2186 */           PeriodicTableIFrame.this.Rg.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2189 */     this.Cn.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2191 */         PeriodicTableIFrame.this.Cn.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2192 */     this.Cn.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2194 */         PeriodicTableIFrame.this.Cn.setBorder(null);
/*      */       }
/* 2196 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Cn");
/* 2197 */         if (PeriodicTableIFrame.this.vCn == true) {
/* 2198 */           PeriodicTableIFrame.this.vCn = false;
/* 2199 */           PeriodicTableIFrame.this.Cn.setBackground(PeriodicTableIFrame.this.g0Color);
/* 2200 */         } else if (!PeriodicTableIFrame.this.vCn) {
/* 2201 */           PeriodicTableIFrame.this.vCn = true;
/* 2202 */           PeriodicTableIFrame.this.Cn.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2205 */     this.Uut.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2207 */         PeriodicTableIFrame.this.Uut.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2208 */     this.Uut.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2210 */         PeriodicTableIFrame.this.Uut.setBorder(null);
/*      */       }
/* 2212 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Uut");
/* 2213 */         if (PeriodicTableIFrame.this.vUut == true) {
/* 2214 */           PeriodicTableIFrame.this.vUut = false;
/* 2215 */           PeriodicTableIFrame.this.Uut.setBackground(PeriodicTableIFrame.this.g0Color);
/* 2216 */         } else if (!PeriodicTableIFrame.this.vUut) {
/* 2217 */           PeriodicTableIFrame.this.vUut = true;
/* 2218 */           PeriodicTableIFrame.this.Uut.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2221 */     this.Fl.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2223 */         PeriodicTableIFrame.this.Fl.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2224 */     this.Fl.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2226 */         PeriodicTableIFrame.this.Fl.setBorder(null);
/*      */       }
/* 2228 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Fl");
/* 2229 */         if (PeriodicTableIFrame.this.vFl == true) {
/* 2230 */           PeriodicTableIFrame.this.vFl = false;
/* 2231 */           PeriodicTableIFrame.this.Fl.setBackground(PeriodicTableIFrame.this.g0Color);
/* 2232 */         } else if (!PeriodicTableIFrame.this.vFl) {
/* 2233 */           PeriodicTableIFrame.this.vFl = true;
/* 2234 */           PeriodicTableIFrame.this.Fl.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2237 */     this.Uup.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2239 */         PeriodicTableIFrame.this.Uup.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2240 */     this.Uup.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2242 */         PeriodicTableIFrame.this.Uup.setBorder(null);
/*      */       }
/* 2244 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Uup");
/* 2245 */         if (PeriodicTableIFrame.this.vUup == true) {
/* 2246 */           PeriodicTableIFrame.this.vUup = false;
/* 2247 */           PeriodicTableIFrame.this.Uup.setBackground(PeriodicTableIFrame.this.g0Color);
/* 2248 */         } else if (!PeriodicTableIFrame.this.vUup) {
/* 2249 */           PeriodicTableIFrame.this.vUup = true;
/* 2250 */           PeriodicTableIFrame.this.Uup.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2253 */     this.Lv.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2255 */         PeriodicTableIFrame.this.Lv.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2256 */     this.Lv.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2258 */         PeriodicTableIFrame.this.Lv.setBorder(null);
/*      */       }
/* 2260 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Lv");
/* 2261 */         if (PeriodicTableIFrame.this.vLv == true) {
/* 2262 */           PeriodicTableIFrame.this.vLv = false;
/* 2263 */           PeriodicTableIFrame.this.Lv.setBackground(PeriodicTableIFrame.this.g0Color);
/* 2264 */         } else if (!PeriodicTableIFrame.this.vLv) {
/* 2265 */           PeriodicTableIFrame.this.vLv = true;
/* 2266 */           PeriodicTableIFrame.this.Lv.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2269 */     this.Uus.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2271 */         PeriodicTableIFrame.this.Uus.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2272 */     this.Uus.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2274 */         PeriodicTableIFrame.this.Uus.setBorder(null);
/*      */       }
/* 2276 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Uus");
/* 2277 */         if (PeriodicTableIFrame.this.vUus == true) {
/* 2278 */           PeriodicTableIFrame.this.vUus = false;
/* 2279 */           PeriodicTableIFrame.this.Uus.setBackground(PeriodicTableIFrame.this.g0Color);
/* 2280 */         } else if (!PeriodicTableIFrame.this.vUus) {
/* 2281 */           PeriodicTableIFrame.this.vUus = true;
/* 2282 */           PeriodicTableIFrame.this.Uus.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2285 */     this.Uuo.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2287 */         PeriodicTableIFrame.this.Uuo.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2288 */     this.Uuo.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2290 */         PeriodicTableIFrame.this.Uuo.setBorder(null);
/*      */       }
/* 2292 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Uuo");
/* 2293 */         if (PeriodicTableIFrame.this.vUuo == true) {
/* 2294 */           PeriodicTableIFrame.this.vUuo = false;
/* 2295 */           PeriodicTableIFrame.this.Uuo.setBackground(PeriodicTableIFrame.this.g0Color);
/* 2296 */         } else if (!PeriodicTableIFrame.this.vUuo) {
/* 2297 */           PeriodicTableIFrame.this.vUuo = true;
/* 2298 */           PeriodicTableIFrame.this.Uuo.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2301 */     this.La.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2303 */         PeriodicTableIFrame.this.La.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2304 */     this.La.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2306 */         PeriodicTableIFrame.this.La.setBorder(null);
/*      */       }
/* 2308 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("La");
/* 2309 */         if (PeriodicTableIFrame.this.vLa == true) {
/* 2310 */           PeriodicTableIFrame.this.vLa = false;
/* 2311 */           PeriodicTableIFrame.this.La.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2312 */         } else if (!PeriodicTableIFrame.this.vLa) {
/* 2313 */           PeriodicTableIFrame.this.vLa = true;
/* 2314 */           PeriodicTableIFrame.this.La.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2317 */     this.Ce.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2319 */         PeriodicTableIFrame.this.Ce.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2320 */     this.Ce.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2322 */         PeriodicTableIFrame.this.Ce.setBorder(null);
/*      */       }
/* 2324 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Ce");
/* 2325 */         if (PeriodicTableIFrame.this.vCe == true) {
/* 2326 */           PeriodicTableIFrame.this.vCe = false;
/* 2327 */           PeriodicTableIFrame.this.Ce.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2328 */         } else if (!PeriodicTableIFrame.this.vCe) {
/* 2329 */           PeriodicTableIFrame.this.vCe = true;
/* 2330 */           PeriodicTableIFrame.this.Ce.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2333 */     this.Pr.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2335 */         PeriodicTableIFrame.this.Pr.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2336 */     this.Pr.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2338 */         PeriodicTableIFrame.this.Pr.setBorder(null);
/*      */       }
/* 2340 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Pr");
/* 2341 */         if (PeriodicTableIFrame.this.vPr == true) {
/* 2342 */           PeriodicTableIFrame.this.vPr = false;
/* 2343 */           PeriodicTableIFrame.this.Pr.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2344 */         } else if (!PeriodicTableIFrame.this.vPr) {
/* 2345 */           PeriodicTableIFrame.this.vPr = true;
/* 2346 */           PeriodicTableIFrame.this.Pr.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2349 */     this.Nd.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2351 */         PeriodicTableIFrame.this.Nd.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2352 */     this.Nd.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2354 */         PeriodicTableIFrame.this.Nd.setBorder(null);
/*      */       }
/* 2356 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Nd");
/* 2357 */         if (PeriodicTableIFrame.this.vNd == true) {
/* 2358 */           PeriodicTableIFrame.this.vNd = false;
/* 2359 */           PeriodicTableIFrame.this.Nd.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2360 */         } else if (!PeriodicTableIFrame.this.vNd) {
/* 2361 */           PeriodicTableIFrame.this.vNd = true;
/* 2362 */           PeriodicTableIFrame.this.Nd.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2365 */     this.Pm.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2367 */         PeriodicTableIFrame.this.Pm.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2368 */     this.Pm.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2370 */         PeriodicTableIFrame.this.Pm.setBorder(null);
/*      */       }
/* 2372 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Pm");
/* 2373 */         if (PeriodicTableIFrame.this.vPm == true) {
/* 2374 */           PeriodicTableIFrame.this.vPm = false;
/* 2375 */           PeriodicTableIFrame.this.Pm.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2376 */         } else if (!PeriodicTableIFrame.this.vPm) {
/* 2377 */           PeriodicTableIFrame.this.vPm = true;
/* 2378 */           PeriodicTableIFrame.this.Pm.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2381 */     this.Sm.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2383 */         PeriodicTableIFrame.this.Sm.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2384 */     this.Sm.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2386 */         PeriodicTableIFrame.this.Sm.setBorder(null);
/*      */       }
/* 2388 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Sm");
/* 2389 */         if (PeriodicTableIFrame.this.vSm == true) {
/* 2390 */           PeriodicTableIFrame.this.vSm = false;
/* 2391 */           PeriodicTableIFrame.this.Sm.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2392 */         } else if (!PeriodicTableIFrame.this.vSm) {
/* 2393 */           PeriodicTableIFrame.this.vSm = true;
/* 2394 */           PeriodicTableIFrame.this.Sm.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2397 */     this.Eu.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2399 */         PeriodicTableIFrame.this.Eu.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2400 */     this.Eu.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2402 */         PeriodicTableIFrame.this.Eu.setBorder(null);
/*      */       }
/* 2404 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Eu");
/* 2405 */         if (PeriodicTableIFrame.this.vEu == true) {
/* 2406 */           PeriodicTableIFrame.this.vEu = false;
/* 2407 */           PeriodicTableIFrame.this.Eu.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2408 */         } else if (!PeriodicTableIFrame.this.vEu) {
/* 2409 */           PeriodicTableIFrame.this.vEu = true;
/* 2410 */           PeriodicTableIFrame.this.Eu.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2413 */     this.Gd.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2415 */         PeriodicTableIFrame.this.Gd.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2416 */     this.Gd.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2418 */         PeriodicTableIFrame.this.Gd.setBorder(null);
/*      */       }
/* 2420 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Gd");
/* 2421 */         if (PeriodicTableIFrame.this.vGd == true) {
/* 2422 */           PeriodicTableIFrame.this.vGd = false;
/* 2423 */           PeriodicTableIFrame.this.Gd.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2424 */         } else if (!PeriodicTableIFrame.this.vGd) {
/* 2425 */           PeriodicTableIFrame.this.vGd = true;
/* 2426 */           PeriodicTableIFrame.this.Gd.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2429 */     this.Tb.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2431 */         PeriodicTableIFrame.this.Tb.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2432 */     this.Tb.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2434 */         PeriodicTableIFrame.this.Tb.setBorder(null);
/*      */       }
/* 2436 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Tb");
/* 2437 */         if (PeriodicTableIFrame.this.vTb == true) {
/* 2438 */           PeriodicTableIFrame.this.vTb = false;
/* 2439 */           PeriodicTableIFrame.this.Tb.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2440 */         } else if (!PeriodicTableIFrame.this.vTb) {
/* 2441 */           PeriodicTableIFrame.this.vTb = true;
/* 2442 */           PeriodicTableIFrame.this.Tb.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2445 */     this.Dy.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2447 */         PeriodicTableIFrame.this.Dy.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2448 */     this.Dy.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2450 */         PeriodicTableIFrame.this.Dy.setBorder(null);
/*      */       }
/* 2452 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Dy");
/* 2453 */         if (PeriodicTableIFrame.this.vDy == true) {
/* 2454 */           PeriodicTableIFrame.this.vDy = false;
/* 2455 */           PeriodicTableIFrame.this.Dy.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2456 */         } else if (!PeriodicTableIFrame.this.vDy) {
/* 2457 */           PeriodicTableIFrame.this.vDy = true;
/* 2458 */           PeriodicTableIFrame.this.Dy.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2461 */     this.Ho.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2463 */         PeriodicTableIFrame.this.Ho.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2464 */     this.Ho.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2466 */         PeriodicTableIFrame.this.Ho.setBorder(null);
/*      */       }
/* 2468 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Ho");
/* 2469 */         if (PeriodicTableIFrame.this.vHo == true) {
/* 2470 */           PeriodicTableIFrame.this.vHo = false;
/* 2471 */           PeriodicTableIFrame.this.Ho.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2472 */         } else if (!PeriodicTableIFrame.this.vHo) {
/* 2473 */           PeriodicTableIFrame.this.vHo = true;
/* 2474 */           PeriodicTableIFrame.this.Ho.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2477 */     this.Er.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2479 */         PeriodicTableIFrame.this.Er.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2480 */     this.Er.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2482 */         PeriodicTableIFrame.this.Er.setBorder(null);
/*      */       }
/* 2484 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Er");
/* 2485 */         if (PeriodicTableIFrame.this.vEr == true) {
/* 2486 */           PeriodicTableIFrame.this.vEr = false;
/* 2487 */           PeriodicTableIFrame.this.Er.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2488 */         } else if (!PeriodicTableIFrame.this.vEr) {
/* 2489 */           PeriodicTableIFrame.this.vEr = true;
/* 2490 */           PeriodicTableIFrame.this.Er.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2493 */     this.Tm.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2495 */         PeriodicTableIFrame.this.Tm.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2496 */     this.Tm.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2498 */         PeriodicTableIFrame.this.Tm.setBorder(null);
/*      */       }
/* 2500 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Tm");
/* 2501 */         if (PeriodicTableIFrame.this.vTm == true) {
/* 2502 */           PeriodicTableIFrame.this.vTm = false;
/* 2503 */           PeriodicTableIFrame.this.Tm.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2504 */         } else if (!PeriodicTableIFrame.this.vTm) {
/* 2505 */           PeriodicTableIFrame.this.vTm = true;
/* 2506 */           PeriodicTableIFrame.this.Tm.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2509 */     this.Yb.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2511 */         PeriodicTableIFrame.this.Yb.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2512 */     this.Yb.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2514 */         PeriodicTableIFrame.this.Yb.setBorder(null);
/*      */       }
/* 2516 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Yb");
/* 2517 */         if (PeriodicTableIFrame.this.vYb == true) {
/* 2518 */           PeriodicTableIFrame.this.vYb = false;
/* 2519 */           PeriodicTableIFrame.this.Yb.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2520 */         } else if (!PeriodicTableIFrame.this.vYb) {
/* 2521 */           PeriodicTableIFrame.this.vYb = true;
/* 2522 */           PeriodicTableIFrame.this.Yb.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2525 */     this.Lu.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2527 */         PeriodicTableIFrame.this.Lu.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2528 */     this.Lu.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2530 */         PeriodicTableIFrame.this.Lu.setBorder(null);
/*      */       }
/* 2532 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Lu");
/* 2533 */         if (PeriodicTableIFrame.this.vLu == true) {
/* 2534 */           PeriodicTableIFrame.this.vLu = false;
/* 2535 */           PeriodicTableIFrame.this.Lu.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2536 */         } else if (!PeriodicTableIFrame.this.vLu) {
/* 2537 */           PeriodicTableIFrame.this.vLu = true;
/* 2538 */           PeriodicTableIFrame.this.Lu.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2541 */     this.Ac.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2543 */         PeriodicTableIFrame.this.Ac.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2544 */     this.Ac.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2546 */         PeriodicTableIFrame.this.Ac.setBorder(null);
/*      */       }
/* 2548 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Ac");
/* 2549 */         if (PeriodicTableIFrame.this.vAc == true) {
/* 2550 */           PeriodicTableIFrame.this.vAc = false;
/* 2551 */           PeriodicTableIFrame.this.Ac.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2552 */         } else if (!PeriodicTableIFrame.this.vAc) {
/* 2553 */           PeriodicTableIFrame.this.vAc = true;
/* 2554 */           PeriodicTableIFrame.this.Ac.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2557 */     this.Th.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2559 */         PeriodicTableIFrame.this.Th.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2560 */     this.Th.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2562 */         PeriodicTableIFrame.this.Th.setBorder(null);
/*      */       }
/* 2564 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Th");
/* 2565 */         if (PeriodicTableIFrame.this.vTh == true) {
/* 2566 */           PeriodicTableIFrame.this.vTh = false;
/* 2567 */           PeriodicTableIFrame.this.Th.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2568 */         } else if (!PeriodicTableIFrame.this.vTh) {
/* 2569 */           PeriodicTableIFrame.this.vTh = true;
/* 2570 */           PeriodicTableIFrame.this.Th.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2573 */     this.Pa.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2575 */         PeriodicTableIFrame.this.Pa.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2576 */     this.Pa.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2578 */         PeriodicTableIFrame.this.Pa.setBorder(null);
/*      */       }
/* 2580 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Pa");
/* 2581 */         if (PeriodicTableIFrame.this.vPa == true) {
/* 2582 */           PeriodicTableIFrame.this.vPa = false;
/* 2583 */           PeriodicTableIFrame.this.Pa.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2584 */         } else if (!PeriodicTableIFrame.this.vPa) {
/* 2585 */           PeriodicTableIFrame.this.vPa = true;
/* 2586 */           PeriodicTableIFrame.this.Pa.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2589 */     this.U.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2591 */         PeriodicTableIFrame.this.U.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2592 */     this.U.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2594 */         PeriodicTableIFrame.this.U.setBorder(null);
/*      */       }
/* 2596 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("U");
/* 2597 */         if (PeriodicTableIFrame.this.vU == true) {
/* 2598 */           PeriodicTableIFrame.this.vU = false;
/* 2599 */           PeriodicTableIFrame.this.U.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2600 */         } else if (!PeriodicTableIFrame.this.vU) {
/* 2601 */           PeriodicTableIFrame.this.vU = true;
/* 2602 */           PeriodicTableIFrame.this.U.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2605 */     this.Np.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2607 */         PeriodicTableIFrame.this.Np.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2608 */     this.Np.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2610 */         PeriodicTableIFrame.this.Np.setBorder(null);
/*      */       }
/* 2612 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Np");
/* 2613 */         if (PeriodicTableIFrame.this.vNp == true) {
/* 2614 */           PeriodicTableIFrame.this.vNp = false;
/* 2615 */           PeriodicTableIFrame.this.Np.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2616 */         } else if (!PeriodicTableIFrame.this.vCo) {
/* 2617 */           PeriodicTableIFrame.this.vNp = true;
/* 2618 */           PeriodicTableIFrame.this.Np.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2621 */     this.Pu.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2623 */         PeriodicTableIFrame.this.Pu.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2624 */     this.Pu.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2626 */         PeriodicTableIFrame.this.Pu.setBorder(null);
/*      */       }
/* 2628 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Pu");
/* 2629 */         if (PeriodicTableIFrame.this.vPu == true) {
/* 2630 */           PeriodicTableIFrame.this.vPu = false;
/* 2631 */           PeriodicTableIFrame.this.Pu.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2632 */         } else if (!PeriodicTableIFrame.this.vPu) {
/* 2633 */           PeriodicTableIFrame.this.vPu = true;
/* 2634 */           PeriodicTableIFrame.this.Pu.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2637 */     this.Am.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2639 */         PeriodicTableIFrame.this.Am.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2640 */     this.Am.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2642 */         PeriodicTableIFrame.this.Am.setBorder(null);
/*      */       }
/* 2644 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Am");
/* 2645 */         if (PeriodicTableIFrame.this.vAm == true) {
/* 2646 */           PeriodicTableIFrame.this.vAm = false;
/* 2647 */           PeriodicTableIFrame.this.Am.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2648 */         } else if (!PeriodicTableIFrame.this.vAm) {
/* 2649 */           PeriodicTableIFrame.this.vAm = true;
/* 2650 */           PeriodicTableIFrame.this.Am.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2653 */     this.Cm.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2655 */         PeriodicTableIFrame.this.Cm.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2656 */     this.Cm.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2658 */         PeriodicTableIFrame.this.Cm.setBorder(null);
/*      */       }
/* 2660 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Cm");
/* 2661 */         if (PeriodicTableIFrame.this.vCm == true) {
/* 2662 */           PeriodicTableIFrame.this.vCm = false;
/* 2663 */           PeriodicTableIFrame.this.Cm.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2664 */         } else if (!PeriodicTableIFrame.this.vCm) {
/* 2665 */           PeriodicTableIFrame.this.vCm = true;
/* 2666 */           PeriodicTableIFrame.this.Cm.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2669 */     this.Bk.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2671 */         PeriodicTableIFrame.this.Bk.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2672 */     this.Bk.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2674 */         PeriodicTableIFrame.this.Bk.setBorder(null);
/*      */       }
/* 2676 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Bk");
/* 2677 */         if (PeriodicTableIFrame.this.vBk == true) {
/* 2678 */           PeriodicTableIFrame.this.vBk = false;
/* 2679 */           PeriodicTableIFrame.this.Bk.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2680 */         } else if (!PeriodicTableIFrame.this.vBk) {
/* 2681 */           PeriodicTableIFrame.this.vBk = true;
/* 2682 */           PeriodicTableIFrame.this.Bk.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2685 */     this.Cf.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2687 */         PeriodicTableIFrame.this.Cf.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2688 */     this.Cf.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2690 */         PeriodicTableIFrame.this.Cf.setBorder(null);
/*      */       }
/* 2692 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Cf");
/* 2693 */         if (PeriodicTableIFrame.this.vCf == true) {
/* 2694 */           PeriodicTableIFrame.this.vCf = false;
/* 2695 */           PeriodicTableIFrame.this.Cf.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2696 */         } else if (!PeriodicTableIFrame.this.vCf) {
/* 2697 */           PeriodicTableIFrame.this.vCf = true;
/* 2698 */           PeriodicTableIFrame.this.Cf.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2701 */     this.Es.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2703 */         PeriodicTableIFrame.this.Es.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2704 */     this.Es.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2706 */         PeriodicTableIFrame.this.Es.setBorder(null);
/*      */       }
/* 2708 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Es");
/* 2709 */         if (PeriodicTableIFrame.this.vEs == true) {
/* 2710 */           PeriodicTableIFrame.this.vEs = false;
/* 2711 */           PeriodicTableIFrame.this.Es.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2712 */         } else if (!PeriodicTableIFrame.this.vEs) {
/* 2713 */           PeriodicTableIFrame.this.vEs = true;
/* 2714 */           PeriodicTableIFrame.this.Es.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2717 */     this.Fm.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2719 */         PeriodicTableIFrame.this.Fm.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2720 */     this.Fm.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2722 */         PeriodicTableIFrame.this.Fm.setBorder(null);
/*      */       }
/* 2724 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Fm");
/* 2725 */         if (PeriodicTableIFrame.this.vFm == true) {
/* 2726 */           PeriodicTableIFrame.this.vFm = false;
/* 2727 */           PeriodicTableIFrame.this.Fm.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2728 */         } else if (!PeriodicTableIFrame.this.vFm) {
/* 2729 */           PeriodicTableIFrame.this.vFm = true;
/* 2730 */           PeriodicTableIFrame.this.Fm.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2733 */     this.Md.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2735 */         PeriodicTableIFrame.this.Md.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2736 */     this.Md.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2738 */         PeriodicTableIFrame.this.Md.setBorder(null);
/*      */       }
/* 2740 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Md");
/* 2741 */         if (PeriodicTableIFrame.this.vMd == true) {
/* 2742 */           PeriodicTableIFrame.this.vMd = false;
/* 2743 */           PeriodicTableIFrame.this.Md.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2744 */         } else if (!PeriodicTableIFrame.this.vMd) {
/* 2745 */           PeriodicTableIFrame.this.vMd = true;
/* 2746 */           PeriodicTableIFrame.this.Md.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2749 */     this.No.addMouseMotionListener(new MouseMotionAdapter()
/*      */     {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2752 */         PeriodicTableIFrame.this.No.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2753 */     this.No.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2755 */         PeriodicTableIFrame.this.No.setBorder(null);
/*      */       }
/* 2757 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("No");
/* 2758 */         if (PeriodicTableIFrame.this.vNo == true) {
/* 2759 */           PeriodicTableIFrame.this.vNo = false;
/* 2760 */           PeriodicTableIFrame.this.No.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2761 */         } else if (!PeriodicTableIFrame.this.vNo) {
/* 2762 */           PeriodicTableIFrame.this.vNo = true;
/* 2763 */           PeriodicTableIFrame.this.No.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2766 */     this.Lr.addMouseMotionListener(new MouseMotionAdapter() {
/*      */       public void mouseMoved(MouseEvent evt) {
/* 2768 */         PeriodicTableIFrame.this.Lr.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.red));
/*      */       }
/*      */     });
/* 2769 */     this.Lr.addMouseListener(new MouseAdapter() {
/*      */       public void mouseExited(MouseEvent evt) {
/* 2771 */         PeriodicTableIFrame.this.Lr.setBorder(null);
/*      */       }
/* 2773 */       public void mousePressed(MouseEvent evt) { PeriodicTableIFrame.this.openFileElement("Lr");
/* 2774 */         if (PeriodicTableIFrame.this.vLr == true) {
/* 2775 */           PeriodicTableIFrame.this.vLr = false;
/* 2776 */           PeriodicTableIFrame.this.Lr.setBackground(PeriodicTableIFrame.this.g3Color);
/* 2777 */         } else if (!PeriodicTableIFrame.this.vLr) {
/* 2778 */           PeriodicTableIFrame.this.vLr = true;
/* 2779 */           PeriodicTableIFrame.this.Lr.setBackground(PeriodicTableIFrame.this.g4Color);
/*      */         }
/*      */       }
/*      */     });
/* 2782 */     this.chkFileOpen.addActionListener(new ActionListener() {
/*      */       public void actionPerformed(ActionEvent evt) {
/* 2784 */         PeriodicTableIFrame.this.fileElement = PeriodicTableIFrame.this.chkFileOpen.isSelected();
/*      */       } } );
/*      */   }
/*      */ 
/*      */   private void openFileElement(String file) {
/* 2789 */     if (this.fileElement)
/*      */       try {
/* 2791 */         File path = new File(System.getProperty("user.dir") + "/src/Elements/" + file + ".htm");
/* 2792 */         Desktop.getDesktop().open(path);
/*      */       } catch (Exception e) {
/* 2794 */         JOptionPane.showMessageDialog(null, "Elemento no registrado");
/*      */       }
/*      */   }
/*      */ 
/*      */   private void selectElements() {
/* 2799 */     this.elemSelected.clear();
/* 2800 */     if (this.vH == true) this.elemSelected.add("H");
/* 2801 */     if (this.vHe == true) this.elemSelected.add("He");
/* 2802 */     if (this.vLi == true) this.elemSelected.add("Li");
/* 2803 */     if (this.vBe == true) this.elemSelected.add("Be");
/* 2804 */     if (this.vB == true) this.elemSelected.add("B");
/* 2805 */     if (this.vC == true) this.elemSelected.add("C");
/* 2806 */     if (this.vN == true) this.elemSelected.add("N");
/* 2807 */     if (this.vO == true) this.elemSelected.add("O");
/* 2808 */     if (this.vF == true) this.elemSelected.add("F");
/* 2809 */     if (this.vNe == true) this.elemSelected.add("Ne");
/* 2810 */     if (this.vNa == true) this.elemSelected.add("Na");
/* 2811 */     if (this.vMg == true) this.elemSelected.add("Mg");
/* 2812 */     if (this.vAl == true) this.elemSelected.add("Al");
/* 2813 */     if (this.vSi == true) this.elemSelected.add("Si");
/* 2814 */     if (this.vP == true) this.elemSelected.add("P");
/* 2815 */     if (this.vS == true) this.elemSelected.add("S");
/* 2816 */     if (this.vCl == true) this.elemSelected.add("Cl");
/* 2817 */     if (this.vAr == true) this.elemSelected.add("Ar");
/* 2818 */     if (this.vK == true) this.elemSelected.add("K");
/* 2819 */     if (this.vCa == true) this.elemSelected.add("Ca");
/* 2820 */     if (this.vSc == true) this.elemSelected.add("Sc");
/* 2821 */     if (this.vTi == true) this.elemSelected.add("Ti");
/* 2822 */     if (this.vV == true) this.elemSelected.add("V");
/* 2823 */     if (this.vCr == true) this.elemSelected.add("Cr");
/* 2824 */     if (this.vMn == true) this.elemSelected.add("Mn");
/* 2825 */     if (this.vFe == true) this.elemSelected.add("Fe");
/* 2826 */     if (this.vCo == true) this.elemSelected.add("Co");
/* 2827 */     if (this.vNi == true) this.elemSelected.add("Ni");
/* 2828 */     if (this.vCu == true) this.elemSelected.add("Cu");
/* 2829 */     if (this.vZn == true) this.elemSelected.add("Zn");
/* 2830 */     if (this.vGa == true) this.elemSelected.add("Ga");
/* 2831 */     if (this.vGe == true) this.elemSelected.add("Ge");
/* 2832 */     if (this.vAs == true) this.elemSelected.add("As");
/* 2833 */     if (this.vSe == true) this.elemSelected.add("Se");
/* 2834 */     if (this.vBr == true) this.elemSelected.add("Br");
/* 2835 */     if (this.vKr == true) this.elemSelected.add("Kr");
/* 2836 */     if (this.vRb == true) this.elemSelected.add("Rb");
/* 2837 */     if (this.vSr == true) this.elemSelected.add("Sr");
/* 2838 */     if (this.vY == true) this.elemSelected.add("Y");
/* 2839 */     if (this.vZr == true) this.elemSelected.add("Zr");
/* 2840 */     if (this.vNb == true) this.elemSelected.add("Nb");
/* 2841 */     if (this.vMo == true) this.elemSelected.add("Mo");
/* 2842 */     if (this.vTc == true) this.elemSelected.add("Tc");
/* 2843 */     if (this.vRu == true) this.elemSelected.add("Ru");
/* 2844 */     if (this.vRh == true) this.elemSelected.add("Rh");
/* 2845 */     if (this.vPd == true) this.elemSelected.add("Pd");
/* 2846 */     if (this.vAg == true) this.elemSelected.add("Ag");
/* 2847 */     if (this.vCd == true) this.elemSelected.add("Cd");
/* 2848 */     if (this.vIn == true) this.elemSelected.add("In");
/* 2849 */     if (this.vSn == true) this.elemSelected.add("Sn");
/* 2850 */     if (this.vSb == true) this.elemSelected.add("Sb");
/* 2851 */     if (this.vTe == true) this.elemSelected.add("Te");
/* 2852 */     if (this.vI == true) this.elemSelected.add("I");
/* 2853 */     if (this.vXe == true) this.elemSelected.add("Xe");
/* 2854 */     if (this.vCs == true) this.elemSelected.add("Cs");
/* 2855 */     if (this.vBa == true) this.elemSelected.add("Ba");
/* 2856 */     if (this.vHf == true) this.elemSelected.add("Hf");
/* 2857 */     if (this.vTa == true) this.elemSelected.add("Ta");
/* 2858 */     if (this.vW == true) this.elemSelected.add("W");
/* 2859 */     if (this.vRe == true) this.elemSelected.add("Re");
/* 2860 */     if (this.vOs == true) this.elemSelected.add("Os");
/* 2861 */     if (this.vIr == true) this.elemSelected.add("Ir");
/* 2862 */     if (this.vPt == true) this.elemSelected.add("Pt");
/* 2863 */     if (this.vAu == true) this.elemSelected.add("Au");
/* 2864 */     if (this.vHg == true) this.elemSelected.add("Hg");
/* 2865 */     if (this.vTl == true) this.elemSelected.add("Tl");
/* 2866 */     if (this.vPb == true) this.elemSelected.add("Pb");
/* 2867 */     if (this.vBi == true) this.elemSelected.add("Bi");
/* 2868 */     if (this.vPo == true) this.elemSelected.add("Po");
/* 2869 */     if (this.vAt == true) this.elemSelected.add("At");
/* 2870 */     if (this.vRn == true) this.elemSelected.add("Rn");
/* 2871 */     if (this.vFr == true) this.elemSelected.add("Fr");
/* 2872 */     if (this.vRa == true) this.elemSelected.add("Ra");
/* 2873 */     if (this.vRf == true) this.elemSelected.add("Rf");
/* 2874 */     if (this.vDb == true) this.elemSelected.add("Db");
/* 2875 */     if (this.vSg == true) this.elemSelected.add("Sg");
/* 2876 */     if (this.vBh == true) this.elemSelected.add("Bh");
/* 2877 */     if (this.vHs == true) this.elemSelected.add("Hs");
/* 2878 */     if (this.vMt == true) this.elemSelected.add("Mt");
/* 2879 */     if (this.vDs == true) this.elemSelected.add("Ds");
/* 2880 */     if (this.vRg == true) this.elemSelected.add("Rg");
/* 2881 */     if (this.vCn == true) this.elemSelected.add("Cn");
/* 2882 */     if (this.vFl == true) this.elemSelected.add("Fl");
/* 2883 */     if (this.vUup == true) this.elemSelected.add("Uup");
/* 2884 */     if (this.vLv == true) this.elemSelected.add("Lv");
/* 2885 */     if (this.vUus == true) this.elemSelected.add("Uus");
/* 2886 */     if (this.vUut == true) this.elemSelected.add("Uut");
/* 2887 */     if (this.vUuo == true) this.elemSelected.add("Uuo");
/* 2888 */     if (this.vLa == true) this.elemSelected.add("La");
/* 2889 */     if (this.vCe == true) this.elemSelected.add("Ce");
/* 2890 */     if (this.vPr == true) this.elemSelected.add("Pr");
/* 2891 */     if (this.vNd == true) this.elemSelected.add("Nd");
/* 2892 */     if (this.vPm == true) this.elemSelected.add("Pm");
/* 2893 */     if (this.vSm == true) this.elemSelected.add("Sm");
/* 2894 */     if (this.vEu == true) this.elemSelected.add("Eu");
/* 2895 */     if (this.vGd == true) this.elemSelected.add("Gd");
/* 2896 */     if (this.vTb == true) this.elemSelected.add("Tb");
/* 2897 */     if (this.vDy == true) this.elemSelected.add("Dy");
/* 2898 */     if (this.vHo == true) this.elemSelected.add("Ho");
/* 2899 */     if (this.vEr == true) this.elemSelected.add("Er");
/* 2900 */     if (this.vTm == true) this.elemSelected.add("Tm");
/* 2901 */     if (this.vYb == true) this.elemSelected.add("Yb");
/* 2902 */     if (this.vLu == true) this.elemSelected.add("Lu");
/* 2903 */     if (this.vAc == true) this.elemSelected.add("Ac");
/* 2904 */     if (this.vTh == true) this.elemSelected.add("Th");
/* 2905 */     if (this.vPa == true) this.elemSelected.add("Pa");
/* 2906 */     if (this.vU == true) this.elemSelected.add("U");
/* 2907 */     if (this.vNp == true) this.elemSelected.add("Np");
/* 2908 */     if (this.vPu == true) this.elemSelected.add("Pu");
/* 2909 */     if (this.vAm == true) this.elemSelected.add("Am");
/* 2910 */     if (this.vCm == true) this.elemSelected.add("Cm");
/* 2911 */     if (this.vBk == true) this.elemSelected.add("Bk");
/* 2912 */     if (this.vCf == true) this.elemSelected.add("Cf");
/* 2913 */     if (this.vEs == true) this.elemSelected.add("Es");
/* 2914 */     if (this.vFm == true) this.elemSelected.add("Fm");
/* 2915 */     if (this.vMd == true) this.elemSelected.add("Md");
/* 2916 */     if (this.vNo == true) this.elemSelected.add("No");
/* 2917 */     if (this.vLr == true) this.elemSelected.add("Lr"); 
/*      */   }
/*      */ 
/* 2920 */   private void getPeaks() { this.spectraPeakFind = this.espectro.getPeaks(); }
/*      */ 
/*      */   private String[][] discriminateElements(String[][] spect) {
/* 2923 */     int cont = 0;
/* 2924 */     for (int i = 0; i < spect.length; i++) {
/* 2925 */       String str = spect[i][1];
/* 2926 */       if (str.equals("I")) {
/* 2927 */         cont++;
/*      */       }
/*      */     }
/* 2930 */     String[][] aux = new String[cont][7];
/* 2931 */     cont = 0;
/* 2932 */     for (int i = 0; i < spect.length; i++) {
/* 2933 */       String str = spect[i][1];
/* 2934 */       if (str.equals("I")) {
/* 2935 */         aux[cont][0] = spect[i][0];
/* 2936 */         aux[cont][1] = spect[i][1];
/* 2937 */         aux[cont][2] = spect[i][2];
/* 2938 */         aux[cont][3] = spect[i][3];
/* 2939 */         aux[cont][4] = spect[i][4];
/* 2940 */         aux[cont][5] = spect[i][5];
/* 2941 */         aux[cont][6] = spect[i][6];
/* 2942 */         cont++;
/*      */       }
/*      */     }
/* 2945 */     return aux;
/*      */   }
/*      */   private String[][] discriminatePersistentes(String[][] spect) {
/* 2948 */     int cont = 0;
/* 2949 */     for (int i = 0; i < spect.length; i++) {
/* 2950 */       String str = spect[i][4];
/* 2951 */       if (str.equals("P")) {
/* 2952 */         cont++;
/*      */       }
/*      */     }
/* 2955 */     String[][] aux = new String[cont][7];
/* 2956 */     cont = 0;
/* 2957 */     for (int i = 0; i < spect.length; i++) {
/* 2958 */       String str = spect[i][4];
/* 2959 */       if (str.equals("P")) {
/* 2960 */         aux[cont][0] = spect[i][0];
/* 2961 */         aux[cont][1] = spect[i][1];
/* 2962 */         aux[cont][2] = spect[i][2];
/* 2963 */         aux[cont][3] = spect[i][3];
/* 2964 */         aux[cont][4] = spect[i][4];
/* 2965 */         aux[cont][5] = spect[i][5];
/* 2966 */         aux[cont][6] = spect[i][6];
/* 2967 */         cont++;
/*      */       }
                }
/* 2970 */     return aux;
/*      */   }
/*      */   private String[][] clearExess(String[][] spect) {
/* 2973 */     ArrayList val0 = new ArrayList();
/* 2974 */     ArrayList val1 = new ArrayList();
/* 2975 */     ArrayList val2 = new ArrayList();
/* 2976 */     ArrayList val3 = new ArrayList();
/* 2977 */     ArrayList val4 = new ArrayList();
/* 2978 */     ArrayList val5 = new ArrayList();
/* 2979 */     ArrayList val6 = new ArrayList();
/* 2980 */     for (int i = 0; i < spect.length; i++) {
/* 2981 */       if (i == 0) {
/* 2982 */         val0.add(spect[i][0]);
/* 2983 */         val1.add(spect[i][1]);
/* 2984 */         val2.add(spect[i][2]);
/* 2985 */         val3.add(spect[i][3]);
/* 2986 */         val4.add(spect[i][4]);
/* 2987 */         val5.add(spect[i][5]);
/* 2988 */         val6.add(spect[i][6]);
/* 2989 */       } else if (!spect[i][6].equals(spect[(i - 1)][6])) {
/* 2990 */         val0.add(spect[i][0]);
/* 2991 */         val1.add(spect[i][1]);
/* 2992 */         val2.add(spect[i][2]);
/* 2993 */         val3.add(spect[i][3]);
/* 2994 */         val4.add(spect[i][4]);
/* 2995 */         val5.add(spect[i][5]);
/* 2996 */         val6.add(spect[i][6]);
/*      */       }
/*      */     }
/* 2999 */     spect = new String[val0.size()][7];
/* 3000 */     for (int i = 0; i < spect.length; i++) {
/* 3001 */       spect[i][0] = ((String)val0.get(i));
/* 3002 */       spect[i][1] = ((String)val1.get(i));
/* 3003 */       spect[i][2] = ((String)val2.get(i));
/* 3004 */       spect[i][3] = ((String)val3.get(i));
/* 3005 */       spect[i][4] = ((String)val4.get(i));
/* 3006 */       spect[i][5] = ((String)val5.get(i));
/* 3007 */       spect[i][6] = ((String)val6.get(i));
/*      */     }
/* 3009 */     return spect;
/*      */   }
/*      */ 
/*      */   private void findElements() {
/* 3013 */     this.err = Float.valueOf(this.spinnerTolerance.getValue().toString()).floatValue();
/* 3014 */     this.cnn = new DBCnn();
/* 3015 */     for (int i = 0; i < this.elemSelected.size(); i++) {
/* 3016 */       for (int j = 0; j < this.spectraPeakFind.length; j++) {
/* 3017 */         this.cnn.setErr(this.err);
/* 3018 */         this.cnn.setLambda(this.spectraPeakFind[j][0]);
/* 3019 */         this.cnn.setTbName((String)this.elemSelected.get(i));
/* 3020 */         this.cnn.setIntRel(this.spectraPeakFind[j][1]);
/* 3021 */         this.cnn.busqElement();
/*      */       }
/*      */     }
/*      */ 
/* 3025 */     this.spectraFind = this.cnn.getSpectraFind();
/* 3026 */     if (!this.chkMultipleIonization.isSelected()) {
/* 3027 */       String[][] spectA = discriminateElements(this.spectraFind);
/* 3028 */       this.spectraFind = new String[spectA.length][7];
/* 3029 */       System.arraycopy(spectA, 0, this.spectraFind, 0, spectA.length);
/*      */     }
/* 3031 */     if (this.chkPersistenteLine.isSelected()) {
/* 3032 */       String[][] spectA = discriminatePersistentes(this.spectraFind);
/* 3033 */       this.spectraFind = new String[spectA.length][7];
/* 3034 */       System.arraycopy(spectA, 0, this.spectraFind, 0, spectA.length);
/*      */     }
/* 3036 */     String[][] spectA = clearExess(this.spectraFind);
/* 3037 */     this.spectraFind = new String[spectA.length][7];
/* 3038 */     System.arraycopy(spectA, 0, this.spectraFind, 0, spectA.length);
/*      */ 

               int minLineas = 1;
               HashMap<String, Vector<double[]>> identificados = new HashMap<String, Vector<double[]>>();
               Vector<double[]> todos = new Vector<double[]>();
               this.spectraPeakFind = new double[this.spectraFind.length][2];

               for (int i = 0; i < this.spectraFind.length; i++) {
                 String name = this.spectraFind[i][0];
                 double x = Double.parseDouble(this.spectraFind[i][6]);
                 double y = Double.parseDouble(this.spectraFind[i][5]);

                 this.spectraPeakFind[i][0] = x;
                 this.spectraPeakFind[i][1] = y;

                 if (!identificados.containsKey(name)) {
                   identificados.put(name, new Vector<double[]>());
                 }

                 double row[] = {x,y};
                 identificados.get(name).add(row);
                 todos.add(row);
               }

               for (Map.Entry<String, Vector<double[]>> entry : identificados.entrySet()) {
                 String name  = entry.getKey();
                 Vector<double[]> values = entry.getValue();
                 if (values.size() < minLineas) {
                    continue;
                 }

                 if (!this.espectrosIdentificados.containsKey(name)) {
                   EspectroIdentificado E = new EspectroIdentificado();
                   E.name = name;
                   this.espectrosIdentificados.put(name, E);
                 }

                 double data[][] = new double[values.size()][2];
                 values.toArray(data);
                 this.espectrosIdentificados.get(name).setData(data);
               }

               double[][] mergeados = new double[todos.size()][2];
               todos.toArray(mergeados);
               this.elementosIdentificados.setData(mergeados);

               this.notifyChange();

             }
 
             private String trunkValue(String value) {
               float valTrunk = Float.valueOf(value).floatValue();
               double aux = Math.rint(valTrunk * 10.0F) / 10.0D;
               value = String.valueOf(aux);
               return value;
             }

             public synchronized void addChangeListener(ChangeListener listener)
             {
               listenerList.addElement(listener);
             }

             public synchronized void removeChangeListener(ChangeListener listener)
             {
               listenerList.removeElement(listener);
             }

             protected void notifyChange()
             {
               ChangeEvent ev = new ChangeEvent(this);
               for (int i = 0;  i < listenerList.size(); i++) {
                 ((ChangeListener)listenerList.elementAt(i)).stateChanged(ev);
               }
             }

           }

/* Location:           /home/pardo/Desktop/material_beca_ciop/LIBS Analyzer/LIBS Analyzer Linux/LIBSAnalyzer.jar
 * Qualified Name:     IFrames.PeriodicTableIFrame
 * JD-Core Version:    0.6.2
 */
