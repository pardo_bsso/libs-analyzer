/*     */ package systemlibs;
/*     */ 
/*     */ import Algoritmos.AjusteCero;
/*     */ import Algoritmos.AjusteDominio;
/*     */ import Algoritmos.BaseLine;
/*     */ import Algoritmos.CreateGraphic;
/*     */ import Algoritmos.DBCnn;

          import Algoritmos.Espectro;
          import Algoritmos.EspectroIdentificado;
          import Algoritmos.GraficaEspectro;

/*     */ import Algoritmos.NormalizeData;
/*     */ import Algoritmos.PeakDetection;
/*     */ import Algoritmos.SeriesDatos;
/*     */ import Algoritmos.Smoothing;
/*     */ import Algoritmos.TxtManager;
/*     */ import IFrames.AboutIFrame;
/*     */ import IFrames.GraphicEspecIFrame;
/*     */ import IFrames.PeriodicTableIFrame;
/*     */ import IFrames.SetGraphicIFrame;
/*     */ import IFrames.TasksSpectraIFrame;
/*     */ import java.awt.Container;
/*     */ import java.awt.Dimension;
/*     */ import java.awt.Toolkit;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.ActionListener;
/*     */ import java.io.File;
/*     */ import java.io.PrintStream;
          import java.io.IOException;
/*     */ import java.net.URL;
/*     */ import javax.swing.ImageIcon;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JCheckBox;
/*     */ import javax.swing.JDesktopPane;
/*     */ import javax.swing.JFileChooser;
/*     */ import javax.swing.JFrame;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JMenu;
/*     */ import javax.swing.JMenuBar;
/*     */ import javax.swing.JMenuItem;
/*     */ import javax.swing.JPanel;
/*     */ import javax.swing.JSlider;
/*     */ import javax.swing.JToolBar;
/*     */ import javax.swing.filechooser.FileNameExtensionFilter;
/*     */ import org.jfree.chart.ChartPanel;
/*     */

import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

/*     */ public class MDIFrame extends JFrame
/*     */ {
/*  51 */   public static SeriesDatos sd = new SeriesDatos();
/*  52 */   public static CreateGraphic chart = new CreateGraphic();
/*  53 */   DBCnn dbCnn = new DBCnn();
/*     */   Smoothing sm;
/*     */   PeakDetection pd;
/*     */   BaseLine bl;
/*     */   AjusteCero ac;
/*     */   NormalizeData nd;
/*     */   AjusteDominio ad;
/*     */   public ChartPanel chartPanel;
/*  62 */   JButton setEspectro = new JButton();
/*  63 */   JButton grafElement = new JButton();
/*  64 */   JButton exportGraph = new JButton();
/*  65 */   JButton importExcel = new JButton();
/*  66 */   JButton clearGraph = new JButton();
/*  67 */   JButton setGraph = new JButton();
/*  68 */   JButton findElement = new JButton();
/*  69 */   JButton exportPDF = new JButton();
/*  70 */   JSlider valSmooth = new JSlider();
/*  71 */   JPanel contSlider = new JPanel();
/*  72 */   JLabel smoothLevel = new JLabel();
/*  73 */   JCheckBox peakCheck = new JCheckBox();
/*  74 */   JCheckBox baseL = new JCheckBox();
/*     */ 
/*  76 */   public static JDesktopPane desk = new JDesktopPane();
/*     */   JFileChooser popupWin;
            JFileChooser fcImageExport;
/*     */   FileNameExtensionFilter xlsFilter;
/*     */   File graphDir;
/*  80 */   String nameGraph = "Espectro de Onda";
/*  81 */   boolean specFill = false;
/*  82 */   boolean peakDetect = false;
/*  83 */   boolean baseDetect = false;
/*     */   int respMsj;
/*     */   public static double[][] spectra;
            public static Espectro espectro;
            public static GraficaEspectro graficaEspectro;
            private EspectroIdentificado elementosIdentificados;
            private ChangeListener espectroListener;
            private ChangeListener identificacionListener;
/*     */ 
/*     */   public MDIFrame()
/*     */   {
/*  90 */     initComponents();
              GraficaEspectro g;

              this.elementosIdentificados = new EspectroIdentificado(new double[0][2]);
              this.espectro = new Espectro(new double[0][2]);
              this.espectro.name = "";

              g = new GraficaEspectro(this.espectro);
              chart.addGraficaEspectro(g);
              g.render();
              this.graficaEspectro = g;

              // Por ahora no.
              // elementosIdentificados.name = "Elementos Identificados";
              // g = new GraficaEspectro(elementosIdentificados);
              // chart.addGraficaEspectro(g);
              // g.render();

              espectroListener = new ChangeListener() {
                  @Override
                  public void stateChanged(ChangeEvent e) {
                      Espectro E = (Espectro) e.getSource();
                      MDIFrame.this.renderEspectro(E);
                  }
              };

              identificacionListener = new ChangeListener() {
                  @Override
                  public void stateChanged(ChangeEvent e) {
                      PeriodicTableIFrame perTable = (PeriodicTableIFrame) e.getSource();
                      MDIFrame.this.renderPicosIdentificados(perTable);
                  }
              };
/*     */   }
/*     */   private void initComponents() {
/*  93 */     setTitle("LIBS Analyzer");
/*  94 */     setDefaultCloseOperation(3);
/*  95 */     setLocation(0, 0);
/*  96 */     URL url = getClass().getResource("/ImagesSys/LIBS.png");
/*  97 */     ImageIcon img = new ImageIcon(url);
/*  98 */     setIconImage(img.getImage());
/*  99 */     setExtendedState(6);
/* 100 */     getContentPane().add(addDesk());
/* 101 */     setJMenuBar(addMenuBar());
/* 102 */     getContentPane().add(addToolBar(), "North");
/* 103 */     setMinimumSize(new Dimension(800, 600));
/*     */   }
/*     */ 
            public void renderPicosIdentificados (PeriodicTableIFrame P) {

              chart.clearLabels();
              sd.removePeaks();

              sd.setSpectraPeak(P.spectraPeakFind);
              sd.addPeak();
              String[][] labelPeak = new String[P.spectraFind.length][3];
              for (int i = 0; i < P.spectraFind.length; i++) {
                labelPeak[i][0] = String.valueOf(P.spectraFind[i][2]);
                labelPeak[i][1] = String.valueOf(P.spectraFind[i][5]);

                String valIntRelFind = P.spectraFind[i][2];
                String tmp = String.valueOf(Math.rint(Float.parseFloat(valIntRelFind)*10.0F) / 10.0D);

                labelPeak[i][2] = (tmp + "nm  " + P.spectraFind[i][0] + " " + P.spectraFind[i][1]);
              }
              MDIFrame.chart.setLabels(labelPeak, 30);
              MDIFrame.chart.viewLabels();

            }

            public void renderEspectro (Espectro e) {
                String[][] labelPeak;

                sd.setLine(1);
                sd.setSpectraGraph(e.spectra);
                sd.addSpectra();

                double[][] spectraPeak = e.getPeaks();

                sd.setSpectraPeak(spectraPeak);
                sd.addPeak();

                if (e.doPeaks) {
                  labelPeak = new String[spectraPeak.length][3];
                  for (int i = 0; i < spectraPeak.length; i++) {
                    for (int j = 0; j < 2; j++) {
                      labelPeak[i][j] = String.valueOf(spectraPeak[i][j]);
                    }

                    labelPeak[i][2] = String.valueOf(Math.rint(spectraPeak[i][0]*10)/10) + " nm";
                  }
                  chart.setLabels(labelPeak, 25);
                  chart.viewLabels();
                  sd.addPeak();
                } else {
                  chart.clearLabels();
                  sd.removePeaks();
                }
            }

/*     */   public void addGrafToDesk()
/*     */   {
/* 110 */     this.chartPanel = new ChartPanel(chart.creaChart(sd.getCoorXY()));
/* 111 */     this.chartPanel.setBounds(0, 0, desk.getSize().width, desk.getSize().height - 120);
/* 112 */     desk.add(this.chartPanel);
/*     */   }
/*     */   public JDesktopPane addDesk() {
/* 115 */     desk = new JDesktopPane();
/* 116 */     desk.setSize(Toolkit.getDefaultToolkit().getScreenSize());
/* 117 */     desk.setOpaque(true);
/* 118 */     initialize();
/* 119 */     addGrafToDesk();
/* 120 */     System.out.println("Se creo el DesktopPane");
/* 121 */     return desk;
/*     */   }
/*     */ 
/*     */   private JMenuBar addMenuBar()
/*     */   {
/* 128 */     JMenuBar menuBar = new JMenuBar();
/* 129 */     JMenu menuImport = new JMenu("Archivo");
/* 130 */     JMenu menuElements = new JMenu("Gráfica");
/* 131 */     JMenu menuHelp = new JMenu("Ayuda");
/*     */ 
/* 133 */     menuBar.add(menuImport);
/* 134 */     menuBar.add(menuElements);
/* 135 */     menuBar.add(menuHelp);
/*     */ 
/* 137 */     JMenuItem importTabla = new JMenuItem("Importar Tabla");
/* 138 */     JMenuItem exportPDF = new JMenuItem("Exportar Gráfica en PDF");
/* 139 */     JMenuItem exportPNG = new JMenuItem("Exportar Gráfica en JPG / PNG");
/* 140 */     JMenuItem tableElement = new JMenuItem("Graficar  Elemento");
/* 141 */     JMenuItem ajustarEspectro = new JMenuItem("Ajustar Espectro");
/* 142 */     JMenuItem ajustarGrafica = new JMenuItem("Ajustar Gráfica");
/* 143 */     JMenuItem buscarElementos = new JMenuItem("Buscar Elementos");
/* 144 */     JMenuItem clearGrafica = new JMenuItem("Limpiar Gráfica");
/* 145 */     JMenuItem about = new JMenuItem("Acerca de");
/*     */ 
/* 147 */     menuImport.add(importTabla);
/* 148 */     menuImport.add(exportPNG);
/* 149 */     menuImport.add(exportPDF);
/* 150 */     menuElements.add(tableElement);
/* 151 */     menuElements.add(ajustarEspectro);
/* 152 */     menuElements.add(ajustarGrafica);
/* 153 */     menuElements.add(buscarElementos);
/* 154 */     menuElements.add(clearGrafica);
/* 155 */     menuHelp.add(about);
/*     */ 
/* 157 */     System.out.println("Se creo la MenuBar");
/*     */ 
/* 160 */     importTabla.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 162 */         MDIFrame.this.methodImportExcel();
/*     */       }
/*     */     });
/* 166 */     tableElement.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 168 */         MDIFrame.this.methodGraphElement();
/*     */       }
/*     */     });
/* 172 */     exportPNG.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 174 */         MDIFrame.this.methodExportGraphic();
/*     */       }
/*     */     });
/* 178 */     exportPDF.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 180 */         MDIFrame.this.methodExportGraphicPDF();
/*     */       }
/*     */     });
/* 184 */     ajustarEspectro.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 186 */         MDIFrame.this.methodSetEspectro();
/*     */       }
/*     */     });
/* 190 */     ajustarGrafica.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 192 */         MDIFrame.this.methodSetGraphic();
/*     */       }
/*     */     });
/* 196 */     buscarElementos.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 198 */         MDIFrame.this.methodFindElements();
/*     */       }
/*     */     });
/* 202 */     clearGrafica.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 204 */         MDIFrame.this.methodClearGraph();
/*     */       }
/*     */     });
/* 208 */     about.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 210 */         MDIFrame.this.methodAbout();
/*     */       }
/*     */     });
/* 214 */     return menuBar;
/*     */   }
/*     */ 
/*     */   private JToolBar addToolBar()
/*     */   {
/* 219 */     JToolBar toolBar = new JToolBar();
/*     */ 
/* 221 */     this.grafElement.setIcon(new ImageIcon(getClass().getResource("/ImagesSys/Element.png")));
/* 222 */     this.grafElement.setToolTipText("Graficar Elemento");
/* 223 */     this.setEspectro.setIcon(new ImageIcon(getClass().getResource("/ImagesSys/SetTask.png")));
/* 224 */     this.setEspectro.setToolTipText("Ajustar Espectros");
/* 225 */     this.exportGraph.setIcon(new ImageIcon(getClass().getResource("/ImagesSys/ExportGraf.png")));
/* 226 */     this.exportGraph.setToolTipText("Exportar Gráfica en JPG / PNG");
/* 227 */     this.exportPDF.setIcon(new ImageIcon(getClass().getResource("/ImagesSys/ExportPdf.png")));
/* 228 */     this.exportPDF.setToolTipText("Exportar Gráfica en PDF");
/* 229 */     this.importExcel.setIcon(new ImageIcon(getClass().getResource("/ImagesSys/ImportT.png")));
/* 230 */     this.importExcel.setToolTipText("Importar Tabla");
/* 231 */     this.clearGraph.setIcon(new ImageIcon(getClass().getResource("/ImagesSys/ClearGraph.png")));
/* 232 */     this.clearGraph.setToolTipText("Limpiar Gráfica");
/* 233 */     this.setGraph.setIcon(new ImageIcon(getClass().getResource("/ImagesSys/Engrane.png")));
/* 234 */     this.setGraph.setToolTipText("Ajustar Gráfica");
/* 235 */     this.findElement.setIcon(new ImageIcon(getClass().getResource("/ImagesSys/FindElement.png")));
/* 236 */     this.findElement.setToolTipText("Buscar Elementos");
/* 237 */     this.smoothLevel.setText("Suavisado: ");
/*     */ 
/* 239 */     toolBar.add(this.importExcel);
/* 240 */     toolBar.add(this.grafElement);
/* 241 */     toolBar.add(this.setEspectro);
/* 242 */     toolBar.add(this.setGraph);
/* 243 */     toolBar.add(this.findElement);
/* 244 */     toolBar.add(this.exportGraph);
/* 245 */     toolBar.add(this.exportPDF);
/* 246 */     toolBar.add(this.clearGraph);
/*     */ 
/* 249 */     this.setEspectro.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 251 */         MDIFrame.this.methodSetEspectro();
/*     */       }
/*     */     });
/* 255 */     this.grafElement.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 257 */         MDIFrame.this.methodGraphElement();
/*     */       }
/*     */     });
/* 261 */     this.exportGraph.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 263 */         MDIFrame.this.methodExportGraphic();
/*     */       }
/*     */     });
/* 267 */     this.exportPDF.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 269 */         MDIFrame.this.methodExportGraphicPDF();
/*     */       }
/*     */     });
/* 273 */     this.importExcel.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 275 */         MDIFrame.this.methodImportExcel();
/*     */       }
/*     */     });
/* 279 */     this.clearGraph.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 281 */         MDIFrame.this.methodClearGraph();
/*     */       }
/*     */     });
/* 285 */     this.setGraph.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 287 */         MDIFrame.this.methodSetGraphic();
/*     */       }
/*     */     });
/* 291 */     this.findElement.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/* 293 */         MDIFrame.this.methodFindElements();
/*     */       }
/*     */     });
/* 297 */     return toolBar;
/*     */   }
/*     */ 
/*     */   private void initialize()
/*     */   {
///* 302 */     spectra = new double[3][2];
///* 303 */     spectra[0][0] = 100.0D;
///* 304 */     spectra[0][1] = 1000.0D;
///* 305 */     spectra[1][0] = 1.0D;
///* 306 */     spectra[1][1] = 0.0D;
///*     */ 
// /* 308 */     sd.setLine(1);
// /* 309 */     sd.setSpectraGraph(spectra);
// /* 310 */     sd.addSpectra();
/*     */   }
/*     */ 
/*     */   private void methodSetEspectro()
/*     */   {
/* 316 */     System.out.println("Evento de importLibrary");
/* 317 */     TasksSpectraIFrame tasksSpectra = new TasksSpectraIFrame(this.espectro, this.graficaEspectro);
/* 319 */     tasksSpectra.show();
/*     */   }
/*     */ 
/*     */   private void methodGraphElement() {
/* 323 */     System.out.println("Evento de tableElement");
/* 324 */     GraphicEspecIFrame graphEspec = new GraphicEspecIFrame();
/* 325 */     desk.add(graphEspec);
/* 326 */     graphEspec.show();
/*     */   }
/*     */ 
            private void methodExportGraphic() {
              JFileChooser fc = this.fcImageExport;
              int respMsj = 0;

              System.out.println("Evento exportGraph");

              if (this.fcImageExport == null) {
                fc = this.fcImageExport = new JFileChooser();
                fc.setDialogTitle("Exportar Grafica");

                fc.setAcceptAllFileFilterUsed(false);
                fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
                fc.setFileFilter( new FileNameExtensionFilter("Imágenes PNG", "png") );
                fc.setFileFilter( new FileNameExtensionFilter("Imágenes JPG", "jpg") );
              }

              fc.setSelectedFile( new File(this.nameGraph.replace(" ", "_") ));

              respMsj = fc.showSaveDialog(null);

              if (respMsj == JFileChooser.APPROVE_OPTION) {

                FileNameExtensionFilter filter = (FileNameExtensionFilter) fc.getFileFilter();

                String extension = "." + filter.getExtensions()[0];
                String filename;
                try {
                  filename = fc.getSelectedFile().getCanonicalPath();
                } catch (java.io.IOException e) {
                  System.out.println("methodExportGraphic(): IOException");
                  return;
                }

                if (!filename.toLowerCase().endsWith(extension)) {
                  filename = filename + extension;
                }

                this.nameGraph = filename;
                this.graphDir = new File(filename);

                // Switch con String despues de Java SE7. Ha.
                if (extension.equals(".jpg")) {
                  chart.exportJPEGGraph(this.graphDir);
                }
                if (extension.equals(".png")) {
                  chart.exportPNGGraph(this.graphDir);
                }

              }
            }
/*     */ 
/*     */   private void methodExportGraphicPDF()
/*     */   {
/* 345 */     System.out.println("Evento exportGraph");
/* 346 */     this.popupWin = new JFileChooser();
/* 347 */     this.popupWin.setDialogTitle("Exportar Grafica en PDF");
/*     */ 
/* 349 */     this.popupWin.setSelectedFile(new File(this.nameGraph));
/* 350 */     this.respMsj = this.popupWin.showSaveDialog(null);
/* 351 */     if (this.respMsj == 0) {
/* 352 */       this.nameGraph = (this.popupWin.getSelectedFile() + ".pdf");
/* 353 */       this.graphDir = new File(this.nameGraph);
/* 354 */       chart.writeChartToPDF(this.chartPanel.getChart(), this.graphDir);
/*     */     }
/*     */   }
/*     */ 
/*     */   private void methodImportExcel() {
              try {
                System.out.println("Evento de tableElement");
                this.popupWin = new JFileChooser();
                this.xlsFilter = new FileNameExtensionFilter("Tabla txt", new String[] { "txt" });
                this.popupWin.setFileFilter(this.xlsFilter);
                this.popupWin.setDialogTitle("Importar Tabla");
                this.respMsj = this.popupWin.showOpenDialog(null);
                if (this.respMsj == 0) {
                  methodClearGraph();
                  this.graphDir = this.popupWin.getSelectedFile();
                  this.nameGraph = this.popupWin.getName(this.graphDir);
                  TxtManager tableTxt = new TxtManager();
                  tableTxt.setInputFile(String.valueOf(this.graphDir));
                  spectra = tableTxt.read();
                  this.specFill = true;
                  this.espectro.setData(spectra);
                }
              } catch (java.io.IOException ex) {
                System.out.println("Whats up " + ex);
              }
/*     */   }
/*     */ 
/*     */   private void methodClearGraph() {
/* 391 */     chart.clearLabels();
/* 392 */     sd.removePeaks();
/* 393 */     spectra = new double[3][2];
/* 394 */     initialize();
/*     */   }
/*     */ 
/*     */   private void methodSetGraphic() {
/* 398 */     System.out.println("Evento de setGraphic");
/* 399 */     SetGraphicIFrame setGraph = new SetGraphicIFrame();
/* 400 */     desk.add(setGraph);
/* 401 */     setGraph.show();
/*     */   }
/*     */ 
/*     */   private void methodFindElements() {
/* 405 */     PeriodicTableIFrame perTable = new PeriodicTableIFrame(this.espectro);
              perTable.addChangeListener(this.identificacionListener);
/* 407 */     perTable.show();
/*     */   }
/*     */   private void methodAbout() {
/* 410 */     AboutIFrame aboutInfo = new AboutIFrame();
/* 411 */     desk.add(aboutInfo);
/* 412 */     aboutInfo.show();
/*     */   }
/*     */ }

/* Location:           /home/pardo/Desktop/material_beca_ciop/LIBS Analyzer/LIBS Analyzer Linux/LIBSAnalyzer.jar
 * Qualified Name:     systemlibs.MDIFrame
 * JD-Core Version:    0.6.2
 */
