package Algoritmos;

import java.util.ArrayList;

public class BaseLine
{

  public enum BaselineMode {
    NAIVE,
    ORIGINAL,
    RUBBERBAND,
  }

  public BaselineMode mode = BaselineMode.NAIVE;

  public double[][] spectraAux;

  public int iterations = 1;

  private Smoothing smoothingFilter = new Smoothing();
  private NormalizeData normalizeFilter = new NormalizeData();

  public void BaseSpectro(double[][] espectro)
  {
    this.spectraAux = espectro;
  }

  public void setMode(BaselineMode t)
  {
    this.mode = t;
  }

  public void setMode(int t)
  {
    switch (t) {
      case 0:
      default:
        this.mode = BaselineMode.NAIVE;
        break;

      case 1:
        this.mode = BaselineMode.ORIGINAL;
        break;

      case 2:
        this.mode = BaselineMode.RUBBERBAND;
        break;
    }
  }


  public double[][] process(double[][] src)
  {
    return this.process(src, 1, this.iterations);
  }

  public double[][] process(double[][] src, int smoothing, int iterations)
  {

    switch(this.mode) {
      case NAIVE:
      default:
        return process_naive(src, smoothing, iterations);


      case ORIGINAL:
        return process_original(src, smoothing, iterations);
    }
  }

  private double[][] process_original(double[][] src, int smoothing, int iterations)
  {
    iterations = (int)(15 * ((float)iterations / 1000));
    double[][] tmp = src;
    while (iterations > 0) {
      tmp = this.baseLine(smoothing, tmp);
      tmp = this.normalizeFilter.process(tmp, 1);
      iterations -= 1;
    }
    return tmp;
  }

  private double[][] process_naive(double[][] src, int smoothing, int iterations)
  {
    //double beta = 0.95 + 2 * 0.00001 * iterations;
    double beta = 0.95 + 2 * 0.00001 * 1;
    double alpha = 1 - beta;
    double out[][] = new double[src.length][2];
    double filtered[][] = new double[src.length][2];
    int windowSize = (int)(50 + 400*((float)iterations/1000));
    double window[] = new double[windowSize];
    int    wIdx = 0;

    System.out.printf("process() iterations: %d alpha: %.5f beta: %.5f\n", iterations, alpha, beta);

    for (; wIdx < window.length; wIdx ++) {
      if (wIdx > src.length) {
        break;
      }
      window[wIdx] = src[wIdx][1];
    }

    filtered[0][1] = src[0][1];
    filtered[1][1] = src[0][1];

    for (int idx=0; idx < src.length; idx++) {
      int prev = (idx - 1) >= 0 ? (idx-1) : 0;
      double sample = src[idx][1];

      window[(idx + window.length/2)%window.length] = sample;
      double med = 0;
      for (int i=0; i<window.length; i++) {
        med += window[i];
      }
      med /= window.length;

      sample = sample > 1.5*med ? med : sample;

      filtered[idx][0] = src[idx][0];
      out[idx][0] = src[idx][0];
      filtered[idx][1] = alpha * sample + beta * filtered[prev][1];

      if (filtered[idx][1] < src[idx][1]) {
        out[idx][1] = src[idx][1] - filtered[idx][1];
      } else {
        out[idx][1] = 0;
      }
    }

    return out;
  }

  public double[][] baseL() {
    return this.baseL(this.spectraAux);
  }

  public double[][] baseL(double[][] src) {
    ArrayList valY = new ArrayList();
    ArrayList valX = new ArrayList();

    int    cont;
    double xStart;
    double xEnd;
    double media;
    double m1;
    double m2;
    double m;
    double x0;
    double y0;
    double x1;
    double y1;
    double x2;
    double y2;
    double difY;


    for (int i = 0; i + 2 < src.length; i++) {
      x0 =  src[i][0];
      y0 = (src[i][1] * 100.0D);

      x1 =  src[(i + 1)][0];
      y1 = (src[(i + 1)][1] * 100.0D);

      x2 =  src[(i + 2)][0];
      y2 = (src[(i + 2)][1] * 100.0D);

      if (x1 - x0 != 0.0D) m1 = ((y1 - y0) / (x1 - x0)); else m1 = 0.0D;
      if (x2 - x1 != 0.0D) m2 = ((y2 - y1) / (x2 - x1)); else m2 = 0.0D;

      if ((m1 <=  0.0D) && (m2 >= 0.0D) &&
          (m1 > -0.5D) && (m2 < 0.5D)
      ) {
            valX.add( Double.valueOf( src[(i + 1)][0]        ));
            valY.add( Double.valueOf( src[(i + 1)][1] / 5.0D ));
      }
    }

    double[][] spectramin = new double[valX.size()][2];
    for (int i = 0; i < valX.size(); i++) {
      spectramin[i][0] = ((Double)valX.get(i)).doubleValue();
      spectramin[i][1] = ((Double)valY.get(i)).doubleValue();
    }

    valX.clear();
    valY.clear();

    cont = 0;
    for (int i = 0; i < src.length; i++) {
      if (cont == 0) {
        x0 = src[0][0];
        y0 = src[0][1];
        x1 = spectramin[cont][0];
        y1 = spectramin[cont][1];
      } else if (cont == spectramin.length - 1) {
        x0 = spectramin[cont][0];
        y0 = spectramin[cont][1];
        x1 = src[(src.length - 1)][0];
        y1 = src[(src.length - 1)][1];
      } else {
        x0 = spectramin[(cont - 1)][0];
        y0 = spectramin[(cont - 1)][1];
        x1 = spectramin[cont][0];
        y1 = spectramin[cont][1];
      }

      if (x1 - x0 != 0.0D) {
        m = ((y1 - y0) / (x1 - x0));
      } else {
        m = 0.0D;
      }

      y2 = (m * (src[i][0] - x0) + y0);

      valX.add(Double.valueOf(src[i][0]));

      if (src[i][1] >= y2) {
        valY.add(Double.valueOf(y2));
      } else {
        valY.add(Double.valueOf(src[i][1]));
      }

      if ((src[i][0] == spectramin[cont][0]) && (cont < spectramin.length - 1)) {
        cont++;
      }
    }

    double[][] spectraBLine = new double[valX.size()][2];

    for (int i = 0; i < valX.size(); i++) {
      spectraBLine[i][0] = ((Double)valX.get(i)).doubleValue();
      spectraBLine[i][1] = ((Double)valY.get(i)).doubleValue();

      if (src[i][1] < spectraBLine[i][1]) {
        spectraBLine[i][1] = src[i][1];
      }
      if (spectraBLine[i][1] < 0.0D) {
        spectraBLine[i][1] = 0.0D;
      }
    }
    return spectraBLine;
  }

  private double mediaAr(double[][] espectro) {
    double suma = 0.0D;

    for (int i = 0; i < espectro.length; i++) {
      suma += espectro[i][1];
    }
    double med = suma / espectro.length;
    return med;
  }

  public double[][] baseLine(int n)
  {
    return this.baseLine(n, this.spectraAux);
  }

  private double[][] baseLine(int n, double[][] src)
  {
    int     ancho = 2 * n;
    boolean flag  = true;
    double  dif   = 0.0D;

    double[][] spectraBLine = baseL(src);

    spectraBLine = this.smoothingFilter.moving_average_filter(n, spectraBLine);


    for (int i = 0; i < spectraBLine.length; i++) {
      dif = src[(i + ancho)][1] - spectraBLine[i][1];
      if (dif <= 0.0D) {
        spectraBLine[i][1] = src[(i + ancho)][1];
      }
      spectraBLine[i][1] = (src[(i + ancho)][1] - spectraBLine[i][1]);
    }
    return spectraBLine;
  }
}

/* Location:           /home/pardo/Desktop/material_beca_ciop/LIBS Analyzer/LIBS Analyzer Linux/LIBSAnalyzer.jar
 * Qualified Name:     Algoritmos.BaseLine
 * JD-Core Version:    0.6.2
 */
