package Algoritmos;

public class NormalizeData
{
  public enum NormalyzeMode {
    AUTO,
    MANUAL
  }

  public NormalyzeMode mode = NormalyzeMode.AUTO;
  public int lambda_min = 100;
  public int lambda_max = 1000;

  private double[][] spectraAux;

  public NormalizeData()
  {
  }

  public NormalizeData(double[][] espectro)
  {
    this.spectraAux = espectro;
  }

  public double[][] process()
  {
    return this.normalizeData(this.spectraAux, 1);
  }

  public double[][] process(double[][] src)
  {
    return this.normalizeData(src, 1);
  }

  public double[][] process(double[][] src, int target)
  {
    return this.normalizeData(src, target);
  }

  public double[][] normalizeData(double[][] src, int normVal)
  {
    double max = valMax(src);
    double[][] tmp = new double[src.length][2];
    for (int i = 0; i < src.length; i++) {
      tmp[i][0] = src[i][0];
      tmp[i][1] = Math.min( ((src[i][1] / max) * normVal), (double)normVal );
    }

    return tmp;
  }

  public void setMode(NormalyzeMode t)
  {
    this.mode = t;
  }

  public void setMode(int t)
  {
    switch (t) {
      case 0:
      default:
        this.mode = NormalyzeMode.AUTO;
        break;

      case 1:
        this.mode = NormalyzeMode.MANUAL;
        break;
    }
  }


  private double valMax(double[][] espectro)
  {
    if (espectro.length == 0) {
      return 1;
    }

    double maxVal = espectro[0][1];

    if (this.lambda_min > this.lambda_max) {
        int t = this.lambda_max;
        this.lambda_max = this.lambda_min;
        this.lambda_min = t;
    }

    for (int i = 0; i < espectro.length; i++) {
      if(this.mode == NormalyzeMode.MANUAL) {
        if (espectro[i][0] < this.lambda_min)
          continue;
        if (espectro[i][0] > this.lambda_max)
          continue;
      }
      maxVal = Math.max(espectro[i][1], maxVal);
    }
    return maxVal;
  }
}

/* Location:           /home/pardo/Desktop/material_beca_ciop/LIBS Analyzer/LIBS Analyzer Linux/LIBSAnalyzer.jar
 * Qualified Name:     Algoritmos.NormalizeData
 * JD-Core Version:    0.6.2
 */
