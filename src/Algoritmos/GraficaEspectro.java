package Algoritmos;

import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Polygon;

import java.text.NumberFormat;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.chart.labels.StandardXYItemLabelGenerator;
import org.jfree.chart.labels.XYItemLabelGenerator;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;

import org.jfree.ui.TextAnchor;

import org.jfree.util.ShapeUtilities;

import Algoritmos.Espectro;

public class GraficaEspectro
{

    public int line = 1;

    public int fontSize = 12;

    public  boolean peaksVisible = true;
    public  boolean labelsVisible = true;

    private XYSeriesCollection dataset;
    private XYSeriesCollection peaksDataset;

    private XYSeries dataSeries;
    private XYSeries peaksSeries;

    private XYLineAndShapeRenderer renderer;
    private XYLineAndShapeRenderer peaksRenderer;

    private Espectro espectro;
    private ChangeListener espectroListener;

    public GraficaEspectro (Espectro e)
    {
        this(e, "", "");
    }

    public GraficaEspectro (Espectro e, String markerSuffix, String markerPrefix)
    {
        this.espectro = e;

        this.dataset = new XYSeriesCollection();
        this.peaksDataset = new XYSeriesCollection();

        this.dataSeries  = new XYSeries(e.name);
        this.peaksSeries = new XYSeries("");

        this.dataset.addSeries(this.dataSeries);
        this.peaksDataset.addSeries(this.peaksSeries);

        espectroListener = new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                GraficaEspectro.this.render();
            }
        };

        espectro.addChangeListener(espectroListener);

        Font labelFont = new Font("Helvetica", 0, 16);

        int[] xPoly = { 1, 5, 9 };
        int[] yPoly = { 1, 9, 1 };
        Polygon shape = new Polygon(xPoly, yPoly, xPoly.length);

        renderer =  new XYLineAndShapeRenderer();
        peaksRenderer =  new XYLineAndShapeRenderer();

        renderer.setBaseLegendTextFont(labelFont);
        renderer.setSeriesLinesVisible(0, true);
        renderer.setSeriesShapesVisible(0, false);
        renderer.setSeriesStroke(0, new BasicStroke(1.2f, 1, 2));

        //render.setSeriesPaint(0, g0Color);
        //render.setSeriesPaint(1, g1Color);

        labelFont = new Font("Helvetica", 0, this.fontSize);
        peaksRenderer.setBaseItemLabelFont(labelFont);
        peaksRenderer.setSeriesLinesVisible(0, false);
        peaksRenderer.setSeriesItemLabelsVisible(0, true);
        peaksRenderer.setSeriesShape(0, ShapeUtilities.createTranslatedShape(shape, -4.5D, -12.0D));
        peaksRenderer.setSeriesShapesVisible(0, true);
        peaksRenderer.setSeriesStroke(0, new BasicStroke(1.2f, 1, 2));

        NumberFormat format = NumberFormat.getNumberInstance();
        format.setMaximumFractionDigits(2);
        XYItemLabelGenerator generator = new StandardXYItemLabelGenerator(markerPrefix + "{1} nm. ({2})" + markerSuffix, format, format);

        peaksRenderer.setBaseItemLabelGenerator(generator);

        peaksRenderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, TextAnchor.CENTER_LEFT, TextAnchor.CENTER_LEFT, -1.57));

        peaksRenderer.setItemLabelAnchorOffset(10.0);
    }

    public void render ()
    {
        renderSpectra();
        renderPeaks();
    }

    public XYSeriesCollection getDataset ()
    {
        return this.dataset;
    }

    public XYSeriesCollection getPeaksDataset ()
    {
        return this.peaksDataset;
    }

    public XYLineAndShapeRenderer getRenderer ()
    {
        return this.renderer;
    }

    public XYLineAndShapeRenderer getPeaksRenderer ()
    {
        return this.peaksRenderer;
    }

    public void setLine (int tipo)
    {
        this.line = tipo;
    }

    public void renderSpectra ()
    {
        double xPoint;
        double yPoint;

        double[][] spectra = this.espectro.spectra;

        this.dataSeries.clear ();

        for (int i = 0; i < spectra.length; i++) {
          xPoint = spectra[i][0];
          yPoint = spectra[i][1];
          boolean notify = (i == spectra.length - 1);
          if (this.line == 0) {
            this.dataSeries.add(xPoint, 0.0D);
            this.dataSeries.add(xPoint, yPoint);
            this.dataSeries.add(xPoint, 0.0D, notify);
          } else {
            this.dataSeries.add(xPoint, yPoint, notify);
          }
        }
    }

    public void renderPeaks ()
    {
        double xPoint;
        double yPoint;

        double[][] peaks = this.espectro.getPeaks();

        this.peaksSeries.clear();

        for (int i = 0; i < peaks.length; i++) {
            boolean notify = (i == peaks.length - 1);
            xPoint = peaks[i][0];
            yPoint = peaks[i][1];
            this.peaksSeries.add(xPoint, yPoint, notify);
        }
    }

    public void removePeaks () {
        this.peaksSeries.clear();
    }

    public void setLabelsVisibility (boolean visible) {
        this.labelsVisible = visible;
        if (visible) {
            this.showLabels();
        } else {
            this.hideLabels();
        }
    }

    public void showLabels () {
        if (!this.labelsVisible) {
            return;
        }
        peaksRenderer.setSeriesItemLabelsVisible(0, true);
    }

    public void hideLabels () {
        peaksRenderer.setSeriesItemLabelsVisible(0, false);
    }

    public void setPeaksVisibility (boolean visible) {
        this.peaksVisible = visible;
        if (visible) {
            this.showPeaks();
        } else {
            this.hidePeaks();
        }
    }

    public void showPeaks () {
        if (!this.peaksVisible) {
            return;
        }
        peaksRenderer.setSeriesShapesVisible(0, true);
        if (!this.labelsVisible) {
            this.hideLabels();
        } else {
            this.showLabels();
        }
    }

    public void hidePeaks () {
        peaksRenderer.setSeriesShapesVisible(0, false);
    }

    public void setFontSize (int size) {
        this.fontSize = size;
        Font labelFont = new Font("Helvetica", 0, this.fontSize);
        peaksRenderer.setBaseItemLabelFont(labelFont);
    }

    public void setPeaksColor (Color c) {
        peaksRenderer.setSeriesPaint(0, c);
    }

    public Color getPeaksColor () {
        return (Color) peaksRenderer.lookupSeriesPaint(0);
    }

    public void setSpectraColor (Color c) {
        renderer.setSeriesPaint(0, c);
    }

    public Color getSpectraColor () {
        return (Color) renderer.lookupSeriesPaint(0);
    }

    public Espectro getEspectro () {
        return this.espectro;
    }

}
