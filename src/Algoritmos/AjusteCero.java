package Algoritmos;

import java.lang.Math;

public class AjusteCero
{
  public double[][] spectraAux;
  public double min;

  public AjusteCero(double[][] espectro)
  {
    this.spectraAux = espectro;
  }

  public AjusteCero()
  {
  }

  public double[][] process()
  {
    return this.process(this.spectraAux);
  }

  public double[][] process(double[][] src)
  {
    this.min = valMin(src);

    double tmp[][] = new double[src.length][2];

    for (int i = 0; i < src.length; i++) {
      tmp[i][0] =  src[i][0];
      tmp[i][1] = (src[i][1] + Math.abs(this.min));
    }

    return tmp;
  }

  private double valMin(double[][] espectro)
  {
    if (espectro.length == 0) {
      return 0;
    }

    double minVal = espectro[0][1];

    for (int i = 0; i < espectro.length; i++) {
      double valRead = espectro[i][1];
      if (valRead < minVal) {
        minVal = valRead;
      }
    }
    return minVal;
  }
}

/* Location:           /home/pardo/Desktop/material_beca_ciop/LIBS Analyzer/LIBS Analyzer Linux/LIBSAnalyzer.jar
 * Qualified Name:     Algoritmos.AjusteCero
 * JD-Core Version:    0.6.2
 */
