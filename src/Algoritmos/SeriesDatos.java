/*     */ package Algoritmos;
/*     */ 
/*     */ import org.jfree.data.xy.XYSeries;
/*     */ import org.jfree.data.xy.XYSeriesCollection;
/*     */ 
/*     */ public class SeriesDatos
/*     */ {
/*     */   double[][] spectraAux;
/*     */   double[][] peak;
/*     */   double[][] base;
/*  20 */   XYSeriesCollection coorXY = new XYSeriesCollection();
/*     */ 
/*  22 */   XYSeries coorGraf = new XYSeries("Espectro");
/*  23 */   XYSeries coorBase = new XYSeries("Espectro");
/*  24 */   XYSeries coorPeak = new XYSeries("Picos");
/*     */   double xPoint;
/*     */   double yPoint;
/*  33 */   public int line = 0;
/*  34 */   public boolean peakDetect = false;
/*     */ 
/*     */   public SeriesDatos()
/*     */   {
/*  27 */     this.coorXY.addSeries(this.coorGraf);
/*     */ 
/*  29 */     this.coorXY.addSeries(this.coorPeak);
/*     */   }
/*     */ 
/*     */   public void setLine(int tipo)
/*     */   {
/*  37 */     this.line = tipo;
/*     */   }
/*     */ 
/*     */   public void setPeakDetect(boolean peakDetect) {
/*  41 */     this.peakDetect = peakDetect;
/*     */   }
/*     */ 
/*     */   public void setSpectra(double[][] specGraph, double[][] specPeak) {
/*  45 */     this.spectraAux = specGraph;
/*  46 */     this.peak = specPeak;
/*     */   }
/*     */   public void setSpectraGraph(double[][] specGraph) {
/*  49 */     this.spectraAux = specGraph;
/*     */   }
/*     */   public void setSpectraPeak(double[][] specPeak) {
/*  52 */     this.peak = specPeak;
/*     */   }
/*     */   public void setSpectraBase(double[][] specBase) {
/*  55 */     this.base = specBase;
/*     */   }
/*     */   public XYSeriesCollection getCoorXY() {
/*  58 */     return this.coorXY;
/*     */   }
/*     */ 
/*     */   public void addSpectra()
/*     */   {
/*     */ 
/*  65 */     this.coorGraf.clear();
/*     */ 
/*  67 */     for (int i = 0; i < this.spectraAux.length; i++) {
/*  68 */       this.xPoint = this.spectraAux[i][0];
/*  69 */       this.yPoint = this.spectraAux[i][1];
/*  70 */       if (this.line == 0) {
/*  71 */         this.coorGraf.add(this.xPoint, 0.0D);
/*  72 */         this.coorGraf.add(this.xPoint, this.yPoint);
/*  73 */         this.coorGraf.add(this.xPoint, 0.0D);
/*     */       } else {
/*  75 */         this.coorGraf.add(this.xPoint, this.yPoint);
/*     */       }
/*     */     }
/*     */ 
/*     */   }
/*     */ 
/*     */   public void addPeak()
/*     */   {
/*     */ 
/*  86 */     this.coorPeak.clear();
/*  87 */     for (int i = 0; i < this.peak.length; i++) {
/*  88 */       this.xPoint = this.peak[i][0];
/*  89 */       this.yPoint = this.peak[i][1];
/*  90 */       this.coorPeak.add(this.xPoint, this.yPoint);
/*     */     }
/*     */ 
/*     */   }
/*     */ 
/*     */   public void addBase() {
/*  97 */     this.coorXY.removeSeries(this.coorBase);
/*     */ 
/*  99 */     this.coorBase.clear();
/* 100 */     for (int i = 0; i < this.base.length; i++) {
/* 101 */       this.xPoint = this.base[i][0];
/* 102 */       this.yPoint = this.base[i][1];
/* 103 */       this.coorBase.add(this.xPoint, this.yPoint);
/*     */     }
/* 105 */     addOrderSeries();
/*     */   }
/*     */ 
/*     */   public void removePeaks() {
/* 109 */     this.coorPeak.clear();
/*     */   }
/*     */ 
/*     */   private void addOrderSeries() {
/* 113 */     this.coorXY.removeAllSeries();
/*     */ 
/* 115 */     this.coorXY.addSeries(this.coorGraf);
/*     */ 
/* 118 */     this.coorXY.addSeries(this.coorPeak);
/*     */   }
/*     */ }

/* Location:           /home/pardo/Desktop/material_beca_ciop/LIBS Analyzer/LIBS Analyzer Linux/LIBSAnalyzer.jar
 * Qualified Name:     Algoritmos.SeriesDatos
 * JD-Core Version:    0.6.2
 */
