package Algoritmos;

import java.util.Arrays;
import java.util.Vector;

import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;


public class EspectroIdentificado extends Espectro
{
  public boolean doPeaks     = true;

  public EspectroIdentificado()
  {
    this.setData(new double[0][2]);
    this.spectra = new double[0][2];
  }

  public EspectroIdentificado(double[][] data)
  {
    this.setData(data);
    this.spectra = new double[0][2];
  }

  @Override
  public void setData(double[][] data)
  {
    this.orig_spectra = clone2DMatrix(data);
    this.process();
  }

  @Override
  public double[][] process ()
  {
    this.notifyChange();
    return new double[0][2];
  }

  @Override
  public double[][] getPeaks ()
  {
    return this.orig_spectra;
  }

}

