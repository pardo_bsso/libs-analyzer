package Algoritmos;

import java.util.Vector;

public class RangeClip
{
  public double[][] spectraAux;
  public int lambda_min = 0;
  public int lambda_max = 1000;


  public void RangeClip(double[][] espectro)
  {
    this.spectraAux = espectro;
  }

  public void RangeClip()
  {
  }

  public double[][] process ()
  {
    return this.process(this.spectraAux);
  }

  public double[][] process(double[][] src)
  {
    Vector<Double> X = new Vector<Double>();
    Vector<Double> Y = new Vector<Double>();

    double lmin = this.lambda_min;
    double lmax = this.lambda_max;

    if (lmin > lmax) {
      double tmp = lmin;
      lmin = lmax;
      lmax = tmp;
    }

    for (int i = 0; i < src.length; i++) {
      double x = src[i][0];
      double y = src[i][1];
      if (x > lmax) {
        continue;
      }
      if (x < lmin) {
        continue;
      }
      X.add(x);
      Y.add(y);
    }

    int size = X.size();
    double[][] tmp = new double[size][2];

    for (int i = 0; i < size; i++) {
      tmp[i][0] = X.get(i);
      tmp[i][1] = Y.get(i);
    }

    return tmp;
  }
}
