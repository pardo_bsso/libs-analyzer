package Algoritmos;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.StringTokenizer;

import java.util.Vector;


public class TxtManager
{
  private String inputFile;
  public double[][] spectraAux;

  public void setInputFile(String inputFile)
  {
    this.inputFile = inputFile;
  }

  public double[][] read() throws IOException {
    try {
      String lineIn = "";
      FileReader inputTxt = new FileReader(this.inputFile);
      BufferedReader cont = new BufferedReader(inputTxt);
      Vector<double[]> rows = new Vector<double[]>();

      while ((lineIn = cont.readLine()) != null) {
        lineIn = lineIn.trim();

        StringTokenizer tokens = new StringTokenizer(lineIn);

        // HACK: eliminar comentarios. No es muy robusto.
        if (!lineIn.matches("^[0-9].+")) {
          continue;
        }

        if (tokens.countTokens() < 2) {
          continue;
        }

        String tmp;
        double[] row = {0,0};

        // XXX FIXME: posiblemente depende del locale.
        tmp = tokens.nextToken();
        tmp = tmp.replace(",", ".");
        row[0] = Double.parseDouble(tmp);

        tmp = tokens.nextToken();
        tmp = tmp.replace(",", ".");
        row[1] = Double.parseDouble(tmp);

        rows.add(row);
      }

      this.spectraAux = new double[rows.size()][2];
      this.spectraAux = rows.toArray(this.spectraAux);

      return this.spectraAux;
    }
    catch (FileNotFoundException e)
    {
      e.printStackTrace();
    }
    return this.spectraAux;
  }

}

/* Location:           /home/pardo/Desktop/material_beca_ciop/LIBS Analyzer/LIBS Analyzer Linux/LIBSAnalyzer.jar
 * Qualified Name:     Algoritmos.TxtManager
 * JD-Core Version:    0.6.2
 */
