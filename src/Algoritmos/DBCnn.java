/*     */ package Algoritmos;
/*     */ 
          import java.sql.Connection;
          import java.sql.Statement;
/*     */ import java.io.PrintStream;
/*     */ import java.sql.DriverManager;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.SQLException;
/*     */ import java.util.ArrayList;
/*     */ import javax.swing.JOptionPane;
/*     */ 
/*     */ public class DBCnn
/*     */ {
            static String url = "jdbc:sqlite::resource:data/libsNist.sqlite";
/*  23 */   public Connection cnn = null;
/*  24 */   public Statement stmt = null;
/*  25 */   public ResultSet reg = null;
/*  26 */   public String tbName = "";
/*  27 */   public double lambda = 0.0D;
/*  28 */   public double lambdasup = 0.0D;
/*  29 */   public double lambdainf = 0.0D;
/*  30 */   public double intRel = 0.0D;
/*  31 */   public double err = 0.0D;
/*  32 */   public ArrayList lambdaVal = new ArrayList();
/*  33 */   public ArrayList elementVal = new ArrayList();
/*  34 */   public ArrayList intRelVal = new ArrayList();
/*  35 */   public ArrayList intRelBusq = new ArrayList();
/*  36 */   public ArrayList lambdaBusq = new ArrayList();
/*  37 */   public ArrayList ionizacion = new ArrayList();
/*  38 */   public ArrayList persistencia = new ArrayList();
/*     */   public double[][] spectraAux;
/*     */   public String[][] spectraFind;
/*     */ 
/*     */   public void setTbName(String tbName)
/*     */   {
/*  45 */     this.tbName = ("Element" + tbName);
/*     */   }
/*     */ 
/*     */   public void setLambda(double lambda) {
/*  49 */     this.lambda = lambda;
/*     */   }
/*     */ 
/*     */   public void setErr(double err) {
/*  53 */     this.err = err;
/*     */   }
/*     */ 
/*     */   public void setLambdasup(double lambdasup) {
/*  57 */     this.lambdasup = lambdasup;
/*     */   }
/*     */ 
/*     */   public void setLambdainf(double lambdainf) {
/*  61 */     this.lambdainf = lambdainf;
/*     */   }
/*     */ 
/*     */   public void setIntRel(double intRel) {
/*  65 */     this.intRel = intRel;
/*     */   }
/*     */ 
/*     */   public double[][] getSpectraAux() {
/*  69 */     return this.spectraAux;
/*     */   }
/*     */ 
/*     */   public String[][] getSpectraFind() {
/*  73 */     this.spectraFind = new String[this.lambdaVal.size()][7];
/*  74 */     for (int i = 0; i < this.lambdaVal.size(); i++) {
/*  75 */       this.spectraFind[i][0] = String.valueOf(this.elementVal.get(i));
/*  76 */       this.spectraFind[i][1] = String.valueOf(this.ionizacion.get(i));
/*  77 */       this.spectraFind[i][2] = String.valueOf(this.lambdaVal.get(i));
/*  78 */       this.spectraFind[i][3] = String.valueOf(this.intRelVal.get(i));
/*  79 */       this.spectraFind[i][4] = String.valueOf(this.persistencia.get(i));
/*  80 */       this.spectraFind[i][5] = String.valueOf(this.intRelBusq.get(i));
/*  81 */       this.spectraFind[i][6] = String.valueOf(this.lambdaBusq.get(i));
/*     */     }
/*  83 */     return this.spectraFind;
/*     */   }

            public void conex()
            {
              try {
                this.cnn = DriverManager.getConnection(url);
              } catch (Exception e) {
                System.out.println("Error al conectarse a la BD " + e.toString());
                JOptionPane.showMessageDialog(null, "Error " + e.toString(), "Error", 0);
              }
            }

            public void closeConex() {
              if (this.cnn == null) {
                return;
              }
              try {
                this.cnn.close();
                this.cnn = null;
              } catch (SQLException e) {
                System.out.println("Conexion Cerrada");
                JOptionPane.showMessageDialog(null, "Error " + e.toString(), "Error", 0);
              }
            }

/*     */   public void calcRange() {
/* 109 */     this.lambdasup = (this.lambda + this.err);
/* 110 */     this.lambdainf = (this.lambda - this.err);
/*     */   }
/*     */ 
/*     */   public void busqElement() {
/* 114 */     conex();
/* 115 */     if (this.cnn != null) {
/*     */       try {
/* 117 */         this.stmt = null;
/* 118 */         this.reg = null;
/* 119 */         calcRange();
/* 120 */         this.stmt = ((Statement)this.cnn.createStatement());
/* 121 */         this.reg = this.stmt.executeQuery("SELECT Elemento,Ionizacion,Lambda,IntRel,Persistencia FROM " + this.tbName + " WHERE Lambda BETWEEN " + this.lambdainf + " AND " + this.lambdasup);
/* 122 */         if (this.reg.next()) {
/*     */           do {
/* 124 */             this.elementVal.add(this.reg.getString("Elemento"));
/* 125 */             this.ionizacion.add(this.reg.getString("Ionizacion"));
/* 126 */             this.lambdaVal.add(Float.valueOf(this.reg.getFloat("Lambda")));
/* 127 */             this.intRelVal.add(Float.valueOf(this.reg.getFloat("IntRel")));
/* 128 */             this.persistencia.add(this.reg.getString("Persistencia"));
/* 129 */             this.intRelBusq.add(Double.valueOf(this.intRel));
/* 130 */             this.lambdaBusq.add(Double.valueOf(this.lambda));
/* 131 */           }while (this.reg.next());
/*     */         }
/* 133 */         this.reg.close();
/* 134 */         this.stmt.close();
/*     */       } catch (Exception e) {
/* 136 */         System.out.println(e.toString());
/* 137 */         JOptionPane.showMessageDialog(null, "Error " + e.toString(), "Error", 0);
/* 138 */         System.exit(1);
/*     */       }
/*     */     }
/* 141 */     closeConex();
/*     */   }
/*     */ 
/*     */   public void busqLines() {
/* 145 */     conex();
/* 146 */     if (this.cnn != null) {
/*     */       try {
/* 148 */         this.stmt = null;
/* 149 */         this.reg = null;
/* 150 */         this.stmt = this.cnn.createStatement();
                  this.reg = this.stmt.executeQuery("SELECT COUNT(*) as total FROM " + this.tbName + " WHERE Lambda BETWEEN " + this.lambdainf + " AND " + this.lambdasup);
                  int cont = reg.getInt("total");

                  this.reg = this.stmt.executeQuery("SELECT Lambda,IntRel FROM " + this.tbName + " WHERE Lambda BETWEEN " + this.lambdainf + " AND " + this.lambdasup);
                  if (cont > 0) {
                    this.reg.next();
                    int i = 0;
/* 157 */           this.spectraAux = new double[cont][2];
/*     */           do {
/* 159 */             this.spectraAux[i][0] = this.reg.getFloat("Lambda");
/* 160 */             this.spectraAux[i][1] = this.reg.getFloat("IntRel");
/* 161 */             i++;
/* 162 */           }while (this.reg.next());
/*     */         }
/*     */         else {
/* 165 */           JOptionPane.showMessageDialog(null, "No se encontro Elemento", "Error", 0);
/*     */         }
/* 167 */         this.reg.close();
/* 168 */         this.stmt.close();
/*     */       } catch (Exception e) {
/* 170 */         System.out.println("busLines() error: " + e.toString());
/* 171 */         JOptionPane.showMessageDialog(null, "Error " + e.toString(), "Error", 0);
/* 172 */         System.exit(1);
/*     */       }
/*     */     }
/* 175 */     closeConex();
/*     */   }
/*     */ }

/* Location:           /home/pardo/Desktop/material_beca_ciop/LIBS Analyzer/LIBS Analyzer Linux/LIBSAnalyzer.jar
 * Qualified Name:     Algoritmos.DBCnn
 * JD-Core Version:    0.6.2
 */
