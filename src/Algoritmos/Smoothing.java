package Algoritmos;

public class Smoothing
{

  public enum SmoothingType {
    NONE,
    MOVING_AVERAGE,
    SAVITZKY_GOLAY
  }

  public SmoothingType type = SmoothingType.NONE;
  public int k = 1;

  private double[][] spectraAux;

  public Smoothing(double[][] espectro)
  {
    this.spectraAux = espectro;
  }

  public Smoothing()
  {
  }

  public void setType(SmoothingType t)
  {
    this.type = t;
  }

  public void setType(int t)
  {
    switch (t) {
      case 0:
      default:
        this.type = SmoothingType.NONE;
        break;

      case 1:
        this.type = SmoothingType.MOVING_AVERAGE;
        break;

      case 2:
        this.type = SmoothingType.SAVITZKY_GOLAY;
        break;
    }
  }

  public double[][] process()
  {
    return this.process(this.spectraAux, this.k);
  }

  public double[][] process(int k)
  {
    return this.process(this.spectraAux, k);
  }

  public double[][] process(double[][] src)
  {
    return this.process(src, this.k);
  }

  public double[][] process(double[][] src, int k)
  {
    switch(this.type) {
      case MOVING_AVERAGE:
        return this.moving_average_filter(k, src);

      case SAVITZKY_GOLAY:
        return this.savitzky_golay_filter(k, src);

      default:
        return src;
    }
  }

  public double[][] moving_average_filter(int k)
  {
    return this.moving_average_filter(k, this.spectraAux);
  }

  public double[][] moving_average_filter(int k, double[][] src)
  {
    int ancho = 2 * k + 1;
    double[][] newspectra = new double[src.length - ancho][2];
    double temp = 0.0D;
    double temp1 = 0.0D;
    int j = 0;
    int i = 0;
    if ((ancho < src.length) && (k > 0)) {
      for (int n = 0; n < 2; n++) {
        j = 0;
        while (j + ancho < src.length) {
          for (i = j; i < j + ancho; i++) {
            temp +=  src[i][0];
            temp1 += src[i][1];
          }
          temp /= ancho;
          temp1 /= ancho;
          newspectra[j][0] = src[(j + k)][0];
          newspectra[j][1] = temp1;
          j++;
          temp = temp1 = 0.0D;
        }
      }
      return newspectra;
    }
    return src;
  }

  public double[][] savitzky_golay_filter(int k)
  {
    return this.savitzky_golay_filter(k, this.spectraAux);
  }

  public double[][] savitzky_golay_filter(int k, double[][] src)
  {
    int ancho = 2 * k + 1;

    int[] A = { 0 };

    double temp = 0.0D;
    double temp1 = 0.0D;
    int j = 0;
    int i = 0;

    if ((ancho < src.length) && (k > 1)) {
      switch (k) {
      case 2:
        A = new int[] { -3, 12, 17, 12, -3 };
        break;
      case 3:
        A = new int[] { -2, 3, 6, 7, 6, 3, -2 };
        break;
      case 4:
        A = new int[] { -21, 14, 39, 54, 59, 54, 39, 14, -21 };
        break;
      case 5:
        A = new int[] { -36, 9, 44, 69, 84, 89, 84, 69, 44, 9, -36 };
        break;
      default:
        A = new int[] { -36, 9, 44, 69, 84, 89, 84, 69, 44, 9, -36 };
        k = 5;
        ancho = 11;
      }

      double[][] newspectra = new double[src.length - ancho][2];
      int sumA = sumatoriaA(A);
      for (int n = 0; n < 2; n++) {
        while (j + ancho < src.length) {
          int m = 0;
          for (i = j; i < j + ancho; i++) {
            temp += src[i][1] * A[m];
            m++;
          }

          temp /= sumA;
          newspectra[j][0] = src[(j + k)][0];
          newspectra[j][1] = temp;

          j++;
          temp = 0.0D;
        }
      }
      return newspectra;
    }
    return src;
  }

  private int sumatoriaA(int[] A)
  {
    int sum = 0;
    for (int i = 0; i < A.length; i++) {
      sum += A[i];
    }
    return sum;
  }
}

/* Location:           /home/pardo/Desktop/material_beca_ciop/LIBS Analyzer/LIBS Analyzer Linux/LIBSAnalyzer.jar
 * Qualified Name:     Algoritmos.Smoothing
 * JD-Core Version:    0.6.2
 */
