package Algoritmos;

public class AjusteDominio
{
  public double[][] spectraAux;
  public double desp;

  public void AjusteDominio(double[][] espectro)
  {
    this.spectraAux = espectro;
  }

  public void AjusteDominio()
  {
  }

  public void setDesp(double desp) {
    this.desp = desp;
  }

  public double[][] process ()
  {
    return this.process(this.spectraAux);
  }

  public double[][] process(double[][] src)
  {
    double[][] tmp = new double[src.length][2];

    for (int i = 0; i < src.length; i++) {
      tmp[i][0] = (src[i][0] + this.desp);
      tmp[i][1] =  src[i][1];
    }

    return tmp;
  }
}

/* Location:           /home/pardo/Desktop/material_beca_ciop/LIBS Analyzer/LIBS Analyzer Linux/LIBSAnalyzer.jar
 * Qualified Name:     Algoritmos.AjusteDominio
 * JD-Core Version:    0.6.2
 */
