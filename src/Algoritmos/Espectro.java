package Algoritmos;

import java.util.Arrays;
import java.util.Vector;

import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

import Algoritmos.AjusteCero;
import Algoritmos.AjusteDominio;
import Algoritmos.BaseLine;
import Algoritmos.NormalizeData;
import Algoritmos.PeakDetection;
import Algoritmos.RangeClip;
import Algoritmos.SeriesDatos;
import Algoritmos.Smoothing;


public class Espectro
{
  public double[][] orig_spectra;
  public double[][] spectra;
  public double[][] peaks;

  public String name;

  public boolean doOffset    = false;
  public boolean doRange     = false;
  public boolean doNormalize = true;
  public boolean doBaseline  = false;
  public boolean doPeaks     = false;
  public int     doSmoothing = 0;

  // deberian ser private, solo por ahorrar tiempo ahora.
  public AjusteCero    ceroFilter;
  public AjusteDominio offsetFilter;
  public RangeClip     rangeClipFilter;
  public NormalizeData normalizeFilter;
  public BaseLine      baselineFilter;
  public PeakDetection peakDetectionFilter;
  public Smoothing     smoothingFilter;

  private Vector<ChangeListener> listenerList = new Vector<ChangeListener>();

  public Espectro (double[][] espectro)
  {
    this();
    this.setData(espectro);
  }

  public Espectro ()
  {
    this.ceroFilter           = new AjusteCero();
    this.offsetFilter         = new AjusteDominio();
    this.rangeClipFilter      = new RangeClip();
    this.normalizeFilter      = new NormalizeData();
    this.baselineFilter       = new BaseLine();
    this.peakDetectionFilter  = new PeakDetection();
    this.smoothingFilter      = new Smoothing();

    spectra = new double[3][2];
    spectra[0][0] = 100.0D;
    spectra[0][1] = 1000.0D;
    spectra[1][0] = 0.0D;
    spectra[1][1] = 1.0D;

    this.baselineFilter.setMode(BaseLine.BaselineMode.NAIVE);
    peaks = new double[0][2];
  }

  public void setData(double[][] data)
  {
    this.orig_spectra = clone2DMatrix(data);
    this.process();
  }

  public double[][] process ()
  {
    double[][] spectra = this.orig_spectra;

    spectra = this.ceroFilter.process(spectra);

    if (this.doOffset) {
      spectra = this.offsetFilter.process(spectra);
    }

    if (this.doRange) {
      spectra = this.rangeClipFilter.process(spectra);
    }

    if (this.doNormalize) {
      spectra = this.normalizeFilter.process(spectra, 1);
    }

    if (this.doBaseline) {
      spectra = this.baselineFilter.process(spectra);
    }

    if (this.doSmoothing != 0) {
      spectra = this.smoothingFilter.process(spectra);
    }

    if (this.doPeaks) {
      spectra = this.peakDetectionFilter.process(spectra);
      peaks   = this.peakDetectionFilter.peakDetect();
    }

    this.spectra = spectra;
    this.notifyChange();
    return spectra;
  }

  public double[][] getPeaks ()
  {
    if (this.doPeaks) {
      return this.peaks;
    } else {
      return new double[0][2];
    }
  }


  // Really?!
  public double[][] clone2DMatrix(double[][] src)
  {
    double[][] ret = new double[src.length][];

    for(int i = 0; i < src.length; i++) {
      ret[i] = Arrays.copyOf(src[i], src[i].length);
    }

    return ret;
  }

  public String toString ()
  {
    return this.name;
  }

  public synchronized void addChangeListener(ChangeListener listener)
  {
    listenerList.addElement(listener);
  }

  public synchronized void removeChangeListener(ChangeListener listener)
  {
    listenerList.removeElement(listener);
  }

  protected void notifyChange()
  {
    ChangeEvent ev = new ChangeEvent(this);
    for (int i = 0;  i < listenerList.size(); i++) {
      ((ChangeListener)listenerList.elementAt(i)).stateChanged(ev);
    }
  }


}
