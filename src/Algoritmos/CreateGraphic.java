/*     */ package Algoritmos;
/*     */ 
/*     */ import com.itextpdf.awt.DefaultFontMapper;
/*     */ import com.itextpdf.text.Document;
/*     */ import com.itextpdf.text.Rectangle;
/*     */ import com.itextpdf.text.pdf.PdfContentByte;
/*     */ import com.itextpdf.text.pdf.PdfTemplate;
/*     */ import com.itextpdf.text.pdf.PdfWriter;
/*     */ import java.awt.BasicStroke;
/*     */ import java.awt.Color;
/*     */ import java.awt.Font;
/*     */ import java.awt.Graphics2D;
/*     */ import java.awt.Polygon;
/*     */ import java.awt.geom.Rectangle2D;
/*     */ import java.awt.geom.Rectangle2D.Double;
/*     */ import java.awt.image.BufferedImage;
/*     */ import java.io.File;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.IOException;
/*     */ import java.io.PrintStream;

          import java.util.HashMap;

/*     */ import org.jfree.chart.ChartFactory;
/*     */ import org.jfree.chart.ChartUtilities;
/*     */ import org.jfree.chart.JFreeChart;
/*     */ import org.jfree.chart.annotations.XYTextAnnotation;
/*     */ import org.jfree.chart.axis.NumberAxis;
/*     */ import org.jfree.chart.axis.NumberTickUnit;
/*     */ import org.jfree.chart.plot.PlotOrientation;
/*     */ import org.jfree.chart.plot.XYPlot;
/*     */ import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;

import org.jfree.chart.axis.NumberTickUnitSource;
import org.jfree.chart.title.LegendTitle;

/*     */ import org.jfree.data.xy.XYSeriesCollection;
/*     */ import org.jfree.util.ShapeUtilities;
/*     */ 
/*     */ public class CreateGraphic
/*     */ {
/*  42 */   String ejeX = "Longitud de Onda (nm)";
/*  43 */   final String ejeY = "Intensidad Relativa (U.A.)";
/*  44 */   public String title = "Espectro de Onda";
/*     */   JFreeChart GraphEnd;
/*     */   BufferedImage graphImage;
/*  49 */   Color backC = new Color(255, 255, 255);
/*  50 */   Color guideL = new Color(200, 200, 200);
/*  51 */   Color backL = new Color(255, 255, 255);
/*  52 */   Color serieC = new Color(255, 150, 220);
/*  53 */   Color g0Color = new Color(0, 0, 0);
/*  54 */   Color g1Color = new Color(255, 50, 50);
/*  55 */   Color g2Color = new Color(150, 150, 150);
/*  56 */   int[] xPoly = { 1, 5, 9 };
/*  57 */   int[] yPoly = { 1, 9, 1 };
/*     */ 
/*  59 */   Polygon shape = new Polygon(this.xPoly, this.yPoly, this.xPoly.length);
/*     */ 
/*  61 */   Font labelFont = new Font("Helvetica", 0, 18);
/*  62 */   Font labelFontA = new Font("Helvetica", 0, 16);
/*     */ 
            public int datasetIdx = 1;
            public HashMap<GraficaEspectro, Integer> graficasMap = new HashMap<GraficaEspectro, Integer>();

/*  64 */   int rangeMin = 100;
/*  65 */   int rangeMax = 1000;
/*  66 */   float domainMin = 0.0F;
/*  67 */   float domainMax = 1.1F;
/*  68 */   int incrementRange = 100;
/*  69 */   float incrementDomain = 0.2F;
/*     */ 
/*  71 */   float strokLine = 1.2F;
/*  72 */   float strokGuide = 1.0F;
/*     */   String[][] labelPeak;
/*     */   XYTextAnnotation lblP;
/*     */   int numS;
/*     */   String txtElement;
/*     */ 
/*     */   public void setLabels(String[][] labelP, int numSpace)
/*     */   {
/*  79 */     this.labelPeak = labelP;
/*  80 */     this.numS = numSpace;
/*     */   }
/*     */ 
/*     */   public void setG0Color(Color g0Color) {
/*  84 */     this.g0Color = g0Color;
/*     */   }
/*     */ 
/*     */   public void setG1Color(Color g1Color) {
/*  88 */     this.g1Color = g1Color;
/*     */   }
/*     */ 
/*     */   public void setG2Color(Color g2Color) {
/*  92 */     this.g2Color = g2Color;
/*     */   }
/*     */ 
/*     */   public void setBackC(Color backC) {
/*  96 */     this.backC = backC;
/*     */   }
/*     */ 
/*     */   public void setBackL(Color backL) {
/* 100 */     this.backL = backL;
/*     */   }
/*     */ 
/*     */   public void setGuideL(Color guideL) {
/* 104 */     this.guideL = guideL;
/*     */   }
/*     */ 
/*     */   public void setDomainMax(float domainMax) {
/* 108 */     this.domainMax = domainMax;
/*     */   }
/*     */ 
/*     */   public void setDomainMin(float domainMin) {
/* 112 */     this.domainMin = domainMin;
/*     */   }
/*     */ 
/*     */   public void setRangeMax(int rangeMax) {
/* 116 */     this.rangeMax = rangeMax;
/*     */   }
/*     */ 
/*     */   public void setRangeMin(int rangeMin) {
/* 120 */     this.rangeMin = rangeMin;
/*     */   }
/*     */ 
/*     */   public void setIncrementRange(int incrementRange) {
/* 124 */     this.incrementRange = incrementRange;
/*     */   }
/*     */ 
/*     */   public void setIncrementDomain(float incrementDomain) {
/* 128 */     this.incrementDomain = incrementDomain;
/*     */   }
/*     */ 
/*     */   public void setStrokLine(float strokLine) {
/* 132 */     this.strokLine = strokLine;
/*     */   }
/*     */ 
/*     */   public void setStrokGuide(float strokGuide) {
/* 136 */     this.strokGuide = strokGuide;
/*     */   }
/*     */ 
/*     */   public float getDomainMax() {
/* 140 */     return this.domainMax;
/*     */   }
/*     */ 
/*     */   public float getDomainMin() {
/* 144 */     return this.domainMin;
/*     */   }
/*     */ 
/*     */   public int getRangeMax() {
/* 148 */     return this.rangeMax;
/*     */   }
/*     */ 
/*     */   public int getRangeMin() {
/* 152 */     return this.rangeMin;
/*     */   }
/*     */ 
/*     */   public float getIncrementDomain() {
/* 156 */     return this.incrementDomain;
/*     */   }
/*     */ 
/*     */   public int getIncrementRange() {
/* 160 */     return this.incrementRange;
/*     */   }
/*     */ 
/*     */   public Color getG0Color() {
/* 164 */     return this.g0Color;
/*     */   }
/*     */ 
/*     */   public Color getG1Color() {
/* 168 */     return this.g1Color;
/*     */   }
/*     */ 
/*     */   public Color getG2Color() {
/* 172 */     return this.g2Color;
/*     */   }
/*     */ 
/*     */   public Color getBackC() {
/* 176 */     return this.backC;
/*     */   }
/*     */ 
/*     */   public Color getBackL() {
/* 180 */     return this.backL;
/*     */   }
/*     */ 
/*     */   public Color getGuideL() {
/* 184 */     return this.guideL;
/*     */   }
/*     */ 
/*     */   public float getStrokLine() {
/* 188 */     return this.strokLine;
/*     */   }
/*     */ 
/*     */   public float getStrokGuide() {
/* 192 */     return this.strokGuide;
/*     */   }
/*     */ 
/*     */   public void clearLabels() {
/* 196 */     XYPlot plot2 = (XYPlot)this.GraphEnd.getPlot();
/* 197 */     plot2.clearAnnotations();
/*     */   }
/*     */ 
/*     */   public void viewLabels() {
/* 201 */     XYPlot plot1 = (XYPlot)this.GraphEnd.getPlot();
/* 202 */     plot1.clearAnnotations();
/* 203 */     for (int i = 0; i < this.labelPeak.length; i++) {
/* 204 */       float cX = Float.valueOf(this.labelPeak[i][0]).floatValue();
/* 205 */       float cY = Float.valueOf(this.labelPeak[i][1]).floatValue();
/* 206 */       String lbl = this.labelPeak[i][2];
/* 207 */       String space = new String("");
/* 208 */       for (int j = 0; j <= this.numS; j++) {
/* 209 */         space = space + " ";
/*     */       }
/* 211 */       String lbl1 = space + lbl;
/* 212 */       this.lblP = new XYTextAnnotation(lbl1, cX, cY);
/* 213 */       this.lblP.setFont(this.labelFontA);
/* 214 */       this.lblP.setRotationAngle(-1.570796326794897D);
/*     */ 
/* 216 */       plot1.addAnnotation(this.lblP);
/*     */     }
/*     */   }
/*     */ 
            public JFreeChart creaChart(XYSeriesCollection coorXY)
            {
              try {
                this.GraphEnd = ChartFactory.createXYLineChart("", this.ejeX, "Intensidad Relativa (U.A.)", coorXY, PlotOrientation.VERTICAL, true, true, false);

                XYPlot plot = (XYPlot)this.GraphEnd.getPlot();
                XYLineAndShapeRenderer render = (XYLineAndShapeRenderer)plot.getRenderer();
                setAxis();
                setColorsStrok();

                render.setSeriesShape(1, ShapeUtilities.createTranslatedShape(this.shape, -4.5D, -12.0D));
                render.setSeriesLinesVisible(0, true);
                render.setSeriesLinesVisible(1, false);
                render.setSeriesShapesVisible(1, true);
                render.setBaseLegendTextFont(this.labelFont);
                render.setBaseItemLabelFont(this.labelFont);
              }
              catch (Exception e) {
                System.out.println(e.toString());
              }
              return this.GraphEnd;
            }

            public JFreeChart creaChart()
            {
              return this.creaChart(null);
            }

            public void setLegendVisibility (boolean visible) {
                JFreeChart  chart = this.GraphEnd;
                LegendTitle title = chart.getLegend();
                title.setVisible(visible);
            }

            public void addGraficaEspectro (GraficaEspectro g) {
                XYPlot plot = (XYPlot)this.GraphEnd.getPlot();

                XYLineAndShapeRenderer renderer = g.getRenderer();
                XYSeriesCollection     dataset  = g.getDataset();

                plot.setDataset( this.datasetIdx, dataset);
                plot.setRenderer(this.datasetIdx, renderer);
                this.graficasMap.put(g, this.datasetIdx);

                this.datasetIdx++;

                renderer = g.getPeaksRenderer();
                dataset  = g.getPeaksDataset();

                plot.setDataset( this.datasetIdx, dataset);
                plot.setRenderer(this.datasetIdx, renderer);

                this.datasetIdx++;
            }

            public void removeGraficaEspectro (GraficaEspectro g) {
                XYPlot plot = (XYPlot)this.GraphEnd.getPlot();
                int idx = this.graficasMap.get(g);

                plot.setDataset( idx,   null);
                plot.setRenderer(idx,   null);
                plot.setDataset( idx+1, null);
                plot.setRenderer(idx+1, null);
            }

/*     */   public void setAxis() {
/* 249 */     XYPlot plot = (XYPlot)this.GraphEnd.getPlot();
/*     */ 
/* 251 */     NumberAxis domain = (NumberAxis)plot.getDomainAxis();
// /* 252 */     domain.setRange(this.rangeMin, this.rangeMax);
              domain.setAutoRange(true);
//              domain.setTickUnit(new NumberTickUnit(this.incrementRange));
//
              domain.setStandardTickUnits(new NumberTickUnitSource(true));
//              domain.setAutoTickUnitSelection(true);

              domain.setMinorTickCount(5);
              domain.setMinorTickMarksVisible(true);

              domain.setTickMarkOutsideLength(10);
              domain.setTickMarkStroke(new BasicStroke(1.5F, 1, 2));

/* 254 */     domain.setLabelFont(this.labelFont);
/* 255 */     domain.setTickLabelFont(this.labelFont);
/* 256 */     domain.setLabelPaint(Color.BLACK);
/* 257 */     domain.setTickLabelPaint(Color.black);
/* 258 */     domain.setAxisLinePaint(Color.black);
/* 259 */     domain.setAxisLineStroke(new BasicStroke(1.0F, 1, 2));
/*     */ 
/* 261 */     NumberAxis range = (NumberAxis)plot.getRangeAxis();
/*     */ 
/* 263 */     range.setRange(this.domainMin, this.domainMax);
              range.setRange(0, 1.5);

//              range.setTickUnit(new NumberTickUnit(this.incrementDomain));
              // range.setAutoTickUnitSelection(true);
              range.setStandardTickUnits(new NumberTickUnitSource());
              range.setMinorTickCount(5);
              range.setMinorTickMarksVisible(true);
              range.setTickMarkOutsideLength(5);
              range.setTickMarkStroke(new BasicStroke(1.5F, 1, 2));

/* 265 */     range.setLabelFont(this.labelFont);
/* 266 */     range.setTickLabelFont(this.labelFont);
/* 267 */     range.setLabelPaint(Color.BLACK);
/* 268 */     range.setTickLabelPaint(Color.black);
/* 269 */     range.setAxisLinePaint(Color.black);
/* 270 */     range.setAxisLineStroke(new BasicStroke(0.5F, 1, 2));
/*     */   }
/*     */ 
/*     */   public void setColorsStrok() {
/* 274 */     XYPlot plot = (XYPlot)this.GraphEnd.getPlot();
/* 275 */     XYLineAndShapeRenderer render = (XYLineAndShapeRenderer)plot.getRenderer();
/* 276 */     render.setSeriesPaint(0, this.g0Color);
/* 277 */     render.setSeriesPaint(1, this.g1Color);
/* 278 */     render.setSeriesStroke(0, new BasicStroke(this.strokLine, 1, 2));
/* 279 */     render.setSeriesStroke(1, new BasicStroke(this.strokLine, 1, 2));
/* 280 */     this.GraphEnd.setBackgroundPaint(this.backC);

//              plot.setDomainGridlinePaint(this.guideL);
//              plot.setRangeGridlinePaint(this.guideL);
//              plot.setRangeGridlineStroke(new BasicStroke(this.strokGuide, 1, 1, 1.0F, new float[] { 8.0F, 8.0F }, 0.0F));
//              plot.setDomainGridlinePaint(this.guideL);
//              plot.setDomainGridlineStroke(new BasicStroke(this.strokGuide, 1, 1, 1.0F, new float[] { 8.0F, 8.0F }, 0.0F));
//              plot.setRangeGridlinePaint(this.guideL);

              plot.setBackgroundPaint(this.backL);
/*     */   }
/*     */ 
/*     */   public void exportPNGGraph(File dirGraph) {
/*     */     try {
/* 293 */       ChartUtilities.saveChartAsPNG(dirGraph, this.GraphEnd, 800, 580);
/*     */     }
/*     */     catch (IOException ex) {
/* 296 */       System.out.println("Error al exportar");
/*     */     }
/*     */   }
/*     */ 
            public void exportJPEGGraph(File dirGraph) {
              try {
                ChartUtilities.saveChartAsJPEG(dirGraph, 0.9f, this.GraphEnd, 800, 580);
              }
              catch (IOException ex) {
                System.out.println("Error al exportar");
              }
            }
 
/*     */   public void writeChartToPDF(JFreeChart chart, File fileName) {
/* 301 */     PdfWriter writer = null;
/*     */     try
/*     */     {
/* 305 */       Rectangle pagesize = new Rectangle(800.0F, 600.0F);
/* 306 */       Document document = new Document(pagesize);
/* 307 */       writer = PdfWriter.getInstance(document, new FileOutputStream(fileName));
/* 308 */       document.open();
/* 309 */       PdfContentByte contentByte = writer.getDirectContent();
/* 310 */       PdfTemplate template = contentByte.createTemplate(800.0F, 600.0F);
/* 311 */       Graphics2D graphics2d = template.createGraphics(800.0F, 600.0F, new DefaultFontMapper());
/* 312 */       Rectangle2D rectangle2d = new Rectangle2D.Double(0.0D, 0.0D, 800.0D, 600.0D);
/*     */ 
/* 314 */       chart.draw(graphics2d, rectangle2d);
/*     */ 
/* 316 */       graphics2d.dispose();
/* 317 */       contentByte.addTemplate(template, 0.0F, 0.0F);
/* 318 */       document.close();
/* 319 */       document = null;
/*     */     }
/*     */     catch (Exception e) {
/* 322 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */ }

/* Location:           /home/pardo/Desktop/material_beca_ciop/LIBS Analyzer/LIBS Analyzer Linux/LIBSAnalyzer.jar
 * Qualified Name:     Algoritmos.CreateGraphic
 * JD-Core Version:    0.6.2
 */
