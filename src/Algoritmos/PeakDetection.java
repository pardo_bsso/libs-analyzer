package Algoritmos;

import java.util.ArrayList;

public class PeakDetection
{
  public int n = 10;
  // XXX: Revisar esto.
  public double altura = 10.0D / 400;

  private double[][] spectraAux;
  private Smoothing sm = new Smoothing();

  public PeakDetection()
  {
  }

  public PeakDetection(double[][] espectro)
  {
    this.spectraAux = espectro;
  }

  public double[][] process(double[][] src)
  {
      this.spectraAux = src;
      return src;
  }

  public double[][] peakD__not__used(double[][] src) {
    ArrayList valY = new ArrayList();
    ArrayList valX = new ArrayList();
    ArrayList valYaux = new ArrayList();
    ArrayList valXaux = new ArrayList();

    double media;
    double m1;
    double m2;
    double x0;
    double y0;
    double x1;
    double y1;
    double x2;
    double y2;

    media = mediaAr(src);

    for (int i = 0; i + 2 < src.length; i++) {
      x0 = src[i][0];
      y0 = src[i][1];
      x1 = src[(i + 1)][0];
      y1 = src[(i + 1)][1];
      x2 = src[(i + 2)][0];
      y2 = src[(i + 2)][1];
      if (y1 > media) {
        m1 = ((y1 - y0) / (x1 - x0));
        m2 = ((y2 - y1) / (x2 - x1));
        if ((m1 > 0.0D) && (m2 < 0.0D)) {
          valX.add(Double.valueOf(src[(i + 1)][0]));
          valY.add(Double.valueOf(src[(i + 1)][1]));
        }
      }
    }

    double[][] newspectra = new double[valX.size()][2];

    for (int i = 0; i < newspectra.length; i++) {
      newspectra[i][0] = ((Double)valX.get(i)).doubleValue();
      newspectra[i][1] = ((Double)valY.get(i)).doubleValue();
    }

    return newspectra;
  }

  public double mediaAr(double[][] espectro) {
    double suma = 0.0D;

    for (int i = 0; i < espectro.length; i++)
    {
      suma += espectro[i][1];
    }

    double med = suma / espectro.length;
    return med;
  }

  public double[][] peakDetect()
  {
    return this.peakDetect(this.spectraAux, this.n, this.altura);
  }

  public double[][] peakDetect(double altura)
  {
    return this.peakDetect(this.spectraAux, this.n, altura);
  }

  public double[][] peakDetect(int n, double altura)
  {
    return this.peakDetect(this.spectraAux, n, altura);
  }

  public double[][] peakDetect(double[][] src, int n, double altura)
  {
    // int ancho = 2 * n;
    // XXX: Revisar todo este algoritmo.
    int ancho = n;


    ArrayList valY = new ArrayList();
    ArrayList valX = new ArrayList();
    ArrayList valYaux = new ArrayList();
    ArrayList valXaux = new ArrayList();

    double media;
    double m1;
    double m2;
    double x0;
    double y0;
    double x1;
    double y1;
    double x2;
    double y2;

    double[][] spectraFilter = this.sm.moving_average_filter(n, src);

    for (int i = 0; i < spectraFilter.length; i++) {
      spectraFilter[i][1] += altura;
      if (src[(i + ancho)][1] >= spectraFilter[i][1]) {
        valX.add(Double.valueOf(src[(i + ancho)][0]));
        valY.add(Double.valueOf(src[(i + ancho)][1]));
      }
    }

    for (int i = 0; i + 2 < valX.size(); i++) {
      x0 = ((Double)valX.get(i)).doubleValue();
      y0 = ((Double)valY.get(i)).doubleValue();
      x1 = ((Double)valX.get(i + 1)).doubleValue();
      y1 = ((Double)valY.get(i + 1)).doubleValue();
      x2 = ((Double)valX.get(i + 2)).doubleValue();
      y2 = ((Double)valY.get(i + 2)).doubleValue();

      m1 = ((y1 - y0) / (x1 - x0));
      m2 = ((y2 - y1) / (x2 - x1));
      // <= para detectar tambien mesetas (clipping).
      if ((m1 > 0.0D) && (m2 <= 0.0D)) {
        valXaux.add(valX.get(i + 1));
        valYaux.add(valY.get(i + 1));
      }

    }

    double[][] newspectra = new double[valXaux.size()][2];

    for (int i = 0; i < newspectra.length; i++) {
      newspectra[i][0] = ((Double)valXaux.get(i)).doubleValue();
      newspectra[i][1] = ((Double)valYaux.get(i)).doubleValue();
    }
    return newspectra;
  }
}

/* Location:           /home/pardo/Desktop/material_beca_ciop/LIBS Analyzer/LIBS Analyzer Linux/LIBSAnalyzer.jar
 * Qualified Name:     Algoritmos.PeakDetection
 * JD-Core Version:    0.6.2
 */
