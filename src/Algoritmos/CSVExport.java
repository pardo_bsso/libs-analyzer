package Algoritmos;

import java.io.FileWriter;
import java.io.IOException;

import com.opencsv.CSVWriter;

import javax.swing.JOptionPane;
public class CSVExport
{
    private CSVWriter _writer;

    public CSVExport () {

    }

    public void open (String fileName) {
        try {
            // CSVWriter (Writer writer, char separator, char quotechar, char escapechar, String lineEnd)
            this._writer = new CSVWriter(new FileWriter(fileName), ' ', '\u0000');
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Error " + e.toString(), "Error", 0);
            e.printStackTrace();
        }
    }

    public void close () {
        try {
            if (this._writer != null) {
                this._writer.close();
                this._writer = null;
            }
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Error " + e.toString(), "Error", 0);
            e.printStackTrace();
        }
    }

    public void appendData (double[][] data, String label) {
        if (this._writer == null) {
            return;
        }

        CSVWriter writer = this._writer;

        for (int idx=0; idx < data.length; idx++) {
            double origRow[] = data[idx];
            String row[];
            int length = origRow.length;

            if (label != null) {
                row = new String[length+1];
            } else {
                row = new String[length];
            }

            for (int j=0; j < length; j++) {
                row[j] = String.valueOf(origRow[j]);
            }

            if (label != null) {
                row[length] = label;
            }

            writer.writeNext(row);
        }
    }

    public void appendData (String[] data, String label) {
        if (this._writer == null) {
            return;
        }

        CSVWriter writer = this._writer;

        writer.writeNext(data);
    }

    public void write (String fileName, double[][] data) {
        try {

            // CSVWriter (Writer writer, char separator, char quotechar, char escapechar, String lineEnd)
            CSVWriter writer = new CSVWriter(new FileWriter(fileName), '\t', ' ', "\r\n");

            for (int idx=0; idx < data.length; idx++) {
                double origRow[] = data[idx];
                String row[] = new String[origRow.length];

                for (int j=0; j < origRow.length; j++) {
                    row[j] = String.valueOf(origRow[j]);
                }

                writer.writeNext(row);
            }

            writer.close();

        } catch (IOException e) {
              JOptionPane.showMessageDialog(null, "Error " + e.toString(), "Error", 0);
            e.printStackTrace();
        }
    }
}
